package com.microtec.cmovil.Utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;

import com.microtec.cmovil.CapturarDepositoActivity;
import com.microtec.cmovil.MenuPrincipal;

import java.util.logging.Handler;

import static android.support.v4.content.ContextCompat.startActivity;

public class AlertResponse {

    static boolean bandera = true;

    AlertDialog.Builder builder;

    public AlertResponse(Context context){
        this.builder = new AlertDialog.Builder(context);
    }

    public boolean showResponse(String mensaje){
        builder.setTitle("Mensaje...");
        builder.setMessage(mensaje);
        builder.setCancelable(true);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alerta = builder.create();
        alerta.show();
        return bandera;
    }

}
