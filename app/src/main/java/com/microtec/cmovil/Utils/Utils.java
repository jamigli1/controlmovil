package com.microtec.cmovil.Utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.microtec.cmovil.Helper.ControlMovil;
import com.microtec.cmovil.LoginActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class Utils {

    public static boolean isMockSettingsON(Context context) {
        // returns true if mock location enabled, false if not enabled.
        if (Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ALLOW_MOCK_LOCATION).equals("0"))
            return false;
        else
            return true;
    }

    public static SharedPreferences getSharedPref() {
        return ControlMovil.getAppContext().getSharedPreferences(AppConstants.SHARED_PREF, MODE_PRIVATE);
    }

    public static ArrayList<String> areThereMockPermissionApps(Context context) {
//        int count = 0;
        ArrayList <String> packs = new ArrayList<String>();

        PackageManager pm = context.getPackageManager();
        List<ApplicationInfo> packages =
                pm.getInstalledApplications(PackageManager.GET_META_DATA);

        /*Obtienes los paquetes*/
        for (ApplicationInfo applicationInfo : packages) {
            try {
                PackageInfo packageInfo = pm.getPackageInfo(applicationInfo.packageName,
                        PackageManager.GET_PERMISSIONS);
                // Get Permissions
                String[] requestedPermissions = packageInfo.requestedPermissions;
                if (requestedPermissions != null) {
                    for (int i = 0; i < requestedPermissions.length; i++) {
                        if (requestedPermissions[i]
                                .equals("android.permission.ACCESS_MOCK_LOCATION")
                                && !applicationInfo.packageName.equals(context.getPackageName())
                                && !applicationInfo.packageName.toLowerCase().contains("cmovil")
                                && !applicationInfo.packageName.toLowerCase().contains("controlmovil")
                                && applicationInfo.packageName.toLowerCase().contains("gps")) {
                            Log.d("package",applicationInfo.packageName.toString());
                            String[] packageName = packageInfo.packageName.replace(".","-").split("-");
                            packs.add(packageName[packageName.length-1]);
                            sendPackage(packageName[packageName.length-1], packageInfo.packageName);

                        }
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                Log.e("Got exception " , e.getMessage());
            }
        }

        return packs;
        /*if (AppConstants.packages > 0)
            return true;
        return false;*/

    }

    public static void sendPackage(final String packageName, final String nombre){
        FirebaseDatabase mDatabase;
        final DatabaseReference mReference;

        mDatabase = FirebaseDatabase.getInstance();
        mReference = mDatabase.getReference().child("online_vRuta").child(AppConstants.micdis).child("appsFake");

        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChild(packageName)){
                    mReference.child(packageName).setValue(nombre);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void obtenerIMEI(Context activity, Activity actividad) {
        String[] permissions = {Manifest.permission.READ_PHONE_STATE};
        if (hasPermissions(activity, permissions)) {
            final TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
            String imei = "";
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (activity.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                try {
                    imei = telephonyManager.getImei();
                } catch (Exception e) {
                    imei = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
                }
            }
            else {
                imei = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
            }
            SharedPreferences.Editor editor = activity.getSharedPreferences(AppConstants.SHARED_PREF,MODE_PRIVATE).edit();
            editor.putString(AppConstants.PREF_MPY_IMEI,imei).commit();
            AppConstants.imeig = imei;
            Log.d("IMEIGGGGG", imei);
        }else{
            ActivityCompat.requestPermissions(actividad,permissions,200);
            Log.d("nopaso","");
        }
    }

    public static void getTokenKey(Context context){
        SharedPreferences settings = context.getSharedPreferences("userdata",context.MODE_PRIVATE);
        final String usuario = settings.getString("usuario","-1");
        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/login.php";
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("TOKEN KEY",response.split("<")[0]);
                AppConstants.keyToken = response.split("<")[0];
                AppConstants.imeiToken = response.split("<")[1];
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("method","getTokenKey");
                params.put("usuario",usuario);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
