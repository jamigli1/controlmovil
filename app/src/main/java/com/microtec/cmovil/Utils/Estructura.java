package com.microtec.cmovil.Utils;

import android.provider.BaseColumns;

public abstract class Estructura implements BaseColumns {

    public static final String TABLE_NAME2 = "Pendientes";
    public static final String COLUMN_NAME_CLIENTE = "cliente";
    public static final String COLUMN_NAME_SATCLI = "satcli";
    public static final String COLUMN_NAME_CODIGODIS = "codigodis";
    public static final String COLUMN_NAME_COORDENADAS = "coordenadas";
    public static final String COLUMN_NAME_PRODUCTOS = "productos";

    public static final String TABLE_NAME = "Coordenadas";
    public static final String COLUMN_NAME_LATITUD = "latitud";
    public static final String COLUMN_NAME_LONGITUD = "longitud";
    public static final String COLUMN_NAME_FECHA = "fecha";
    public static final String COLUMN_NAME_HORA = "hora";
    public static final String COLUMN_NAME_ESTACIONADO = "estacionado";
}