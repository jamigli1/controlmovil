package com.microtec.cmovil.Utils;

import android.content.Context;

public class AppConstants {
    public static final int LOCATION_REQUEST = 1000;
    public static final int GPS_REQUEST = 1001;
    public static final String userPreferences = "userdata";
    public static final String userDataBase = "localdata";
    public static final String activityData = "actdata";
    public static final String KEY_LOCATION_UPDATES_RESULT = "location-update-result";
    public static String micdis = "nulo";
    public static String imeig = "nulo";
    public static boolean vruta = true;
    public static int packages = 0;
    public static String keyToken = "null";
    public static String imeiToken = "null";

    public static Context context2 = null;

    public static Context context = null;

    public static final String PREF_MPY_KEY_AUTHENTICATOR   = "CMOVIL_KEY_AUTHENTICATOR";
    public static final String SHARED_PREF              = "CMOVIL_PREF";

    public static final String PREF_MPY_NOMBRE          = "CMOVIL_NOMBRE";
    public static final String PREF_MPY_DIS             = "CMOVIL_DIS";
    public static final String PREF_MPY_USER            = "CMOVIL_USER";
    public static final String PREF_MPY_SUC             = "CMOVIL_SUC";
    public static final String PREF_MPY_ACTIVE_SESSION  = "CMOVIL_SESSION_ACTIVE";
    public static final String PREF_MPY_IMEI            = "CMOVIL_IMEI";
    public static final String PREF_MPY_TOKEN_FIREBASE      = "CMOVIL_TOKEN_FIREBASE";
    public static final String PREF_MPY_NOTIFICACIONES  = "MPY_NOTIFICACIONES";
    public static final String PREF_MPY_TIPO            = "MPY_TIPO";

}
