package com.microtec.cmovil;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.microtec.cmovil.Interactor.ClientesAdapter;
import com.microtec.cmovil.Model.Cliente;
import com.microtec.cmovil.Utils.AppConstants;
import com.microtec.cmovil.Utils.RecyclerTouchListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class PuntoVentaActivity extends AppCompatActivity {

    private static final String prefencesN = "userdata";

    private RecyclerView mList;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private List<Cliente> listClientes;
    private int tipovta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_punto_venta);

        mList = (RecyclerView) findViewById(R.id.rView_PVentas);
        listClientes = new ArrayList<>();
        mAdapter = new ClientesAdapter(getApplicationContext(),listClientes);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(),linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(mAdapter);

        Bundle extras = getIntent().getExtras();
        assert extras != null;
        tipovta = extras.getInt("tipovta1",-1);

        Intent i = null;

        //0 ProductosActivity
        //1 AccesoriosActivity

        if (tipovta == 0){
            i = new Intent(this,ProductosActivity.class);
        } else if (tipovta == 1){
            i = new Intent(this,AccesoriosActivity.class);
        } else if (tipovta != 3){
            finish();
        }

        final Intent i2 = i;

        mList.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (tipovta!=3) {
                    final String cliente = ((TextView) mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.pventa_nombre)).getText().toString();
                    final String satcli = listClientes.get(position).getSatcli();
                    i2.putExtra("cliente", cliente);
                    i2.putExtra("satcli", satcli);
                    if (AppConstants.vruta && i2 != null) {
                        startActivity(i2);
                        finish();
                    }
                }
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));


        listarClientes();

    }

    public void listarClientes(){
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");

        String metodo = "listarClientes";

        if (tipovta == 3){
            metodo = "listarMicropayments";
        }

        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/ClientesA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            final String finalMetodo = metodo;
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try{
                        String cad = response.substring(1,response.length());
                        JSONObject object = new JSONObject(cad);

                        if(object.length() != 0){
                            generaListV(object);
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    mAdapter.notifyDataSetChanged();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(PuntoVentaActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method", finalMetodo);
                    params.put("dis",dis);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void generaListV(JSONObject jsonObject) throws JSONException {
        Iterator x = jsonObject.keys();
        JSONArray jsonArray = new JSONArray();

        String[] usuario; String[] dis; String[] satcli;

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(jsonObject.get(key));
        }

        usuario = (jsonArray.getString(0).replace("[","").replace("]","")).split(",");
        dis = (jsonArray.getString(1).replace("[","").replace("]","")).split(",");
        satcli = (jsonArray.getString(2).replace("[","").replace("]","")).split(",");


        for (int i=0; i<usuario.length; i++){
            Cliente clienteC = new Cliente();
            clienteC.setNombre(usuario[i].replace("\"",""));
            clienteC.setDis(dis[i].replace("\"",""));
            clienteC.setSatcli(satcli[i].replace("\"",""));
            listClientes.add(clienteC);
        }

    }
}
