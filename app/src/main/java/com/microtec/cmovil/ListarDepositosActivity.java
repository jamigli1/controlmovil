package com.microtec.cmovil;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.microtec.cmovil.Interactor.DepositosAdapter;
import com.microtec.cmovil.Model.Deposito;
import com.microtec.cmovil.Utils.AlertResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ListarDepositosActivity extends AppCompatActivity {

    private static final String prefencesN = "userdata";

    private RecyclerView mList;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private List<Deposito> depositoList;

    AlertResponse alertResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_depositos);

        mList = (RecyclerView) findViewById(R.id.rView_Depositos);
        depositoList = new ArrayList<>();
        mAdapter = new DepositosAdapter(getApplicationContext(),depositoList);

        alertResponse = new AlertResponse(this);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(),linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(mAdapter);

        listarDepositos();
    }

    private void listarDepositos() {
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");

        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/DepositosA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //Log.d("Response",response.toString());
                    try{
                        String cad = response.substring(1,response.length());
                        JSONObject object = new JSONObject(cad);

                        if(object.length() != 0){
                            generaListV(object);
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    mAdapter.notifyDataSetChanged();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                            Toast.makeText(ListarDepositosActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                            alertResponse.showResponse("TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET");
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","listarDepositos");
                    params.put("dis",dis);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void generaListV(JSONObject object) throws JSONException {
        Iterator x = object.keys();
        JSONArray jsonArray = new JSONArray();

        String[] fecha; String[] monto; String[] banco; String[] movimiento;

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(object.get(key));
        }

        fecha = (jsonArray.getString(2).replace("[","").replace("]","")).split(",");
        monto = (jsonArray.getString(3).replace("[","").replace("]","")).split(",");
        banco = (jsonArray.getString(4).replace("[","").replace("]","")).split(",");
        movimiento = (jsonArray.getString(5).replace("[","").replace("]","")).split(",");

        for (int i=0; i<fecha.length; i++){
            Deposito depositoA = new Deposito();
            depositoA.setFechaR(fecha[i].replace("\"",""));
            depositoA.setMonto(monto[i].replace("\"",""));
            depositoA.setBanco(banco[i].replace("\"",""));
            depositoA.setMovimiento(movimiento[i].replace("\"",""));
            depositoList.add(depositoA);
        }

    }

}
