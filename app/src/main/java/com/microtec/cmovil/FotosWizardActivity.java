package com.microtec.cmovil;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.microtec.cmovil.Utils.VolleyMultipartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.microtec.cmovil.NuevoProspectoActivity.REQUEST_IMAGE_CAPTURE;

public class FotosWizardActivity extends AppCompatActivity implements View.OnClickListener {

    //public String mCurrentPaht;
    public double lat, lon;
    public String idpv;

    //Response;
    int responC = 0;

    public RelativeLayout relative_carga;

    public Button ine, domc, doml, boucher, guardar;
    public Button fachada, entIzq, entDer, entFrente, mercancia;

    public Bitmap bIne, bDomc, bDoml, bBaucher, finalbmp = null;
    public Bitmap bFachada, bEntIzq, bEntDer, bEntFrent, bMerca;

    public int indexImagen;
    public ArrayList<String> paths;
    public ArrayList<Integer> indexPaths;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fotos_wizard);

        initElementos();

        Bundle extras = getIntent().getExtras();
        assert extras != null;
        lat = extras.getDouble("latitud");
        lon = extras.getDouble("longitud");
        idpv = extras.getString("idpv");

    }

    private void initElementos() {

        paths = new ArrayList<>();
        indexPaths = new ArrayList<>();

        ine = (Button) findViewById(R.id.btnFoto_INE);
        domc = (Button) findViewById(R.id.btnFoto_CompDomC);
        doml = (Button) findViewById(R.id.btnFoto_CompDomL);
        boucher = (Button) findViewById(R.id.btnFoto_Baucher);
        fachada = (Button) findViewById(R.id.btnFoto_fachada);
        entIzq = (Button) findViewById(R.id.btnFoto_entIzq);
        entDer = (Button) findViewById(R.id.btnFoto_entDer);
        entFrente = (Button) findViewById(R.id.btnFoto_entFrente);
        mercancia = (Button) findViewById(R.id.btnFoto_Mercancia);
        guardar = (Button) findViewById(R.id.btn_guardaFotos);
        indexImagen = -1;

        relative_carga = (RelativeLayout) findViewById(R.id.relative_carga);

        ine.setOnClickListener(this);
        domc.setOnClickListener(this);
        doml.setOnClickListener(this);
        boucher.setOnClickListener(this);
        fachada.setOnClickListener(this);
        entIzq.setOnClickListener(this);
        entDer.setOnClickListener(this);
        entFrente.setOnClickListener(this);
        mercancia.setOnClickListener(this);
        guardar.setOnClickListener(this);

        lat = 0;
        lon = 0;

    }

    private void dispatchTakePictureIntent() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try{
                photoFile = createImage();
            } catch (IOException e){
                e.printStackTrace();
            }

            if (photoFile!=null){
                Uri photoUri = FileProvider.getUriForFile(this, "com.microtec.cmovil.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,photoUri);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }

    }

    public void disableButtons(int indexImagen2){

        switch (indexImagen2) {
            case 0:
                ine.setText(R.string.cargada);
                ine.setBackgroundColor(getResources().getColor(R.color.colorDisable));
                break;
            case 1:
                domc.setText(R.string.cargada);
                domc.setBackgroundColor(getResources().getColor(R.color.colorDisable));
                break;
            case 2:
                doml.setText(R.string.cargada);
                doml.setBackgroundColor(getResources().getColor(R.color.colorDisable));
                break;
            case 3:
                boucher.setText(R.string.cargada);
                boucher.setBackgroundColor(getResources().getColor(R.color.colorDisable));
                break;
            case 4:
                fachada.setText(R.string.cargada);
                fachada.setBackgroundColor(getResources().getColor(R.color.colorDisable));
                break;
            case 5:
                entIzq.setText(R.string.cargada);
                entIzq.setBackgroundColor(getResources().getColor(R.color.colorDisable));
                break;
            case 6:
                entDer.setText(R.string.cargada);
                entDer.setBackgroundColor(getResources().getColor(R.color.colorDisable));
                break;
            case 7:
                entFrente.setText(R.string.cargada);
                entFrente.setBackgroundColor(getResources().getColor(R.color.colorDisable));
                break;
            case 8:
                mercancia.setText(R.string.cargada);
                mercancia.setBackgroundColor(getResources().getColor(R.color.colorDisable));
                break;
            default:
                Toast.makeText(this, "No se pudo cargar la imagen, reintente.", Toast.LENGTH_SHORT).show();
        }

    }

    public void setImage(Bitmap imageBitmap, Bitmap finalbmp, int indexImagen2){

        switch (indexImagen2) {
            case 0:
                bIne = imageBitmap;
                break;
            case 1:
                bDomc = imageBitmap;
                break;
            case 2:
                bDoml = imageBitmap;
                break;
            case 3:
                bBaucher = imageBitmap;
                break;
            case 4:
                bFachada = finalbmp;
                break;
            case 5:
                bEntIzq = finalbmp;
                break;
            case 6:
                bEntDer = finalbmp;
                break;
            case 7:
                bEntFrent = finalbmp;
                break;
            case 8:
                bMerca = finalbmp;
                break;
            default:
                Toast.makeText(this, "No se pudo cargar la imagen, reintente.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        try{
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                disableButtons(indexImagen);
                indexPaths.add(indexImagen);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    public void termina(){
        Toast.makeText(this, "CARGA COMPLETA", Toast.LENGTH_SHORT).show();
        relative_carga.setVisibility(View.GONE);
        startActivity(new Intent(this, MenuPrincipal.class));
        finish();
    }

    public void getNUploadF(){

        Bitmap imagenC;

        int carga = 0;

        for (int i=0; i<paths.size(); i++) {
            File file = new File(paths.get(i));
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            Bitmap imageBitmap = null;
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            carga++;

            try {
                imageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.fromFile(file));
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 50, out);
                imagenC = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
                imageBitmap = Bitmap.createScaledBitmap(imagenC,(int)(imagenC.getWidth()*0.6), (int)(imagenC.getHeight()*0.6), true);
                file.delete();
            } catch (IOException e) {
                e.printStackTrace();
            }

            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 10, bytes);
            if (imageBitmap != null) {
                if (indexPaths.get(i) > 3) {
                    //Intento
                    finalbmp = imageBitmap.copy(Bitmap.Config.ARGB_8888, true);
                    Canvas canvas = new Canvas(finalbmp);
                    Paint paint = new Paint();
                    paint.setColor(Color.RED);
                    paint.setTextSize(85);
                    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));

                    canvas.drawBitmap(imageBitmap, 0, 0, paint);
                    String timeStamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
                    canvas.drawText("Coordenadas: " + lat + "," + lon + "\n" + "Fecha: " + timeStamp, 50, 130, paint);
                }

                setImage(imageBitmap, finalbmp, indexPaths.get(i));

            }
        }

        if (carga>=9) {
            imgOneBOne();
        }
    }

    private File createImage() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "CMOVIL_"+timeStamp+"_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg",storageDir);
        //mCurrentPaht = image.getAbsolutePath();
        paths.add(image.getAbsolutePath());
        return image;
    }

    public void showImageDialog(Bitmap imagen){
        /*View mView = getLayoutInflater().inflate(R.layout.alert_image,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.btn_img_ok);
        ImageView imageView = (ImageView) mView.findViewById(R.id.img_dialog);

        imageView.setImageBitmap(imagen);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();*/

    }

    public boolean checkImages(){
        if (indexPaths.size() >= 9){
            return true;
        }
        return false;
    }

    public void setAction(Button btn, Bitmap img){
        if (btn.getText().toString().contains("VER")){
            if (img != null){
                //showImageDialog(img);
            } else {
                Toast.makeText(this, "TOQUE EL BOTÓN REVISAR ANTES PARA MOSTRAR LAS FOTOS", Toast.LENGTH_SHORT).show();
            }
        } else {
            dispatchTakePictureIntent();
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btnFoto_INE:
                indexImagen = 0;
                setAction(ine,bIne);
                break;
            case R.id.btnFoto_CompDomC:
                indexImagen = 1;
                setAction(domc,bDomc);
                break;
            case R.id.btnFoto_CompDomL:
                indexImagen = 2;
                setAction(doml,bDoml);
                break;
            case R.id.btnFoto_Baucher:
                indexImagen = 3;
                setAction(boucher,bBaucher);
                break;
            case R.id.btnFoto_fachada:
                indexImagen = 4;
                setAction(fachada,bFachada);
                break;
            case R.id.btnFoto_entIzq:
                indexImagen = 5;
                setAction(entIzq,bEntIzq);
                break;
            case R.id.btnFoto_entDer:
                indexImagen = 6;
                setAction(entDer,bEntDer);
                break;
            case R.id.btnFoto_entFrente:
                indexImagen = 7;
                setAction(entFrente,bEntFrent);
                break;
            case R.id.btnFoto_Mercancia:
                indexImagen = 8;
                setAction(mercancia,bMerca);
                break;
            case R.id.btn_guardaFotos:

                final String gbtn = guardar.getText().toString().toUpperCase();
                Log.d("boton", gbtn);

                relative_carga.setVisibility(View.VISIBLE);
                guardar.setEnabled(false);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (gbtn.contains("GUARDAR")) {
                            if (checkImages() && paths.size() >= 9) {
                                guardar.setText("ENVIANDO...");

                                AsyncTask.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        getNUploadF();
                                    }
                                });

                                guardar.setEnabled(false);
                                guardar.setBackground(getDrawable(R.drawable.buttonblue));
                                guardar.setText("ENVIADO");
                                relative_carga.setVisibility(View.GONE);

                                termina();

                            } else {
                                relative_carga.setVisibility(View.GONE);
                                Toast.makeText(FotosWizardActivity.this, "DEBE ADJUNTAR TODAS LAS IMAGENES", Toast.LENGTH_SHORT).show();
                                guardar.setEnabled(true);
                                guardar.setBackground(getDrawable(R.drawable.buttonblue));
                            }
                        } else {

                        }
                    }
                }, 1000);
                break;
            default:
                indexImagen = -1;
        }
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public void imgOneBOne(){
        uploadBitmap(bIne,"ine");
        uploadBitmap(bDomc,"compDomCliente");
        uploadBitmap(bDoml, "compDomLocal");
        uploadBitmap(bBaucher, "baucher");
        uploadBitmap(bFachada, "fachada");
        uploadBitmap(bEntIzq, "entradaIzq");
        uploadBitmap(bEntDer, "entradaDer");
        uploadBitmap(bEntFrent, "entradaFrente");
        uploadBitmap(bMerca, "mercancia");

        if (responC>=9){
            Toast.makeText(this, "CARGA COMPLETA", Toast.LENGTH_SHORT).show();
            relative_carga.setVisibility(View.GONE);
            startActivity(new Intent(this, MenuPrincipal.class));
            finish();
        }
    }

    public void uploadBitmap(final Bitmap bitmap, String imgname){

        final String nombre = imgname.concat("-"+idpv+".jpg");

        final String method = "actualizarImagenes";

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/ClientesA.php",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        if (new String(response.data).contains("\"ruta\"")) {
                            Log.d("Response", new String(response.data));
                            responC++;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(FotosWizardActivity.this, "TIEMPO DE ESPERA AGOTADO; REVISA TU CONEXIÓN A INTERNET", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError{
                Map<String,String> params = new HashMap<>();
                params.put("method",method);
                params.put("idPV",idpv);
                return params;
            }

            @Override
            protected Map<String,DataPart> getByteData(){
                Map<String,DataPart> params = new HashMap<>();
                params.put("imagen",new DataPart(nombre, getFileDataFromDrawable(bitmap)));
                return params;
            }
        };
        try{
            volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    500000,
                    -1,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
        } catch (Exception e){
            e.printStackTrace();
        }
        Volley.newRequestQueue(this).add(volleyMultipartRequest);

    }

}
