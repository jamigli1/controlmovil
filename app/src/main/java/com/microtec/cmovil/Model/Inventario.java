package com.microtec.cmovil.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Inventario {

    String folio;
    String tipo;
    boolean isSelect;

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFolio() {
        return folio;
    }

    public String getTipo() {
        return tipo;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }


    public Map toMap() {
        Map<String, String> map = new HashMap<>();
        map.put("Folio",folio);
        map.put("Tipo",tipo);
        return map;
    }

    public ArrayList<String> toArray(){
        ArrayList<String> arreglo = new ArrayList<>();
        arreglo.add(folio);
        arreglo.add(tipo);
        return arreglo;
    }
    public JSONArray toJsonArray(){
        JSONArray myarray = new JSONArray();
        myarray.put(folio);
        myarray.put(tipo);
        return myarray;
    }

    public JSONObject toJsonObject(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Folio",folio);
            jsonObject.put("Tipo",tipo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

}