package com.microtec.cmovil.Model;

public class Deposito {

    private String dis, fechaR, monto, banco, movimiento, fechaMov;

    public Deposito() {
    }

    public Deposito(String dis, String fechaR, String monto, String banco, String movimiento, String fechaMov) {
        this.dis = dis;
        this.fechaR = fechaR;
        this.monto = monto;
        this.banco = banco;
        this.movimiento = movimiento;
        this.fechaMov = fechaMov;
    }

    public String getDis() {
        return dis;
    }

    public void setDis(String dis) {
        this.dis = dis;
    }

    public String getFechaR() {
        return fechaR;
    }

    public void setFechaR(String fechaR) {
        this.fechaR = fechaR;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(String movimiento) {
        this.movimiento = movimiento;
    }

    public String getFechaMov() {
        return fechaMov;
    }

    public void setFechaMov(String fechaMov) {
        this.fechaMov = fechaMov;
    }
}
