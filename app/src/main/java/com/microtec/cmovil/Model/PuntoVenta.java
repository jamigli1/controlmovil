package com.microtec.cmovil.Model;

public class PuntoVenta {

    private String id, nombre, coordendas;
    private String codigo, estado, tipo, subtipo;
    private String titularid, fecha, total, folio;

    public PuntoVenta() {
    }

    public PuntoVenta(String id, String nombre, String coordendas, String codigo, String estado, String tipo, String subtipo, String titularid, String fecha, String total, String folio) {
        this.id = id;
        this.nombre = nombre;
        this.coordendas = coordendas;
        this.codigo = codigo;
        this.estado = estado;
        this.tipo = tipo;
        this.subtipo = subtipo;
        this.titularid = titularid;
        this.fecha = fecha;
        this.total = total;
        this.folio = folio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCoordendas() {
        return coordendas;
    }

    public void setCoordendas(String coordendas) {
        this.coordendas = coordendas;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSubtipo() {
        return subtipo;
    }

    public void setSubtipo(String subtipo) {
        this.subtipo = subtipo;
    }

    public String getTitularid() {
        return titularid;
    }

    public void setTitularid(String titularid) {
        this.titularid = titularid;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }
}
