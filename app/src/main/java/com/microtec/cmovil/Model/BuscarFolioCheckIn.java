package com.microtec.cmovil.Model;

public class BuscarFolioCheckIn {

    private String uno;
    private String uno_a;
    private String dos;
    private String dos_a;
    private String tres;
    private String tres_a;

    public BuscarFolioCheckIn(String uno, String uno_a, String dos, String dos_a, String tres, String tres_a) {
        this.uno = uno;
        this.uno_a = uno_a;
        this.dos = dos;
        this.dos_a = dos_a;
        this.tres = tres;
        this.tres_a = tres_a;
    }

    public BuscarFolioCheckIn() {
    }

    public String getUno() {
        return uno;
    }

    public String getUno_a() {
        return uno_a;
    }

    public String getDos() {
        return dos;
    }

    public String getDos_a() {
        return dos_a;
    }

    public String getTres() {
        return tres;
    }

    public String getTres_a() {
        return tres_a;
    }

    public void setUno(String uno) {
        this.uno = uno;
    }

    public void setUno_a(String uno_a) {
        this.uno_a = uno_a;
    }

    public void setDos(String dos) {
        this.dos = dos;
    }

    public void setDos_a(String dos_a) {
        this.dos_a = dos_a;
    }

    public void setTres(String tres) {
        this.tres = tres;
    }

    public void setTres_a(String tres_a) {
        this.tres_a = tres_a;
    }
}
