package com.microtec.cmovil.Model;

import java.io.Serializable;

public class Garantia implements Serializable {

    private String folio, imei, descripcion, status;
    private String color, fcaja, marca, modelo;
    private String fecha, fventa, fcompra, factelcel;
    private String facmicro, telefono, email, fallo;
    private String checklist, propiedad, historial, comentarios;

    public Garantia() {
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFcaja() {
        return fcaja;
    }

    public void setFcaja(String fcaja) {
        this.fcaja = fcaja;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getFventa() {
        return fventa;
    }

    public void setFventa(String fventa) {
        this.fventa = fventa;
    }

    public String getFcompra() {
        return fcompra;
    }

    public void setFcompra(String fcompra) {
        this.fcompra = fcompra;
    }

    public String getFactelcel() {
        return factelcel;
    }

    public void setFactelcel(String factelcel) {
        this.factelcel = factelcel;
    }

    public String getFacmicro() {
        return facmicro;
    }

    public void setFacmicro(String facmicro) {
        this.facmicro = facmicro;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFallo() {
        return fallo;
    }

    public void setFallo(String fallo) {
        this.fallo = fallo;
    }

    public String getChecklist() {
        return checklist;
    }

    public void setChecklist(String checklist) {
        this.checklist = checklist;
    }

    public String getPropiedad() {
        return propiedad;
    }

    public void setPropiedad(String propiedad) {
        this.propiedad = propiedad;
    }

    public String getHistorial() {
        return historial;
    }

    public void setHistorial(String historial) {
        this.historial = historial;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }
}
