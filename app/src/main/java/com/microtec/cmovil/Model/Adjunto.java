package com.microtec.cmovil.Model;

public class Adjunto {

    private String original;
    private String folio;
    private String id;

    public String getOriginal() {
        return original;
    }

    public String getFolio() {
        return folio;
    }

    public String getId() {
        return id;
    }

}
