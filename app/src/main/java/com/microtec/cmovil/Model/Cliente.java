package com.microtec.cmovil.Model;

public class Cliente {

    private String nombre, tipo, satcli, dis, nnego;
    private String ruta, detallista;

    public Cliente() {
    }

    public String getNnego() {
        return nnego;
    }

    public void setNnego(String nnego) {
        this.nnego = nnego;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSatcli() {
        return satcli;
    }

    public void setSatcli(String satcli) {
        this.satcli = satcli;
    }

    public String getDis() {
        return dis;
    }

    public void setDis(String dis) {
        this.dis = dis;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getDetallista() {
        return detallista;
    }

    public void setDetallista(String detallista) {
        this.detallista = detallista;
    }
}
