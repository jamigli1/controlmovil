package com.microtec.cmovil.Model;

public class Transferencia {

    String folio, fecha, destino, dest_nombre, n_equipos, dias;
    String imei, marca, modelo, origen;

    public Transferencia() {
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getDest_nombre() {
        return dest_nombre;
    }

    public void setDest_nombre(String dest_nombre) {
        this.dest_nombre = dest_nombre;
    }

    public String getN_equipos() {
        return n_equipos;
    }

    public void setN_equipos(String n_equipos) {
        this.n_equipos = n_equipos;
    }

    public String getDias() {
        return dias;
    }

    public void setDias(String dias) {
        this.dias = dias;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }
}
