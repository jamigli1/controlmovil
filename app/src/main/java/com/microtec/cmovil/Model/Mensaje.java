package com.microtec.cmovil.Model;

public class Mensaje {

    private String agMsj;
    private String mensaje;
    private String fechaMsg;
    private String tipomsj;
    private String statusMsg;

    public String getAgMsj() {
        return agMsj;
    }

    public String getMensaje() {
        return mensaje;
    }

    public String getFechaMsg() {
        return fechaMsg;
    }

    public String getTipomsj() {
        return tipomsj;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

}
