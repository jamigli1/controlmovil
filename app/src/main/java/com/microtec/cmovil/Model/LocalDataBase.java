package com.microtec.cmovil.Model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.microtec.cmovil.Utils.Estructura;

public class LocalDataBase extends SQLiteOpenHelper {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Estructura.TABLE_NAME + " (" +
                    Estructura._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    Estructura.COLUMN_NAME_LATITUD + TEXT_TYPE + COMMA_SEP +
                    Estructura.COLUMN_NAME_LONGITUD + TEXT_TYPE + COMMA_SEP +
                    Estructura.COLUMN_NAME_FECHA + TEXT_TYPE + COMMA_SEP +
                    Estructura.COLUMN_NAME_HORA + TEXT_TYPE + COMMA_SEP +
                    Estructura.COLUMN_NAME_ESTACIONADO + TEXT_TYPE +" )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Estructura.TABLE_NAME;

    private static final String SQL_CREATE_TBLVENTASP =
            "CREATE TABLE " + Estructura.TABLE_NAME2 + " (" +
                    Estructura._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    Estructura.COLUMN_NAME_CLIENTE + TEXT_TYPE + COMMA_SEP +
                    Estructura.COLUMN_NAME_SATCLI + TEXT_TYPE + COMMA_SEP +
                    Estructura.COLUMN_NAME_COORDENADAS + TEXT_TYPE + COMMA_SEP +
                    Estructura.COLUMN_NAME_CODIGODIS + TEXT_TYPE + COMMA_SEP +
                    Estructura.COLUMN_NAME_FECHA + TEXT_TYPE + COMMA_SEP +
                    Estructura.COLUMN_NAME_PRODUCTOS + TEXT_TYPE +" )";

    private static final String SQL_DELETE_ENTRIES2 =
            "DROP TABLE IF EXISTS " + Estructura.TABLE_NAME2;

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ControlMovil.sqlite";

    public LocalDataBase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
        sqLiteDatabase.execSQL(SQL_CREATE_TBLVENTASP);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_ENTRIES);
        sqLiteDatabase.execSQL(SQL_DELETE_ENTRIES2);
        onCreate(sqLiteDatabase);
    }

}
