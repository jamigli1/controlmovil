package com.microtec.cmovil.Model;

public class Producto {

    private String marca, modelo, precio, precioR, serie, tipo, precioK;
    private String detallista, ruta, costo;

    public Producto() {
    }

    public Producto(String marca, String modelo, String precio, String precioR, String serie, String tipo, String precioK, String detallista, String costo, String ruta) {
        this.marca = marca;
        this.modelo = modelo;
        this.precio = precio;
        this.precioR = precioR;
        this.precioK = precioK;
        this.serie = serie;
        this.tipo = tipo;
        this.detallista = detallista;
        this.costo = costo;
        this.ruta = ruta;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getPrecioR() {
        return precioR;
    }

    public void setPrecioR(String precioR) {
        this.precioR = precioR;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getPrecioK() {
        return precioK;
    }

    public void setPrecioK(String precioK) {
        this.precioK = precioK;
    }

    public String getDetallista() {
        return detallista;
    }

    public void setDetallista(String detallista) {
        this.detallista = detallista;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getCosto() {
        return costo;
    }

    public void setCosto(String costo) {
        this.costo = costo;
    }
}
