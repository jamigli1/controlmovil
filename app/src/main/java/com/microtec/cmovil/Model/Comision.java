package com.microtec.cmovil.Model;

public class Comision {

    String id, comision, distribuidor, fauto, fecha;
    String fpago, serie, status, tipo, concepto;

    public Comision() {
    }

    public Comision(String id, String comision, String distribuidor, String fauto, String fecha, String fpago, String serie, String status, String tipo, String concepto) {
        this.id = id;
        this.comision = comision;
        this.distribuidor = distribuidor;
        this.fauto = fauto;
        this.fecha = fecha;
        this.fpago = fpago;
        this.serie = serie;
        this.status = status;
        this.tipo = tipo;
        this.concepto = concepto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComision() {
        return comision;
    }

    public void setComision(String comision) {
        this.comision = comision;
    }

    public String getDistribuidor() {
        return distribuidor;
    }

    public void setDistribuidor(String distribuidor) {
        this.distribuidor = distribuidor;
    }

    public String getFauto() {
        return fauto;
    }

    public void setFauto(String fauto) {
        this.fauto = fauto;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getFpago() {
        return fpago;
    }

    public void setFpago(String fpago) {
        this.fpago = fpago;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }
}
