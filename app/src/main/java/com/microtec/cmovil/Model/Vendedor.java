package com.microtec.cmovil.Model;

public class Vendedor {

    private String uid, hora;
    private long  latitud, longitud;
    private int sendData;

    public Vendedor(String uid, String hora, long latitud, long longitud, int sendData) {
        this.uid = uid;
        this.hora = hora;
        this.latitud = latitud;
        this.longitud = longitud;
        this.sendData = sendData;
    }

    public Vendedor() {
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public long getLatitud() {
        return latitud;
    }

    public void setLatitud(long latitud) {
        this.latitud = latitud;
    }

    public long getLongitud() {
        return longitud;
    }

    public void setLongitud(long longitud) {
        this.longitud = longitud;
    }

    public int getSendData() {
        return sendData;
    }

    public void setSendData(int sendData) {
        this.sendData = sendData;
    }
}
