package com.microtec.cmovil.Model;

public class Tickets {

    private String area;
    private String agente;
    private String titulo;
    private String fecha;
    private String visto;
    private String atendido;
    private String apartado;
    private String cerrado;
    private String status;
    private String dis;
    private String folio;
    private String totadj;
    private String nombre_area;
    private String califica;
    private String calificav;


    // Getter Methods


    public String getDis() {
        return dis;
    }

    public String getCalificav() {
        return calificav;
    }

    public String getAgente() {
        return agente;
    }


    public String getFolio() {
        return folio;
    }

    public String getFecha() {
        return fecha;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getArea() {
        return area;
    }

    public String getStatus() {
        return status;
    }

    public String getVisto() {
        return visto;
    }

    public String getApartado() {
        return apartado;
    }

    public String getTotadj() {
        return totadj;
    }

    public String getNombre_area() {
        return nombre_area;
    }

    // Setter Methods

    public void setAgente(String agente) {
        this.agente = agente;
    }

    public void setDis(String dis) {
        this.dis = dis;
    }

    public void setCalificav(String calificav) {
        this.calificav = calificav;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setVisto(String visto) {
        this.visto = visto;
    }

    public void setApartado(String apartado) {
        this.apartado = apartado;
    }

    public void setTotadj(String totadj) {
        this.totadj = totadj;
    }

    public void setNombre_area(String nombre_area) {
        this.nombre_area = nombre_area;
    }

    public String getAtendido() {
        return atendido;
    }

    public String getCerrado() {
        return cerrado;
    }

    public String getCalifica() {
        return califica;
    }

}
