package com.microtec.cmovil.Model;

public class Area {

    private String area;
    private String nom_area;
    private String email_usuario_area;
    private String nombre;


    // Getter Methods

    public String getArea() {
        return area;
    }

    public String getNom_area() {
        return nom_area;
    }

    public String getEmail_usuario_area() {
        return email_usuario_area;
    }

    public String getNombre() {
        return nombre;
    }

    // Setter Methods

    public void setArea(String area) {
        this.area = area;
    }

    public void setNom_area(String nom_area) {
        this.nom_area = nom_area;
    }

    public void setEmail_usuario_area(String email_usuario_area) {
        this.email_usuario_area = email_usuario_area;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
