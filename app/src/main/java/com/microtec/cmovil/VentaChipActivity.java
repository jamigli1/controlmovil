package com.microtec.cmovil;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.microtec.cmovil.Interactor.ClientesAdapter;
import com.microtec.cmovil.Model.Cliente;
import com.microtec.cmovil.Model.Vendedor;
import com.microtec.cmovil.Utils.RecyclerTouchListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class VentaChipActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private static final String prefencesN = "userdata";

    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double lat, lon;

    private RecyclerView mList;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private List<Cliente> listClientes;

    private String cliente, satcli, micd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venta_chip);

        buildGoogleApiclient();

        mList = (RecyclerView) findViewById(R.id.rView_BuscaPV);
        listClientes = new ArrayList<>();
        mAdapter = new ClientesAdapter(getApplicationContext(),listClientes);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(),linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(mAdapter);

        mList.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                cliente = ((TextView) mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.pventa_nombre)).getText().toString();
                satcli = listClientes.get(position).getSatcli();
                micd = listClientes.get(position).getDis();
                Intent intent2 = new Intent(VentaChipActivity.this,ChipsActivity.class);
                intent2.putExtra("cliente",cliente);
                intent2.putExtra("satcli",satcli);
                intent2.putExtra("micd",micd);
                selectorMenu("Convertir a PV", "Continuar venta", intent2, cliente, position);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

    }

    public void selectorMenu(String opc1, String opc2, final Intent intent2, final String cliente, final int position){

        View mView = getLayoutInflater().inflate(R.layout.alert_dialog,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.btnDialog_opc1);
        Button btnopc2 = (Button) mView.findViewById(R.id.btnDialog_opc2);
        Button btncancel = (Button) mView.findViewById(R.id.btnDialog_cancel);

        btnopc1.setText(opc1);
        btnopc2.setText(opc2);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertirAPV();
                dialog.dismiss();
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listClientes.get(position).getDetallista().equals("0.00") || listClientes.get(position).getDetallista().equals("null")
                        || listClientes.get(position).getRuta().equals("0.00") || listClientes.get(position).getRuta().equals("null")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(VentaChipActivity.this);
                    builder.setCancelable(false);
                    builder.setTitle("¡¡Atención!!");
                    builder.setMessage("Cliente "+cliente
                            +"\nUtilidad ruta: "+listClientes.get(position).getRuta().replace("null","0.00")
                            +"\nUtilidad detallista: "+listClientes.get(position).getDetallista().replace("null","0.00")
                            +"\n[Modificaciones: Contacte a su administrador]");
                    builder.setPositiveButton("CONTINUAR", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int ix) {
                            startActivity(intent2);
                            dialog.dismiss();
                        }
                    });

                    builder.setNegativeButton("CANCELAR VENTA", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    builder.show();
                } else {
                    startActivity(intent2);
                }
            }
        });

        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void convertirAPV(){
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");
        final String uid = settings.getString("id","-1");


        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/ClientesA.php";
            final RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response.toLowerCase().contains("convertido")){
                        showResponse("MICROPAYMENT CONVERTIDO A PV");
                    } else {
                        showResponse("NO SE HA PODIDO CONVERTIR A PV");
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            showResponse("TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.");
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","convertirPV");
                    params.put("codigo_dis",dis);
                    params.put("micd_pv",micd);
                    params.put("userid",uid);
                    params.put("latitud", String.valueOf(lat));
                    params.put("longitud", String.valueOf(lon));
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void showResponse(String response){
        View mView = getLayoutInflater().inflate(R.layout.alert_response,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.al_response_ok);
        TextView textView = (TextView) mView.findViewById(R.id.al_response);

        textView.setText(response);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void onClickBuscaPV(View view){
        EditText condicion = (EditText) findViewById(R.id.busca_pv);
        listClientes.clear();
        if (!TextUtils.isEmpty(condicion.getText())){
            listarClientes(condicion.getText().toString());
        } else {
            Toast.makeText(this, "INGRESE UN VALOR A BUSCAR...", Toast.LENGTH_SHORT).show();
        }
    }

    public void listarClientes(final String condicion){
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");


        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/ClientesA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try{
                        String cad = response.substring(1,response.length());
                        JSONObject object = new JSONObject(cad);

                        if(object.length() != 0){
                            generaListV(object);
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    mAdapter.notifyDataSetChanged();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(VentaChipActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","buscarPV");
                    params.put("codigo_dis",dis);
                    params.put("condicion",condicion);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void generaListV(JSONObject jsonObject) throws JSONException {
        Iterator x = jsonObject.keys();
        JSONArray jsonArray = new JSONArray();

        String[] usuario; String[] dis; String[] satcli; String[] detallista; String[] ruta;

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(jsonObject.get(key));
        }

        usuario = (jsonArray.getString(0).replace("[","").replace("]","")).split(",");
        dis = (jsonArray.getString(1).replace("[","").replace("]","")).split(",");
        satcli = (jsonArray.getString(2).replace("[","").replace("]","")).split(",");
        detallista = (jsonArray.getString(5).replace("[","").replace("]","")).split(",");
        ruta = (jsonArray.getString(6).replace("[","").replace("]","")).split(",");

        for (int i=0; i<usuario.length; i++){
            Cliente clienteC = new Cliente();
            clienteC.setNombre(usuario[i].replace("\"",""));
            clienteC.setDis(dis[i].replace("\"",""));
            clienteC.setSatcli(satcli[i].replace("\"",""));
            clienteC.setDetallista(detallista[i].replace("\"",""));
            clienteC.setRuta(ruta[i].replace("\"",""));
            listClientes.add(clienteC);
        }

    }

    public void setCoordenadas(){
        lat = mLastLocation.getLatitude();
        lon = mLastLocation.getLongitude();
        if (lat == 0.0){
            //Log.d("COORDS","Tomadas desde la ultima act");
            String coordenadas = PreferenceManager.getDefaultSharedPreferences(this).getString("location-update-result","");
            String [] partes = coordenadas.split(",");
            lat = Double.parseDouble(partes[0]);
            lon = Double.parseDouble(partes[1]);
        }
    }

    private boolean isMyPositionSuccess = false;
    @Override
    public void onLocationChanged(Location location) {
        if (!isMyPositionSuccess) {
            mLastLocation = location;
            setCoordenadas();
            isMyPositionSuccess =true;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(500);
        mLocationRequest.setFastestInterval(500);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    protected synchronized void buildGoogleApiclient(){
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

}
