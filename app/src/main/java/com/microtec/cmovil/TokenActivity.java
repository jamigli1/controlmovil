package com.microtec.cmovil;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.microtec.cmovil.Presenter.TokenPresenter;
import com.microtec.cmovil.Presenter.TokenPresenterImpl;
import com.microtec.cmovil.Utils.AppConstants;
import com.microtec.cmovil.Utils.Utils;
import com.microtec.cmovil.View.TokenView;

public class TokenActivity extends AppCompatActivity implements TokenView {
    TokenPresenter presenter;
    TextView tvTokenView;
    TextView tvSegundosTokenView;

    String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_token);

        tvTokenView = (TextView) findViewById(R.id.tv_token_view);
        tvSegundosTokenView = (TextView) findViewById(R.id.tv_segundos_token_view);

        presenter = new TokenPresenterImpl(this);
        key = getSharedPreferences(AppConstants.SHARED_PREF, Context.MODE_PRIVATE).getString(AppConstants.PREF_MPY_KEY_AUTHENTICATOR, "");

        key = AppConstants.keyToken;

        if (key.equals("null") || (!AppConstants.imeig.equals(AppConstants.imeiToken))){
            key = "GM2TCOBVG4YDQMRSG44DMNJUNBBGYYLOMNXTENRY";
            View view = findViewById(R.id.tv_token_view);
            Snackbar.make(view, "NO SE PUEDE GENERAR UN TOKEN VALIDO, CONTACTA A TU SUPERVISOR",Snackbar.LENGTH_LONG)
                    .setDuration(6000).show();
        }

        presenter.obtenerToken(key);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                long time = System.currentTimeMillis() / 1000;
                int cont = (int) (30 - (time % 30));
                if (time % 30 == 0) {
                    presenter.obtenerToken(key);
                }
                presenter.setSecondsInView("" + cont);
                handler.postDelayed(this, 500);
            }
        }, 0);
    }

    View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            ClipboardManager clipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            String texto = ((TextView) view).getText().toString();
            ClipData clipData = ClipData.newPlainText(texto, texto);
            clipboardManager.setPrimaryClip(clipData);
            Toast.makeText(getApplicationContext(), "Copiado", Toast.LENGTH_LONG).show();
            return false;
        }
    };

    @Override
    public void onBackPressed() {
        Intent intentStartRoot = new Intent().setComponent(new ComponentName(this, MenuPrincipal.class));
        intentStartRoot.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intentStartRoot);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intentStartRoot = new Intent().setComponent(new ComponentName(this, MenuPrincipal.class));
            intentStartRoot.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intentStartRoot);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
//        presenter.obtenerToken(key);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.stopTask();
    }

    @Override
    public void setTokenInView(String token) {
        tvTokenView.setText(token);
        tvTokenView.setTextIsSelectable(true);
    }

    @Override
    public void setSecondsInView(String seconds) {
        tvSegundosTokenView.setText(seconds);
        tvSegundosTokenView.setTextIsSelectable(true);
    }

    class ObtenerTokenAsync extends AsyncTask<String, Long, String> {
        @Override
        protected String doInBackground(String... voids) {


            while (true) {
                long time = System.currentTimeMillis() / 1000;
                publishProgress(time);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            super.onProgressUpdate(values);
        }
    }
}