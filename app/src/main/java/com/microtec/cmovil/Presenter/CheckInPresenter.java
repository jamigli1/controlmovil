package com.microtec.cmovil.Presenter;

import com.microtec.cmovil.Model.BuscarFolioCheckIn;
import com.microtec.cmovil.Model.Inventario;

import java.util.List;

public interface CheckInPresenter {

    void showInfo(List<BuscarFolioCheckIn> resultado);

    void showMessage(String mensaje);

    void buscarFolioCheckIn(String folio, String tipo1, String tipo2);
    void showButtonCheckIn(boolean isCheckIn);
    void checkInOut(String folio, String comentario, String tipo, String tipo_tramite);
    void checkInOutPaquete(String folio, String comentario, String tipo, String tipo_tramite, List<Inventario> elementos);

}
