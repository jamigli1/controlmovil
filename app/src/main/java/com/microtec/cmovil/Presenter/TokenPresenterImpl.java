package com.microtec.cmovil.Presenter;

import com.microtec.cmovil.Interactor.TokenInteractor;
import com.microtec.cmovil.Interactor.TokenInteractorImpl;
import com.microtec.cmovil.View.TokenView;

public class TokenPresenterImpl implements TokenPresenter {

    TokenView view;
    TokenInteractor interactor;

    public TokenPresenterImpl(TokenView view) {
        this.view = view;
        interactor = new TokenInteractorImpl(this);
    }

    @Override
    public void obtenerToken(String key) {
        interactor.obtenerToken(key);
    }

    @Override
    public void setTokenInView(String token) {
        view.setTokenInView(token);
    }

    @Override
    public void setSecondsInView(String seconds) {
        view.setSecondsInView(seconds);
    }

    @Override
    public void stopTask() {
        interactor.stopTask();
    }
}