package com.microtec.cmovil.Presenter;

import com.microtec.cmovil.Interactor.CheckInInteractor;
import com.microtec.cmovil.Interactor.CheckInInteractorImpl;
import com.microtec.cmovil.Model.BuscarFolioCheckIn;
import com.microtec.cmovil.Model.Inventario;
import com.microtec.cmovil.View.CheckInView;

import java.util.List;

public class CheckInPresenterImpl implements CheckInPresenter {

    private CheckInView view;
    private CheckInInteractor interactor;

    public CheckInPresenterImpl(CheckInView view) {
        this.view = view;
        this.interactor = new CheckInInteractorImpl(this);
    }

    @Override
    public void showInfo(List<BuscarFolioCheckIn> resultado) {
        view.showInfo(resultado);
    }

    @Override
    public void showMessage(String mensaje) {
        view.showMessage(mensaje);
    }

    @Override
    public void buscarFolioCheckIn(String folio, String tipo1, String tipo2) {
        interactor.buscarFolioCheckIn(folio, tipo1, tipo2);
    }

    @Override
    public void showButtonCheckIn(boolean isCheckIn) {
        view.showButtonCheckIn(isCheckIn);
    }

    @Override
    public void checkInOut(String folio, String comentario, String tipo, String tipo_tramite) {
        interactor.checkInOut(folio, comentario, tipo, tipo_tramite);
    }

    @Override
    public void checkInOutPaquete(String folio, String comentario, String tipo, String tipo_tramite, List<Inventario> elementos) {
        interactor.checkInOutPaquete(folio, comentario, tipo, tipo_tramite, elementos);
    }
}