package com.microtec.cmovil.Presenter;

public interface TokenPresenter {
    void obtenerToken(String key);
    void setTokenInView(String token);
    void setSecondsInView(String seconds);void stopTask();
}