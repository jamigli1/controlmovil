package com.microtec.cmovil;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.microtec.cmovil.Interactor.ClientesAdapter;
import com.microtec.cmovil.Model.Cliente;
import com.microtec.cmovil.Utils.AppConstants;
import com.microtec.cmovil.Utils.RecyclerTouchListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class VentaTaeActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private static final String prefencesN = "userdata";

    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double lat, lon;

    private RecyclerView mList;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private List<Cliente> listClientes;

    private String cliente, satcli, micd;

    private ProgressDialog progressDialog;

    private AlertDialog.Builder respuesta;

    private FirebaseDatabase mDatabase;
    private DatabaseReference mReference;

    private int totalSaldo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_micropay);

        buildGoogleApiclient();

        mList = (RecyclerView) findViewById(R.id.rView_BuscaPV);
        listClientes = new ArrayList<>();
        mAdapter = new ClientesAdapter(getApplicationContext(),listClientes);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("ENVIANDO SOLICITUD...");

        respuesta = new AlertDialog.Builder(this);
        respuesta.setTitle("MENSAJE");
        respuesta.setCancelable(false);
        respuesta.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(),linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(mAdapter);

        mList.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                cliente = ((TextView) mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.pventa_nombre)).getText().toString();
                satcli = listClientes.get(position).getSatcli();
                micd = listClientes.get(position).getDis();

                selectorMenu(0, "INGRESE MONTO DE LA VENTA NO MAYOR A $2000\nPARA EL CLIENTE: "+cliente, 0);
                
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

    }

    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    public void onClickBuscaPV(View view){
        EditText condicion = (EditText) findViewById(R.id.busca_pv);
        listClientes.clear();
        if (!TextUtils.isEmpty(condicion.getText())){
            listarClientes(condicion.getText().toString());
        } else {
            Toast.makeText(this, "INGRESE UN VALOR A BUSCAR...", Toast.LENGTH_SHORT).show();
        }
    }

    public void listarClientes(final String condicion){
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");

        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/ClientesA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try{
                        String cad = response.substring(1,response.length());
                        JSONObject object = new JSONObject(cad);

                        if(object.length() != 0){
                            generaListV(object);
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    mAdapter.notifyDataSetChanged();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(VentaTaeActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","buscarPV");
                    params.put("codigo_dis",dis);
                    params.put("condicion",condicion);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void generaListV(JSONObject jsonObject) throws JSONException {
        Iterator x = jsonObject.keys();
        JSONArray jsonArray = new JSONArray();

        String[] usuario; String[] dis; String[] satcli;

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(jsonObject.get(key));
        }

        usuario = (jsonArray.getString(0).replace("[","").replace("]","")).split(",");
        dis = (jsonArray.getString(1).replace("[","").replace("]","")).split(",");
        satcli = (jsonArray.getString(2).replace("[","").replace("]","")).split(",");

        for (int i=0; i<usuario.length; i++){
            Cliente clienteC = new Cliente();
            clienteC.setNombre(usuario[i].replace("\"",""));
            clienteC.setDis(dis[i].replace("\"",""));
            clienteC.setSatcli(satcli[i].replace("\"",""));
            listClientes.add(clienteC);
        }

    }

    public void setCoordenadas(){
        lat = mLastLocation.getLatitude();
        lon = mLastLocation.getLongitude();
        if (lat == 0.0){
            String coordenadas = PreferenceManager.getDefaultSharedPreferences(this).getString("location-update-result","");
            String [] partes = coordenadas.split(",");
            lat = Double.parseDouble(partes[0]);
            lon = Double.parseDouble(partes[1]);
        }
    }

    public void checkMicropay(final String seleccionado, final int vendido){

        mDatabase = FirebaseDatabase.getInstance();
        mReference = mDatabase.getReference().child("online_vRuta").child(AppConstants.micdis);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        final String fecha = format.format(c.getTime());

        mReference.child("micropayments").child(seleccionado).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d("snap",dataSnapshot.toString());
                Log.d("algo", String.valueOf(dataSnapshot.getValue()));
                if (dataSnapshot.exists()) {
                    if (!String.valueOf(dataSnapshot.child("fecha").getValue()).equals("null")) {
                        int saldo = Integer.parseInt(String.valueOf(dataSnapshot.child("saldo").getValue()));
                        int total = saldo - vendido;
                        Log.d("Saldo", String.valueOf(saldo));
                        if (fecha.equals(dataSnapshot.child("fecha").getValue())) {
                            progressDialog.show();
                            if (total >= 0) {
                                totalSaldo = total;
                                checkDisponible(vendido);
                            } else {
                                //Toast.makeText(VentaTaeActivity.this, "LIMITE DE SALDO EXCEDIDO EN ESTE CLIENTE", Toast.LENGTH_SHORT).show();
                                respuesta.setMessage("LIMITE DE SALDO EXCEDIDO EN ESTE CLIENTE");
                                respuesta.show();
                            }
                        } else {
                            if (2000-vendido>=0) {
                                progressDialog.show();
                                checkDisponible(vendido);
                                mReference.child("micropayments").child(seleccionado).child("saldo").setValue(String.valueOf(2000 - vendido));
                                mReference.child("micropayments").child(seleccionado).child("fecha").setValue(fecha);
                            } else {
                                progressDialog.dismiss();
                                respuesta.setMessage("LIMITE DE SALDO EXCEDIDO EN ESTE CLIENTE");
                                respuesta.show();
                            }
                        }
                    }
                } else {
                    if (2000-vendido >= 0) {
                        progressDialog.show();
                        checkDisponible(vendido);
                        mReference.child("micropayments").child(seleccionado).child("fecha").setValue(fecha);
                        mReference.child("micropayments").child(seleccionado).child("saldo").setValue(String.valueOf(2000 - vendido));
                    } else {
                        progressDialog.dismiss();
                        respuesta.setMessage("LIMITE DE SALDO EXCEDIDO EN ESTE CLIENTE");
                        respuesta.show();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void checkDisponible(final float monto){
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");

        if(!dis.equals("-1") && !satcli.toLowerCase().contains("null")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/SaldoA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //Log.d("saldo disp",response);
                    try {
                        float disponible = Float.parseFloat(response);
                        if (disponible-monto >= 0){
                            selectorMenu(1,"VENTA DE SALDO A "+cliente+"\nPOR SEGURIDAD, INGRESA TU CONTRASEÑA", (int) monto);
                        } else {
                            respuesta.setMessage("CREDITO DISPONIBLE INSUFICIENTE: $"+disponible);
                            respuesta.show();
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","checkCreditoDisp");
                    params.put("micdis",dis);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        } else {
            progressDialog.dismiss();
            respuesta.setMessage("Error al procesar venta para este cliente.");
            respuesta.show();
        }
    }

    public void sendVenta(final float vendido, final String password){
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");
        final String user = settings.getString("usuario","-1");
        final String agente = settings.getString("agente","-1");
        final String id = settings.getString("id","-1");

        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/SaldoA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    respuesta.setMessage(response);
                    respuesta.show();
                    progressDialog.dismiss();
                    if (!response.toLowerCase().contains("error")) {
                        mReference.child("micropayments").child(micd).child("saldo").setValue(String.valueOf(totalSaldo));
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","capturaSaldo");
                    params.put("micdis",dis);
                    params.put("micdist",micd);
                    params.put("satcli",satcli);
                    params.put("agente",agente);
                    params.put("id_us",id);
                    params.put("usuario",user);
                    params.put("password",password);
                    params.put("vendido", String.valueOf(vendido));
                    return params;
                }
            };
            requestQueue.add(stringRequest);

        }
    }

    public void selectorMenu(final int tipoRequest, final String title1, final int monto){

        View mView = getLayoutInflater().inflate(R.layout.alert_dialog,null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.btnDialog_opc1);
        Button btnopc2 = (Button) mView.findViewById(R.id.btnDialog_opc2);
        Button btncancel = (Button) mView.findViewById(R.id.btnDialog_cancel);
        final EditText rfc = (EditText) mView.findViewById(R.id.et_rfc);
        TextView header = (TextView) mView.findViewById(R.id.header);
        rfc.setTextColor(Color.parseColor("#000000"));

        if (tipoRequest==0){
            rfc.setHint("INGRESE MONTO A VENDER");
            rfc.setVisibility(View.VISIBLE);
            rfc.setInputType(InputType.TYPE_CLASS_NUMBER);
        } else {
            rfc.setHint("INGRESE SU CONTRASEÑA");
            rfc.setVisibility(View.VISIBLE);
            rfc.setInputType(InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD);
        }

        btnopc1.setText(R.string.ok);
        btnopc2.setText(R.string.cancelar);
        header.setText(title1);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (tipoRequest==0) {
                    if (!TextUtils.isEmpty(rfc.getText())) {
                        if (Float.parseFloat(rfc.getText().toString()) >= 500) {
                            checkMicropay(micd, Integer.parseInt(rfc.getText().toString()));
                        } else {
                            progressDialog.dismiss();
                            respuesta.setMessage("DEBE INGRESAR UN MONTO MAYOR A $500");
                            respuesta.show();
                        }
                    } else {
                        respuesta.setMessage("DEBE INGRESAR UN MONTO A VENDER");
                        respuesta.show();
                    }
                } else if (tipoRequest==1){
                    checkSesion(rfc.getText().toString(), monto);
                }
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                progressDialog.dismiss();
            }
        });

        btncancel.setVisibility(View.GONE);

        dialog.show();
    }

    public void checkSesion(final String password, final int monto){
        RequestQueue queue = Volley.newRequestQueue(this);
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);

        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/login.php";

        final String user = settings.getString("usuario","-1");

        if (!user.equals("-1")) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("LOGIN", response);
                    try {
                        String cad = response.replace("[", "");
                        cad = cad.replace("]", "");

                        JSONObject obj = new JSONObject(cad);

                        if (obj.getString("user").contains(user)) {
                            sendVenta(monto, password);
                        } else {
                            respuesta.setMessage("CONTRASEÑA INCORRECTA, REINTENTE.");
                            respuesta.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.d("Error: ", error.getMessage());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method", "iniciar");
                    params.put("usuario", user);
                    params.put("pass", password);
                    return params;
                }
            };
            queue.add(stringRequest);
        }
    }

    private boolean isMyPositionSuccess = false;
    @Override
    public void onLocationChanged(Location location) {
        if (!isMyPositionSuccess) {
            mLastLocation = location;
            setCoordenadas();
            isMyPositionSuccess =true;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(500);
        mLocationRequest.setFastestInterval(500);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    protected synchronized void buildGoogleApiclient(){
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

}
