package com.microtec.cmovil;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.microtec.cmovil.Utils.AppConstants;
import com.microtec.cmovil.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private EditText usuario, password;
    private ProgressBar cargaLogin;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usuario = (EditText) findViewById(R.id.login_usuario);
        password = (EditText) findViewById(R.id.login_password);
        cargaLogin = (ProgressBar) findViewById(R.id.login_progressbar);

        context = this;

        if (checkSesion()){
            startActivity(new Intent(this,MenuPrincipal.class));
            finish();
        }

    }

    @Override
    protected void onStart(){
        super.onStart();
    }

    @Override
    protected void onStop(){
        super.onStop();
    }

    public boolean checkSesion(){
        SharedPreferences sesion = this.getSharedPreferences(AppConstants.userPreferences,this.MODE_PRIVATE);
        if (sesion.getString("usuario","default") != "default" && sesion.getString("id","default") != "default") {
            return true;
        }
        return false;
    }

    public void onClickLogin(View view) throws JSONException {
        if (!TextUtils.isEmpty(usuario.getText()) && !TextUtils.isEmpty(password.getText())){
            requestLogin(usuario.getText().toString(), password.getText().toString());
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Por favor llena los campos correctamente");
            builder.setCancelable(true);
            builder.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alerta = builder.create();
            alerta.show();
        }
    }

    public void requestLogin(final String user, final String pwd) {
        showProgressBar();
        RequestQueue queue = Volley.newRequestQueue(this);

        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/login.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    String cad = response.replace("[","");
                    cad = cad.replace("]","");

                    JSONObject obj = new JSONObject(cad);

                    Log.d("LOGIIIN", response);

                    if(obj.getString("user").contains(user)){
                        hideProgressBar();
                        saveSharedPref(obj.getString("id"),obj.getString("user"),obj.getString("dis"),obj.getString("nombre"),obj.getString("tipo"), obj.getString("vr"));
                        startActivity(new Intent(context,MenuPrincipal.class));
                        finish();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage("Verifique sus datos por favor.");
                        builder.setCancelable(true);
                        builder.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        hideProgressBar();
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alerta = builder.create();
                        alerta.show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d("Error: ",error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("method","iniciar");
                params.put("usuario",user);
                params.put("pass",pwd);
                return params;
            }
        };
        queue.add(stringRequest);
    }

    public void saveSharedPref(String id, String user, String dis, String agente, String tipo, String vr){

        SharedPreferences settings = this.getSharedPreferences(AppConstants.userPreferences,this.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        editor.putString("id",id);
        editor.putString("usuario",user);
        editor.putString("dis",dis);
        AppConstants.micdis = dis;
        Utils.areThereMockPermissionApps(this);
        editor.putString("agente",agente);
        editor.putString("tipo",tipo);
        editor.putString("vruta",vr);
        editor.apply();

    }

    public void showProgressBar(){
        cargaLogin.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar(){
        cargaLogin.setVisibility(View.GONE);
    }

}
