package com.microtec.cmovil;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.microtec.cmovil.Utils.AlertResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DetalleProspectoActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String prefencesN = "userdata";

    EditText nombre, titular, telefonoT, email;
    EditText colonia, copostal, calle, municipioT, ciudadT;
    Spinner estado;
    String idt, idpv;
    Location mLastLocation;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;

    Button cliente, actualizar, eliminar;

    private double lat, lon;

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_prospecto);

        nombre = (EditText) findViewById(R.id.detalle_nombre_pv);
        titular = (EditText) findViewById(R.id.detalle_nombre_titular);
        telefonoT = (EditText) findViewById(R.id.detalle_telefono);
        email = (EditText) findViewById(R.id.detalle_email);
        colonia = (EditText) findViewById(R.id.detalle_colonia);
        copostal = (EditText) findViewById(R.id.detalle_cp);
        calle = (EditText) findViewById(R.id.detalle_calle);
        municipioT = (EditText) findViewById(R.id.detalle_municipio);
        ciudadT = (EditText) findViewById(R.id.detalle_ciudad);

        cliente = (Button) findViewById(R.id.btn_convertir_dProspecto);
        actualizar = (Button) findViewById(R.id.btn_actualizar_dProspecto);
        eliminar = (Button) findViewById(R.id.btn_elimina_dProspecto);

        estado = (Spinner) findViewById(R.id.detalle_estado);
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this,R.array.estados_array, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        estado.setAdapter(arrayAdapter);

        buildGoogleApiclient();

        Bundle extras = getIntent().getExtras();
        getData(extras.getString("folio"));

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapdetalle);
        mapFragment.getMapAsync(this);
    }

    private void getData(final String folio){

        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/PVentas.php";
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final AlertResponse alertResponse = new AlertResponse(DetalleProspectoActivity.this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d("Response",response.toString());
                try{
                    String cad = response.substring(1,response.length());
                    JSONObject object = new JSONObject(cad);
                    putData(object);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(DetalleProspectoActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        alertResponse.showResponse("TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("method","verPV");
                params.put("idpv",folio);
                return params;
            }
        };
        requestQueue.add(stringRequest);

    }

    public void putData(JSONObject object) throws JSONException {
        Iterator x = object.keys();
        JSONArray jsonArray = new JSONArray();

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(object.get(key));
        }

        String nombrepv = jsonArray.getString(0).replace("[","").replace("]","");
        String nombret = jsonArray.getString(1).replace("[","").replace("]","");
        String telefono = jsonArray.getString(2).replace("[","").replace("]","");
        String estadot = jsonArray.getString(3).replace("[","").replace("]","");
        String municipio = jsonArray.getString(4).replace("[","").replace("]","");
        String ciudad = jsonArray.getString(5).replace("[","").replace("]","");
        String colonia = jsonArray.getString(6).replace("[","").replace("]","");
        String codpostal = jsonArray.getString(7).replace("[","").replace("]","");
        String calle = jsonArray.getString(8).replace("[","").replace("]","");
        idt = jsonArray.getString(10).replace("[","").replace("]","");
        idpv = jsonArray.getString(11).replace("[","").replace("]","");
        String lati = jsonArray.getString(14).replace("[","").replace("]","");
        String longi = jsonArray.getString(15).replace("[","").replace("]","");

        notNull(nombre,nombrepv);
        notNull(titular,nombret);
        notNull(telefonoT,telefono);
        notNull(municipioT,municipio);
        notNull(this.colonia,colonia);
        notNull(ciudadT,ciudad);
        notNull(this.copostal,codpostal);
        notNull(this.calle,calle);

        if (!estadot.contains("null")){
            ArrayAdapter arrayAdapter = (ArrayAdapter) estado.getAdapter();
            int position = arrayAdapter.getPosition(estadot);
            estado.setSelection(position);
        }

        if (!longi.isEmpty() && !longi.contains("null")) {

            lon = Double.parseDouble(longi);
            lat = Double.parseDouble(lati);

            LatLng sydney = new LatLng(lat, lon);
            mMap.addMarker(new MarkerOptions().position(sydney).title(nombrepv));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));

        } else {
            LatLng def = new LatLng(19.4978,-99.1269);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(def));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(0.0f));
        }
    }

    public void notNull(EditText editText, String cadena){

        if (!cadena.contains("null")){
            editText.setText(cadena);
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));

    }

    private boolean isMyPositionSuccess = false;
    @Override
    public void onLocationChanged(Location location) {
        if (!isMyPositionSuccess) {
            mLastLocation = location;
            isMyPositionSuccess =true;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    protected synchronized void buildGoogleApiclient(){
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public void onClickEliminaProspecto(View view){
        Bundle extras = getIntent().getExtras();
        String folio = extras.getString("folio");
        selectorMenu("SI","¿Desea eliminar el PV "+nombre.getText().toString()+"?",folio,0, 0);
    }

    public void onClickActualizaPV(View view){
        Bundle extras = getIntent().getExtras();
        String folio = extras.getString("folio");
        selectorMenu("SI","¿Desea actualizar el PV "+nombre.getText().toString()+"?",folio,1, 0);
    }

    public void onClickConvertir(View view){
        if (faltantes()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("TIPO DE CLIENTE:");
            builder.setTitle("CONVERSIÓN");
            builder.setPositiveButton("Punto de Venta", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    selectorMenu("OK", "Escriba su RFC", "ALGO", 2, 1);
                }
            });

            builder.setNeutralButton("Micropayment", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    selectorMenu("OK", "Escriba su RFC", "ALGO", 2, 0);
                }
            });

            builder.show();
        }
    }

    public boolean checkRequired(EditText editText){
        if (TextUtils.isEmpty(editText.getText())){
            editText.setError("CAMPO REQUERIDO");
            return false;
        }
        return true;
    }

    public boolean faltantes(){
        AlertResponse alertResponse = new AlertResponse(DetalleProspectoActivity.this);
        checkRequired(nombre);
        checkRequired(titular);
        checkRequired(telefonoT);
        if (estado.getSelectedItem() == " "){
//            Toast.makeText(this, "EL CAMPO 'ESTADO' ES REQUERIDO ", Toast.LENGTH_SHORT).show();
            alertResponse.showResponse("EL CAMPO 'ESTADO' ES REQUERIDO");
        }
        checkRequired(municipioT);
        checkRequired(colonia);
        checkRequired(copostal);
        checkRequired(calle);
        if ( checkRequired(nombre) && checkRequired(titular) && checkRequired(telefonoT) &&
                (estado.getSelectedItem() != " ") && checkRequired(municipioT) && checkRequired(colonia) &&
                checkRequired(copostal) && checkRequired(calle) ){
            return true;
        }
        return false;
    }

    public void sendConversionRequest(final String rfc, final String pv, final int subtipo){
        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/ClientesA.php";
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");
        final String agente = settings.getString("agente","-1");
        final AlertResponse alertResponse = new AlertResponse(DetalleProspectoActivity.this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("RESPUESTA",response);
                if (response.toLowerCase().contains("done")){
                    //showResponse(response.replace("null","").replace("DONE",""));
                    //Toast.makeText(DetalleProspectoActivity.this, response.replace("null","").replace("DONE",""), Toast.LENGTH_SHORT).show();
                    Intent wizard = new Intent(DetalleProspectoActivity.this, FotosWizardActivity.class);
                    wizard.putExtra("latitud",mLastLocation.getLatitude());
                    wizard.putExtra("longitud",mLastLocation.getLongitude());
                    wizard.putExtra("idpv",idpv);
                    alertResponse.showResponse(response.replace("null","").replace("DONE",""));
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(DetalleProspectoActivity.this,ProspectosActivity.class));
                            finish();
                        }
                    },2500);
                } else {
                    alertResponse.showResponse("Error al convertir este Prospecto");
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        cliente.setEnabled(true);
//                        Toast.makeText(DetalleProspectoActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        alertResponse.showResponse("TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("method","crearCliente");
                params.put("idPV",idpv);
                params.put("idT",idt);
                params.put("dis",dis);
                params.put("nombreT",titular.getText().toString());
                params.put("cel",telefonoT.getText().toString());
                params.put("email",email.getText().toString());
                params.put("edo",estado.getSelectedItem().toString());
                params.put("muni",municipioT.getText().toString());
                params.put("ciudad",ciudadT.getText().toString());
                params.put("colonia",colonia.getText().toString());
                params.put("calle",calle.getText().toString());
                params.put("next","8");
                params.put("nint","");
                params.put("cp",copostal.getText().toString());
                params.put("rfc",rfc);
                params.put("agente",agente);
                //TODO Agregar el subtipo
                params.put("subtinuevo", String.valueOf(subtipo));
                return params;
            }
        };
        try{
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    500000,
                    -1,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
        } catch (Exception e){
            e.printStackTrace();
        }
        requestQueue.add(stringRequest);
    }

    public void sendDeleteRequest(final String folio, final String pv){

        final AlertResponse alertResponse = new AlertResponse(DetalleProspectoActivity.this);

        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/TitularesA.php";
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d("RESPUESTA",response);
                if (response.contains("DONE")){
                    /*showResponse("El punto de venta "+pv+" se ha eliminado.");*/
//                    Toast.makeText(DetalleProspectoActivity.this, "", Toast.LENGTH_SHORT).show();
                    alertResponse.showResponse("El punto de venta"+pv+" se ha eliminado");
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(DetalleProspectoActivity.this,ProspectosActivity.class));
                            finish();
                        }
                    },2500);
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        eliminar.setEnabled(true);
//                        Toast.makeText(DetalleProspectoActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        alertResponse.showResponse("TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("method","eliminar");
                params.put("idPV",folio);
                return params;
            }
        };
        try{
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    500000,
                    -1,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
        } catch (Exception e){
            e.printStackTrace();
        }
        requestQueue.add(stringRequest);
    }

    public void sendEditRequest(final String pv, final String idpv, final String idT){
        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/TitularesA.php";
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        final AlertResponse alertResponse = new AlertResponse(DetalleProspectoActivity.this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d("RESPUESTA",response);
                if (response.contains("DONE")){
                    Handler handler = new Handler();
                    alertResponse.showResponse("El punto de venta "+pv+" se ha actualizado.");
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(DetalleProspectoActivity.this,ProspectosActivity.class));
                            finish();
                        }
                    },3000);
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        actualizar.setEnabled(true);
                        alertResponse.showResponse("TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("method","editar");
                params.put("idPV",idpv);
                params.put("idT",idT);
                params.put("nombrePV",nombre.getText().toString());
                params.put("nombreT",titular.getText().toString());
                params.put("telefono",telefonoT.getText().toString());
                params.put("email",email.getText().toString());
                params.put("estado",estado.getSelectedItem().toString());
                params.put("municipio",municipioT.getText().toString());
                params.put("ciudad",ciudadT.getText().toString());
                params.put("colonia",colonia.getText().toString());
                params.put("calle",calle.getText().toString());
                params.put("codigo_postal",copostal.getText().toString());
                return params;
            }
        };
        try{
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    500000,
                    -1,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
        } catch (Exception e){
            e.printStackTrace();
        }
        requestQueue.add(stringRequest);
    }

    public void selectorMenu(String opc1, final String title1, final String folio, final int tipoRequest, final int subtipo){

        View mView = getLayoutInflater().inflate(R.layout.alert_dialog,null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.btnDialog_opc1);
        Button btnopc2 = (Button) mView.findViewById(R.id.btnDialog_opc2);
        Button btncancel = (Button) mView.findViewById(R.id.btnDialog_cancel);
        final EditText rfc = (EditText) mView.findViewById(R.id.et_rfc);
        TextView header = (TextView) mView.findViewById(R.id.header);

        if (tipoRequest==2){
            rfc.setVisibility(View.VISIBLE);
        }

        btnopc1.setText(opc1);
        btnopc2.setText(R.string.cancelar);
        header.setText(title1);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (tipoRequest==0) {
                    eliminar.setEnabled(false);
                    sendDeleteRequest(folio, nombre.getText().toString());
                } else if (tipoRequest==1){
                    actualizar.setEnabled(false);
                    sendEditRequest(nombre.getText().toString(),idpv,idt);
                } else if (tipoRequest==2){
                    cliente.setEnabled(false);
                    //if (!TextUtils.isEmpty(rfc.getText())){
                        //Log.d("RFC",rfc.getText().toString());
                        sendConversionRequest(rfc.getText().toString(), nombre.getText().toString(), subtipo);
                    //} else {
                        //rfc.setError("Este campo es requerido.");
                    //}
                }
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btncancel.setVisibility(View.GONE);

        dialog.show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
