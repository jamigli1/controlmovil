package com.microtec.cmovil;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.microtec.cmovil.Interactor.ClientesAdapter;
import com.microtec.cmovil.Model.Cliente;
import com.microtec.cmovil.Model.LocalDataBase;
import com.microtec.cmovil.Utils.Estructura;
import com.microtec.cmovil.Utils.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

public class VentasPendientesActivity extends AppCompatActivity {

    private static final String prefencesN = "userdata";

    private RecyclerView mList;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private List<Cliente> listClientes;

    private ClientesAdapter filtro;

    private EditText buscaPV;

    private LocalDataBase localDataBase;
    private SQLiteDatabase sqLiteDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventas_pendientes);

        localDataBase = new LocalDataBase(this);
        sqLiteDatabase = localDataBase.getReadableDatabase();

        buscaPV = (EditText) findViewById(R.id.busca_pvpendiente);

        mList = (RecyclerView) findViewById(R.id.rView_Pendientes);
        listClientes = new ArrayList<>();
        mAdapter = new ClientesAdapter(getApplicationContext(),listClientes);
        filtro = new ClientesAdapter(getApplicationContext(),listClientes);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(),linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(filtro);

        final Intent i = new Intent(this,TerminaPendienteActivity.class);

        mList.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                final String satcli = ((TextView) mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.pventa_tipo)).getText().toString();
                final String fecha = ((TextView) mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.pventa_fecha)).getText().toString();
                final String nombre = ((TextView) mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.pventa_nombre)).getText().toString();
                i.putExtra("nombre",nombre);
                i.putExtra("satcli",satcli);
                i.putExtra("fecha",fecha);
                startActivity(i);
                finish();
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        getDataDB();

        sqLiteDatabase.close();

        mAdapter.notifyDataSetChanged();

        buscaPV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filter(editable.toString());
            }
        });

    }

    private void filter(String text){
        List<Cliente> temp = new ArrayList();
        for (Cliente cliente : listClientes){
            if (cliente.getNombre().toLowerCase().contains(text.toLowerCase())){
                temp.add(cliente);
            }
        }
        filtro.updateList(temp);
    }

    public void getDataDB(){
        Cursor response = sqLiteDatabase.rawQuery("select * from " + Estructura.TABLE_NAME2 + " where satcli != '-128'", null);

        if (response.getCount() != 0){
            while (response.moveToNext()){
                Cliente cliente = new Cliente();
                cliente.setNombre(response.getString(1));
                cliente.setDis(response.getString(2));
                cliente.setTipo(response.getString(5));
                listClientes.add(cliente);
            }
        }

    }
}
