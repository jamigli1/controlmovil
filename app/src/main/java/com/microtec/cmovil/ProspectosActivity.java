package com.microtec.cmovil;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.microtec.cmovil.Interactor.PuntosVentaAdapter;
import com.microtec.cmovil.Model.PuntoVenta;
import com.microtec.cmovil.Utils.AppConstants;
import com.microtec.cmovil.Utils.DatePickerFragment;
import com.microtec.cmovil.Utils.RecyclerTouchListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ProspectosActivity extends AppCompatActivity {

    private static final String prefencesN = "userdata";

    private RecyclerView mList;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private List<PuntoVenta> list;

    private FirebaseDatabase mDatabase;
    private DatabaseReference mReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prospectos);

        list = new ArrayList<>();
        mList = (RecyclerView) findViewById(R.id.rView_Prosp);
        mAdapter = new PuntosVentaAdapter(getApplicationContext(),list);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(mAdapter);

        mList.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                makeQuestion(position);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        listarPtoVentas();

        final Intent nuevoprospecto = new Intent(this,NuevoProspectoActivity.class);

        FloatingActionButton fab = findViewById(R.id.prospecto_nuevo);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Selector("COMPETENCIA","MICROTEC",nuevoprospecto);
            }
        });
    }

    public void makeQuestion(final int position){
        final Intent x = new Intent(this, DetalleProspectoActivity.class);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setCancelable(false);
        builder.setTitle("SELECCIONE UNA ACCIÓN");
        builder.setMessage("¿Desea agregar un recordatorio a este Prospecto o Visualizar a detalle?");
        builder.setPositiveButton("RECORDATORIO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                makeReminder(position);
            }
        });

        builder.setNegativeButton("DETALLES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                final String title1 = ((TextView) mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.pv_id_oculto)).getText().toString();
                x.putExtra("folio",title1);
                startActivity(x);
                finish();
            }
        });

        builder.setNeutralButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.show();
    }

    public void mReminderText(final String fecha, final int position){
        View mView = getLayoutInflater().inflate(R.layout.alert_image,null);
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setView(mView);
        builder.setCancelable(false);

        final Dialog dialog;
        dialog = builder.create();

        final String title1 = ((TextView) mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.pv_title)).getText().toString();

        TextView header = mView.findViewById(R.id.header);
        TextView header2 = mView.findViewById(R.id.header2);
        header.setTextColor(Color.parseColor("#ffffff"));
        header.setTextSize(16);
        Button btnopc1 = mView.findViewById(R.id.btn_img_ok);
        Button btnopc2 = mView.findViewById(R.id.btn_img_cancel);
        final EditText editText = mView.findViewById(R.id.modif_observ);

        editText.setHint("Recordatorio...");

        header.setText("Recordatorio prospecto: "+title1);
        header2.setText("Agregue el recordatorio para la fecha: "+fecha);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(editText.getText())){
                    sendToFbase(fecha, editText.getText().toString(), position);
                    dialog.dismiss();
                } else {
                    editText.setError("CAMPO REQUERIDO");
                }
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void sendToFbase(String fecha, String mensaje, int position){
        mDatabase = FirebaseDatabase.getInstance();
        mReference = mDatabase.getReference().child("online_vRuta").child(AppConstants.micdis);

        try {
            String id, nombre;
            id = ((TextView) mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.pv_id_oculto)).getText().toString();
            nombre = ((TextView) mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.pv_title)).getText().toString();

            mReference.child("recordatorios").child(fecha).child(nombre).child("id").setValue(id);
            mReference.child("recordatorios").child(fecha).child(nombre).child("mensaje").setValue(mensaje);

            Toast.makeText(this, "RECORDATORIO AGREGADO", Toast.LENGTH_SHORT).show();

        }catch (Exception e){
            Toast.makeText(this, "NO SE PUEDE ASIGNAR RECORDATORIO A ESTE PROSPECTO...", Toast.LENGTH_SHORT).show();
        }

    }

    public void makeReminder(final int position){
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                String selectedDate = "";

                if (month+1 < 10){
                    selectedDate = year+"-0"+(month+1);
                } else {
                    selectedDate = year+"-"+(month+1);
                }

                if (dayOfMonth < 10){
                    selectedDate += "-0" + dayOfMonth;
                } else {
                    selectedDate += "-" + dayOfMonth;
                }


                mReminderText(selectedDate, position);
            }
        });

        newFragment.show(ProspectosActivity.this.getSupportFragmentManager(),"datePicker");
    }

    private void listarPtoVentas(){
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String uid = settings.getString("id","-1");
        if(!uid.equals("-1")){
            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/PVentas.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //Log.d("Response",response.toString());
                    try{
                        String cad = response.substring(1,response.length());
                        JSONObject object = new JSONObject(cad);
                        if(object.getString("usuario_id").contains(uid)){
                            generaList(object);
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    mAdapter.notifyDataSetChanged();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(ProspectosActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","listar");
                    params.put("uid",uid);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }

    }

    public void generaList(JSONObject object) throws JSONException {
        Iterator x = object.keys();
        JSONArray jsonArray = new JSONArray();

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(object.get(key));
        }

        String codigo = jsonArray.getString(3).replace("[","").replace("]","");
        String nombre = jsonArray.getString(1).replace("[","").replace("]","");
        String folio = jsonArray.getString(0).replace("[","").replace("]","");
        String[] cod_part = codigo.split(",");
        String[] arr_nom = nombre.split(",");
        String[] arr_folio = folio.split(",");
        List<Integer> indices = new ArrayList<Integer>();

        for (int i=0; i<cod_part.length; i++){
            if (cod_part[i].toString().contains("null")) {
                indices.add(i);
            }
        }

        for (int i=0; i<indices.size(); i++){
            PuntoVenta puntoVenta = new PuntoVenta();
            puntoVenta.setNombre(arr_nom[indices.get(i)].replace("\"",""));
            puntoVenta.setFolio(arr_folio[indices.get(i)].replace("\"",""));
            list.add(puntoVenta);
        }
    }

    public void Selector(String opc1, String opc2, final Intent intent1){

        View mView = getLayoutInflater().inflate(R.layout.alert_dialog,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.btnDialog_opc1);
        Button btnopc2 = (Button) mView.findViewById(R.id.btnDialog_opc2);
        Button btncancel = (Button) mView.findViewById(R.id.btnDialog_cancel);
        TextView header = (TextView) mView.findViewById(R.id.header);

        btnopc1.setText(opc1);
        btnopc2.setText(opc2);
        header.setText("CREAR NUEVO PROSPECTO");

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent1.putExtra("tipotienda","comp");
                startActivity(intent1);
                finish();
                dialog.dismiss();
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent1.putExtra("tipotienda","micro");
                startActivity(intent1);
                finish();
                dialog.dismiss();
            }
        });

        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

}
