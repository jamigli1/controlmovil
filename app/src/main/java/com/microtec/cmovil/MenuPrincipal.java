package com.microtec.cmovil;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.JsonObject;
import com.microtec.cmovil.Helper.LocationRequestHelper;
import com.microtec.cmovil.Helper.LocationResultHelper;
import com.microtec.cmovil.Interactor.PuntosVentaAdapter;
import com.microtec.cmovil.Interactor.ReminderAdapter;
import com.microtec.cmovil.Interactor.TransferenciaRAdapter;
import com.microtec.cmovil.Model.LocalDataBase;
import com.microtec.cmovil.Model.Pendiente;
import com.microtec.cmovil.Model.PuntoVenta;
import com.microtec.cmovil.Service.FloatingViewService;
import com.microtec.cmovil.Service.LocationUpdatesBroadcastReceiver;
import com.microtec.cmovil.Service.LocationUpdatesIntentService;
import com.microtec.cmovil.Service.NetworkStateReceiver;
import com.microtec.cmovil.Utils.AppConstants;
import com.microtec.cmovil.Utils.Estructura;
import com.microtec.cmovil.Utils.RecyclerTouchListener;
import com.microtec.cmovil.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MenuPrincipal extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, NetworkStateReceiver.NetworkStateReceiverListener , GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String prefencesN = "userdata";

    Context context = this;

    private TextView username;
    public static TextView speed;
    public static int p = 0;
    private View header;
    private View menuG, menuV, menuRE, menuRP;

    private String arrayStr;

    private View mView;

    private TextView textView;

    public static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    //4 Minutos
    public static long UPDATE_INTERVAL = 1000*3;
    //Tiempo de actualización 2 minutos
    public static long FASTEST_UPDATE_INTERVAL = (UPDATE_INTERVAL / 2);
    //Tiempo maximo de espera coincide con actualización
    public static long MAX_WAIT_TIME = UPDATE_INTERVAL;

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;

    private FirebaseDatabase mDatabase;
    private DatabaseReference mReference;

    private RecyclerView mList;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;

    private String [] sucursales;

    //Network
    private NetworkStateReceiver networkStateReceiver;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSucursales();
        Utils.getTokenKey(context);

//        Log.d("MUNDO", String.valueOf(sucursales.size()));

        // OCULTAR VISTA EN MULTI-TASKING
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        header = ((NavigationView) findViewById(R.id.nav_view)).getHeaderView(0);

        menuG = findViewById(R.id.menu_gerente);
        menuV = findViewById(R.id.menu_vruta);
        menuRE = findViewById(R.id.menu_reclutador);
        menuRP = findViewById(R.id.menu_repartidor);

        mView = getLayoutInflater().inflate(R.layout.content_recordatorios,null);
        mList = (RecyclerView) mView.findViewById(R.id.rView_recordatorios);

        SharedPreferences userData = this.getSharedPreferences(AppConstants.userPreferences, this.MODE_PRIVATE);

        AppConstants.context = context;
        AppConstants.micdis = userData.getString("dis","-1");


        Utils.obtenerIMEI(context, MenuPrincipal.this);

        Log.d("IMEI GG", AppConstants.imeig);

        checkVersion();
        guardarEnBD();
        checkFecha();

        // TODO REVISAR ESTE PROBLEMA

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {

            try {

                new AlertDialog.Builder(context)
                        .setTitle("Permisos")
                        .setMessage("Por favor activa la superposición de CMovil. Gracias.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                askPermission();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert).show();
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        //NetworkService
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        //Cambia nombre de usuario en el header

        String user = userData.getString("usuario", "-1");
        username = (TextView) header.findViewById(R.id.menuprincipal_usuario);
        username.setText(user.toUpperCase());

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


        //todo verifica esto
        updateJornada(navigationView);
        checkVrType(navigationView);

        //Inicia tracking
        if (!checkPermissions()) {
            requestPermissions();
        }

        SharedPreferences settings = this.getSharedPreferences(AppConstants.activityData, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        int jpausada = settings.getInt("jpausada",-1);
        editor.putInt("jpausada",jpausada);

        buildGoogleApiClient();
        //if (Utils.areThereMockPermissionApps(this)||Utils.isMockSettingsON(this)){
        ArrayList<String> packs = Utils.areThereMockPermissionApps(this);

        Log.d("array",packs.toString());

        checkDatabase(packs);


    }

    public void checkVrType(NavigationView navigationView){
        SharedPreferences userData = this.getSharedPreferences(AppConstants.userPreferences, this.MODE_PRIVATE);
        String vruta = userData.getString("vruta","VR");

        Menu menu = navigationView.getMenu();
        MenuItem ventas = menu.findItem(R.id.nav_ventas);
        MenuItem ventasP = menu.findItem(R.id.nav_vPendientes);
        MenuItem comis = menu.findItem(R.id.nav_comisiones);
        MenuItem vtachips = menu.findItem(R.id.nav_chips);
        MenuItem vtatae = menu.findItem(R.id.nav_microp);
        MenuItem garantias = menu.findItem(R.id.nav_garantias);
        MenuItem cambio = menu.findItem(R.id.nav_cambiasuc);
        MenuItem depositos = menu.findItem(R.id.nav_depositos);

        if (vruta.toLowerCase().equals("ge")){
            AppConstants.vruta = false;
            menuG.setVisibility(View.VISIBLE);
            menuV.setVisibility(View.GONE);
            ventas.setVisible(false);
            ventasP.setVisible(false);
            comis.setVisible(false);
            vtachips.setVisible(false);
            vtatae.setVisible(false);
            garantias.setVisible(false);
            cambio.setVisible(true);
        } else if (vruta.toLowerCase().equals("re")) {
            AppConstants.vruta = false;
            menuG.setVisibility(View.GONE);
            menuV.setVisibility(View.GONE);
            menuRP.setVisibility(View.GONE);
            menuRE.setVisibility(View.VISIBLE);
            hideMenu(menu);
        } else if (vruta.toLowerCase().equals("rp")) {
            AppConstants.vruta = false;
            menuG.setVisibility(View.GONE);
            menuV.setVisibility(View.GONE);
            menuRE.setVisibility(View.GONE);
            menuRP.setVisibility(View.VISIBLE);
            hideMenu(menu);
            depositos.setVisible(true);
        } else {
            AppConstants.vruta = true;
            menuV.setVisibility(View.VISIBLE);
            menuG.setVisibility(View.GONE);
            ventas.setVisible(true);
            ventasP.setVisible(true);
            comis.setVisible(true);
            vtachips.setVisible(true);
            vtatae.setVisible(true);
            garantias.setVisible(true);
            cambio.setVisible(false);
        }

    }

    public void hideMenu(Menu menu){
        MenuItem ventas = menu.findItem(R.id.nav_ventas);
        MenuItem ventasP = menu.findItem(R.id.nav_vPendientes);
        MenuItem comis = menu.findItem(R.id.nav_comisiones);
        MenuItem vtachips = menu.findItem(R.id.nav_chips);
        MenuItem vtatae = menu.findItem(R.id.nav_microp);
        MenuItem garantias = menu.findItem(R.id.nav_garantias);
        MenuItem cambio = menu.findItem(R.id.nav_cambiasuc);
        MenuItem prospectos = menu.findItem(R.id.nav_prospectos);
        MenuItem pventa = menu.findItem(R.id.nav_pVenta);
        MenuItem depositos = menu.findItem(R.id.nav_depositos);
        MenuItem smart = menu.findItem(R.id.nav_smartt);
        MenuItem vpendiente = menu.findItem(R.id.nav_pendientes);
        MenuItem visitas = menu.findItem(R.id.nav_visitas);
        ventas.setVisible(false);
        ventasP.setVisible(false);
        comis.setVisible(false);
        vtachips.setVisible(false);
        vtatae.setVisible(false);
        garantias.setVisible(false);
        cambio.setVisible(false);
        prospectos.setVisible(false);
        pventa.setVisible(false);
        depositos.setVisible(false);
        smart.setVisible(false);
        vpendiente.setVisible(false);
        visitas.setVisible(false);
    }

    //TODO corrige superposicion

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void makeFloatingWidget(){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M && !isMyServiceRunning(FloatingViewService.class)) {
            startService(new Intent(MenuPrincipal.this, FloatingViewService.class));
            //finish();
        } else if (Settings.canDrawOverlays(this) && !isMyServiceRunning(FloatingViewService.class)) {
            startService(new Intent(MenuPrincipal.this, FloatingViewService.class));
            //finish();
        } else if (!Settings.canDrawOverlays(this)){
            try {
                askPermission();
            } catch (Exception e){
                e.printStackTrace();
            }
            Toast.makeText(this, "ACTIVAR PARA UN MEJOR FUNCIONAMIENTO.", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void askPermission() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + getPackageName()));
        startActivityForResult(intent, 2084);
    }

    public void checkDatabase(final ArrayList<String> packs){
        FirebaseApp.getInstance();

        mDatabase = FirebaseDatabase.getInstance();
        mReference = mDatabase.getReference().child("online_vRuta").child(AppConstants.micdis);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fecha = format.format(c.getTime());

        try{
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(getPackageName(),0);
            mReference.child("version").setValue(packageInfo.versionName+" "+fecha);
            //mReference.child("simulador_gps").setValue(Utils.areThereMockPermissionApps(getApplicationContext()));

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChild("latitud")){
                    mReference.child("latitud").setValue("0");
                    mReference.child("longitud").setValue("0");
                    mReference.child("latUb").setValue("0");
                    mReference.child("lonUb").setValue("0");
                    mReference.child("uid").setValue(AppConstants.micdis);
                    mReference.child("hora").setValue("00:00:00");
                    mReference.child("horaUb").setValue("00:00:00");
                    mReference.child("bandera").child("sendData").setValue("nulo");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mReference.child("appsFake").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (int i=0; i<packs.size(); i++){
                    if (dataSnapshot.hasChild(packs.get(i))){
                        if (!dataSnapshot.child(packs.get(i)).toString().contains("permitido")) {
                            startActivity(new Intent(context, BanUserActivity.class));
                            SharedPreferences settings = context.getSharedPreferences(AppConstants.userPreferences, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.clear();
                            editor.apply();
                            settings = context.getSharedPreferences(AppConstants.activityData, Context.MODE_PRIVATE);
                            editor = settings.edit();
                            editor.clear();
                            editor.apply();
                            finish();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    //Estados

    @Override
    protected void onStart() {
        super.onStart();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onStop() {
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    //Termina estados

    //Empieza Location

    private void createLocationRequest(){
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);

        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        mLocationRequest.setMaxWaitTime(MAX_WAIT_TIME);
    }

    private void buildGoogleApiClient(){
        if (mGoogleApiClient != null){
            return;
        }
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this,this)
                .addApi(LocationServices.API)
                .build();

        createLocationRequest();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle){
        Log.i("GPSGPS","GoogleApiCliente Connected");
    }

    private PendingIntent getPendingIntent(){
        Intent intent = new Intent(this, LocationUpdatesBroadcastReceiver.class);
        intent.setAction(LocationUpdatesBroadcastReceiver.ACTION_PROCESS_UPDATES);
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onConnectionSuspended(int i){
        final String text = "Connection suspended";
        Log.w("GPSGPS",text + ": error code " + i);
        Toast.makeText(this, "Connection suspended",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        final String text = "Exception while connecting to Google Play services";
        Log.w("GPSGPS", text + ": " + connectionResult.getErrorMessage());
        Toast.makeText(this, text,Toast.LENGTH_SHORT).show();
    }

    //Termina Location Service

    //Checa permisos
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i("GPSGPS", "Displaying permission rationale to provide additional context.");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(MenuPrincipal.this,new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSIONS_REQUEST_CODE);
                }
            });

            builder.setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            Log.i("GPSGPS", "Requesting permission");
            ActivityCompat.requestPermissions(MenuPrincipal.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i("GPSGPS", "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                Log.i("GPSGPS", "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        intent.setAction(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package",
                                BuildConfig.APPLICATION_ID, null);
                        intent.setData(uri);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });

                builder.setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if (s.equals(LocationResultHelper.KEY_LOCATION_UPDATES_RESULT)) {
            //mLocationUpdatesResultView.setText(LocationResultHelper.getSavedLocationResult(this));
            //Log.d("Dentro de la app", LocationResultHelper.getSavedLocationResult(this));
        } else if (s.equals(LocationRequestHelper.KEY_LOCATION_UPDATES_REQUESTED)) {
            //updateButtonsState(LocationRequestHelper.getRequesting(this));
        }
    }

    public void requestLocationUpdates() {

        try {
            Log.i("GPSGPS", "Starting location updates");
            LocationRequestHelper.setRequesting(this, true);
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, getPendingIntent());
        } catch (SecurityException e) {
            LocationRequestHelper.setRequesting(this, false);
            e.printStackTrace();
        }
    }

    public void removeLocationUpdates() {
        Log.i("GPSGPS", "Removing location updates");
        LocationRequestHelper.setRequesting(this, false);
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                getPendingIntent());
    }

    // <------------------- Termina permisos ------------------->

    //Inicia obtener latitud y longitud de BroadcastReceiver

    //Inician metodos onClick para los botones del menu principal

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();
        Intent i;

        if (id == R.id.nav_prospectos) {
            i = new Intent(this, ProspectosActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_garantias) {
            i = new Intent(this, MenuGarantiasActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_pVenta) {
            i = new Intent(this, PuntoVentaActivity.class);
            i.putExtra("tipovta1", 0);
            startActivity(i);
        } else if (id == R.id.nav_visitas) {
            i = new Intent(this,VisitasActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_ventas) {
            i = new Intent(this,VentasActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_vPendientes) {
            startActivity(new Intent(this,VentasPendientesActivity.class));
        } else if (id == R.id.nav_depositos) {
            Intent intent1 = new Intent(this,CapturarDepositoActivity.class);
            Intent intent2 = new Intent(this,ListarDepositosActivity.class);
            selectorMenu("Capturar depósito", "Listar mis depositos", intent1, intent2);
        } else if (id == R.id.nav_microp){
            startActivity(new Intent(this, VentaTaeActivity.class));
        } else if (id == R.id.nav_comisiones){
            i = new Intent(this,ComisionesActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_about){
            /*creaPendiente();*/
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(getPackageName(),0);
                Toast.makeText(context, "VERSIÓN: "+packageInfo.versionName, Toast.LENGTH_SHORT).show();
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        } else if (id == R.id.nav_logout){
            i = new Intent(this,LoginActivity.class);
            SharedPreferences settings = this.getSharedPreferences(AppConstants.userPreferences, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();
            editor.clear();
            editor.apply();
            settings = this.getSharedPreferences(AppConstants.activityData, Context.MODE_PRIVATE);
            editor = settings.edit();
            editor.clear();
            editor.apply();
            startActivity(i);
            this.finish();
        } else if (id == R.id.nav_iniciarjornada){
            if (item.getTitle().toString().toLowerCase().contains("iniciar")) {
                Intent myService = new Intent(this,LocationUpdatesIntentService.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(myService);
                } else {
                    startService(myService);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    makeFloatingWidget();
                }
                item.setTitle(R.string.terminar_jornada);
                SharedPreferences settings = this.getSharedPreferences(AppConstants.activityData, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                setPausa(0);
                editor.putString("jornada","Terminar jornada");
                editor.putInt("jpausada",-1);
                editor.apply();
                Toast.makeText(context, "JORNADA INICIADA", Toast.LENGTH_SHORT).show();
                requestLocationUpdates();
                eliminarBasura();
            } else {
                actualizaWidget();
                try {
                    //TODO Des/Comentar esta parte para pruebas.
                    sendPosiocionesRequest();
                } catch (Exception e){
                    e.printStackTrace();
                }
                item.setTitle(R.string.iniciar_jornada);
                setPausa(1);
                SharedPreferences settings = this.getSharedPreferences(AppConstants.userDataBase, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.clear();
                editor.apply();
                settings = this.getSharedPreferences(AppConstants.KEY_LOCATION_UPDATES_RESULT, Context.MODE_PRIVATE);
                editor = settings.edit();
                editor.clear();
                editor.apply();
                settings = this.getSharedPreferences(AppConstants.activityData,Context.MODE_PRIVATE);
                editor = settings.edit();
                editor.putString("jornada","Iniciar jornada");
                editor.putString("pausada","Pausar jornada");
                editor.putInt("jpausada",-1);
                editor.putInt("compara",1);
                editor.apply();
                Toast.makeText(context, "JORNADA TERMINADA", Toast.LENGTH_SHORT).show();
                removeLocationUpdates();
                stopService(new Intent(this,FloatingViewService.class));
                stopService(new Intent(this, LocationUpdatesIntentService.class));
            }
        } else if (id == R.id.nav_miruta){
            try {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String hora = format.format(c.getTime());
                mReference.child("latUb").setValue(LocationResultHelper.getSavedLatitude(context));
                mReference.child("lonUb").setValue(LocationResultHelper.getSavedLongitude(context));
                mReference.child("horaUb").setValue(hora);
                Toast.makeText(context, "ENVIANDO UBICACIÓN...", Toast.LENGTH_SHORT).show();
            } catch (Exception e){
                Toast.makeText(context, "ERROR AL ENVIAR, INTENTA INICIAR JORNADA", Toast.LENGTH_SHORT).show();
            }
            /*consultaGral();
            startActivity(new Intent(this,MiRutaActivity.class));*/
        } else if (id == R.id.nav_pausaruta){
            if (item.getTitle().toString().toLowerCase().contains("pausar")){
                item.setTitle("Reanudar jornada");
                Toast.makeText(this, "Se pausó la jornada.", Toast.LENGTH_SHORT).show();
                SharedPreferences settings = this.getSharedPreferences(AppConstants.activityData, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("pausada","Reanudar jornada");
                Calendar c = Calendar.getInstance();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
                String hora = simpleDateFormat.format(c.getTime());
                editor.putInt("notificacion",1);
                editor.putInt("jpausada",1);
                editor.putString("notificacionStart", hora);
                editor.apply();
            } else if (item.getTitle().toString().toLowerCase().contains("reanudar")){
                item.setTitle("Pausar jornada");
                Toast.makeText(this, "Se reanudó la jornada", Toast.LENGTH_SHORT).show();
                SharedPreferences settings = this.getSharedPreferences(AppConstants.userDataBase, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.clear();
                editor.apply();
                settings = this.getSharedPreferences(AppConstants.KEY_LOCATION_UPDATES_RESULT, Context.MODE_PRIVATE);
                editor = settings.edit();
                editor.clear();
                editor.apply();
                settings = this.getSharedPreferences(AppConstants.activityData,Context.MODE_PRIVATE);
                editor = settings.edit();
                editor.putString("pausada","Pausar jornada");
                editor.putInt("notificacion",-1);
                editor.putInt("jpausada",-1);
                editor.putInt("compara",1);
                editor.apply();
            }
        } else if (id == R.id.nav_chips){
            startActivity(new Intent(this, VentaChipActivity.class));
        } else if (id == R.id.nav_pendientes){
           startActivity(new Intent(this, MiRutaActivity.class));
        } else if (id == R.id.nav_smartt){
            startActivity(new Intent(this, PromotorActivity.class));
        } else if (id == R.id.nav_cambiasuc){
            cambioSucursal();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void cambioSucursal(){
        View mView = getLayoutInflater().inflate(R.layout.alert_sucursal,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        Log.d("MUNDOTE", String.valueOf(sucursales.length));

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.sucursal_enviar);
        Button btnopc2 = (Button) mView.findViewById(R.id.sucursal_cancel);
        final AutoCompleteTextView origen = mView.findViewById(R.id.et_origen);
        final AutoCompleteTextView destino = mView.findViewById(R.id.et_destino);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MenuPrincipal.this, android.R.layout.simple_expandable_list_item_1, sucursales);
        origen.setAdapter(adapter);
        destino.setAdapter(adapter);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(origen.getText())) {
                    if (!TextUtils.isEmpty(destino.getText())){
                        String movimiento = "Se registro un cambio de sucursal. Origen: ".concat(origen.getText().toString()).concat(" Destino: ").concat(destino.getText().toString());
                        try{
                            registraCambio(movimiento, LocationResultHelper.getSavedLatitude(MenuPrincipal.this), LocationResultHelper.getSavedLongitude(MenuPrincipal.this));
                        } catch (Exception e){
                            Toast.makeText(context, "ERROR AL REALIZAR MOVIMIENTO", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        destino.setError("Campo requerido");
                    }
                } else {
                    origen.setError("Campo requerido");
                }
                dialog.dismiss();
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void actualizaWidget(){

        getSharedPreferences(AppConstants.userDataBase, MODE_PRIVATE).edit().putString("ltlng","SIN ACTIVIDAD").apply();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                AppWidgetManager widgetManager = AppWidgetManager.getInstance(context);
                int [] widgetIds = widgetManager.getAppWidgetIds(new ComponentName(context, CMovilWidget.class));
                for (int ids : widgetIds){
                    try {
                        CMovilWidget.updateAppWidget(context, widgetManager, ids);
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, 1000);
    }

    public void setPausa(int visible){
        NavigationView navigationView = findViewById(R.id.nav_view);
        Menu menu = navigationView.getMenu();
        MenuItem pausar = menu.findItem(R.id.nav_pausaruta);

        if (visible==0){
            //visible
            pausar.setVisible(true);
        } else {
            //gone
            pausar.setVisible(false);
        }
    }

    public void onClickBtnPrspctos(View v){
        Intent i;
        i = new Intent(this,ProspectosActivity.class);
        startActivity(i);
    }

    public void onClickMpaymentRE_crear(View v){
        startActivity(new Intent(this, NuevoMicropayActivity.class));
    }

    public void onClickMpaymentRE_listar(View v){
        Intent i = new Intent(this,PuntoVentaActivity.class);
        i.putExtra("tipovta1", 3);
        startActivity(i);
    }

    public void onClickPventas(View v){
        Intent i = new Intent(this,PuntoVentaActivity.class);
        i.putExtra("tipovta1", 0);
        startActivity(i);
    }

    public void onClickVisitas(View v){
        Intent i = new Intent(this,VisitasActivity.class);
        startActivity(i);
    }

    public void onClickSmartt(View view){
        startActivity(new Intent(this, PromotorActivity.class));
    }

    public void onClickVentas(View v){
        Intent i = new Intent(this,VentasActivity.class);
        startActivity(i);
    }

    public void onClickCheckin(View v){
        startActivity(new Intent(this, CheckActivity.class));
    }

    public void onClickVentasP(View v){
        startActivity(new Intent(this,VentasPendientesActivity.class));
    }

    public void onClickVtaAccesorio(View v){
        startActivity(new Intent(this, PuntoVentaActivity.class).putExtra("tipovta1", 1));
    }

    public void onClickDepositos(View v){
        Intent intent1 = new Intent(this,CapturarDepositoActivity.class);
        Intent intent2 = new Intent(this,ListarDepositosActivity.class);
        selectorMenu("Capturar depósito", "Listar mis depositos", intent1, intent2);
    }

    public void selectorMenu(String opc1, String opc2, final Intent intent1, final Intent intent2){

        View mView = getLayoutInflater().inflate(R.layout.alert_dialog,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.btnDialog_opc1);
        Button btnopc2 = (Button) mView.findViewById(R.id.btnDialog_opc2);
        Button btncancel = (Button) mView.findViewById(R.id.btnDialog_cancel);

        btnopc1.setText(opc1);
        btnopc2.setText(opc2);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent1);
                dialog.dismiss();
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent2);
                dialog.dismiss();
        }
        });

        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void onClickMicro(View v){
        startActivity(new Intent(this, VentaTaeActivity.class));
    }

    public void onClickGarantias(View v){
        startActivity(new Intent(this, MenuGarantiasActivity.class));
    }

    public void onClickToken(View v){
        startActivity(new Intent(this, TokenActivity.class));
    }

    public void onClickComisiones(View v){
        Intent i = new Intent(this,ComisionesActivity.class);
        startActivity(i);
    }

    public void onClickChipsN(View v){
        startActivity(new Intent(this, VentaChipActivity.class));
    }

    public void onClickVisitasPendientes(View v){
        startActivity(new Intent(this,MiRutaActivity.class));
    }

    //Consulta todos los campos
    public void consultaGral(){
        LocalDataBase localDataBase;
        localDataBase = new LocalDataBase(this);

        SQLiteDatabase sqLiteDatabase = localDataBase.getReadableDatabase();

        String longitud = Estructura._ID + " LIKE 1";

        String [] columnas = {
                Estructura._ID,
                Estructura.COLUMN_NAME_LATITUD,
                Estructura.COLUMN_NAME_LONGITUD
        };

        Cursor cursor = sqLiteDatabase.query(Estructura.TABLE_NAME,columnas,longitud,null, null,null,null);

        //Log.d("contd",String.valueOf(cursor.getCount()));

        if (cursor.getCount() != 0){
            cursor.moveToFirst();
            long identificador = cursor.getLong(cursor.getColumnIndex(Estructura._ID));
            String lnn = cursor.getString(cursor.getColumnIndex(Estructura.COLUMN_NAME_LATITUD));
            //Log.d("ALMACENADOOOO----",String.valueOf(identificador));
            //Log.d("ALMACENADOOOO----",lnn);
        }

        sqLiteDatabase.close();

    }

    //Terminan los metodos onClick

    public void updateJornada(NavigationView navigationView){

        SharedPreferences userData = this.getSharedPreferences(AppConstants.activityData, this.MODE_PRIVATE);
        String jornadaS = userData.getString("jornada", "Iniciar jornada");
        String pausada = userData.getString("pausada","Pausar jornada");
        int jpausada = userData.getInt("jpausada",1);

        Menu menu = navigationView.getMenu();
        MenuItem jornada = menu.findItem(R.id.nav_iniciarjornada);
        MenuItem pausar = menu.findItem(R.id.nav_pausaruta);
        MenuItem ubicacion = menu.findItem(R.id.nav_miubicacion);
        ubicacion.setVisible(false);
        jornada.setTitle(jornadaS);
        //Log.d("estado",pausada);
        if (jornadaS.contains("Terminar") || jpausada ==1) {
            pausar.setTitle(pausada);
        } else if (jpausada == -1){
            pausar.setTitle("Pausar jornada");
        }
        if (pausar.getTitle().toString().contains("Reanudar") || jornadaS.contains("Terminar")){
            pausar.setVisible(true);
        }
        navigationView.setNavigationItemSelectedListener(this);
    }

    //Empiezan los metodos para obtener el tipo de red

    @Override
    public void networkAvailable() {
        //Log.d("ESTADO CONEXIÓN","REESTABLECIDA");

        ConnectivityManager manager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = manager.getActiveNetworkInfo();

        /*Log.d("CONEXION",ni.getTypeName());
        Log.d("CONEXION INT", String.valueOf(ni.getType()));*/

        if (ni.getType()==1){
            try {
                //TODO Des/comentar esta parte para pruebas
                sendPosiocionesRequest();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void networkUnavailable() {
        //Log.d("ESTADO CONEXIÓN","PERDIDA");
    }

    //Terminan los metodos para obtener el tipo de red

    //Envia posiciones y vacia la tabla local.

    public void sendPosiocionesRequest() throws JSONException, IOException {

        LocalDataBase localDataBase = new LocalDataBase(this);
        SQLiteDatabase sqLiteDatabase = localDataBase.getReadableDatabase();

        final JSONArray jsonArray = new JSONArray();

        ArrayList<String> datos = new ArrayList<String>();

        Cursor response = sqLiteDatabase.rawQuery("select * from "+Estructura.TABLE_NAME, null);

        if (response.getCount() != 0){
            while (response.moveToNext()){
                JSONObject object = new JSONObject();
                object.put("latitud",response.getString(1));
                object.put("longitud",response.getString(2));
                object.put("fecha",response.getString(3));
                object.put("hora",response.getString(4));
                jsonArray.put(object);

                datos.add(response.getString(1)+","+response.getString(2)+","+response.getString(3)+","+response.getString(4));

            }

            //TODO des/comentar esto

            //getFile(datos);

            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        requestPosi(jsonArray.toString());
                        Thread.sleep(9000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        sqLiteDatabase.close();

        //Log.d("BD Tamaño", String.valueOf(response.getCount()));

    }

    public void getFile(final ArrayList<String> datos) throws IOException {
        final File localFile = File.createTempFile("Posiciones","txt");

        String semana = new SimpleDateFormat("w").format(new java.util.Date());

        StorageReference storageReference = FirebaseStorage.getInstance().getReference();

        storageReference.child(AppConstants.micdis+"/posiciones/Posiciones_S"+semana+".txt").getFile(localFile)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        try {
                            sendFile(localFile,datos);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        try {
                            sendFile(localFile,datos);
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                });

    }

    public void sendFile(File posiciones, ArrayList<String> datos) throws IOException, JSONException {

        String mCurrentPath = posiciones.getAbsolutePath();
        String semana = new SimpleDateFormat("w").format(new java.util.Date());

        FileWriter write = new FileWriter(posiciones, true);

        for (int i=0; i<datos.size(); i++){
            write.append(datos.get(i));
            write.append("\n");
        }

        write.flush();
        write.close();

        Log.d("PATH",mCurrentPath);

        StorageReference mStorage = FirebaseStorage.getInstance().getReference();
        Uri file = Uri.fromFile(new File(mCurrentPath));
        mStorage = mStorage.child(AppConstants.micdis+"/posiciones/Posiciones_S"+semana+".txt");

        mStorage.putFile(file)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Log.d("FBASE","SE LOGRO");
                        Uri download = taskSnapshot.getUploadSessionUri();
                        Log.d("algo", download.toString());
                        borraDB();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("FBASE","NO SE LOGRO ALV");
                    }
                });

    }

    public void borraDB(){
        LocalDataBase localDataBase;
        localDataBase = new LocalDataBase(context);
        SQLiteDatabase sqLiteDatabase = localDataBase.getWritableDatabase();
        sqLiteDatabase.execSQL("delete from "+ Estructura.TABLE_NAME);
        sqLiteDatabase.close();
    }

    public void requestPosi(final String posiciones){
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");
        final String id_us = settings.getString("usuario","-1");

        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/PosicionesA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("RESPUESTA",response);
                    if (response.toLowerCase().contains("done")){
                        LocalDataBase localDataBase;
                        localDataBase = new LocalDataBase(context);
                        SQLiteDatabase sqLiteDatabase = localDataBase.getWritableDatabase();
                        sqLiteDatabase.execSQL("delete from "+ Estructura.TABLE_NAME);
                        sqLiteDatabase.close();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","crearPunto");
                    params.put("codigo_dis",dis);
                    params.put("usuario",id_us);
                    params.put("status_gps","");
                    params.put("tipo","0");
                    params.put("jsonC",posiciones);
                    return params;
                }
            };
            try{
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        80 * 1000,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
            } catch (Exception e){
                e.printStackTrace();
            }
            requestQueue.add(stringRequest);
        }
    }

    public void checkVersion(){

        String version = "";

        try{
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(getPackageName(),0);
            version = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        RequestQueue queue = Volley.newRequestQueue(this);

        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/login.php";

        final String finalVersion = version;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (!response.replace("null","").toLowerCase().contains(finalVersion)){
                    Toast.makeText(context, "¡DEBE ACTUALIZAR SU APLICACIÓN!", Toast.LENGTH_SHORT).show();
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            cierraSesion();
                        }
                    }, 2000);
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.d("Error: ", error.getMessage());
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("method","checkVersion");
                return params;
            }
        };
        queue.add(stringRequest);
    }

    public void cierraSesion(){
        Intent i = new Intent(this,LoginActivity.class);
        SharedPreferences settings = this.getSharedPreferences(AppConstants.userPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.apply();
        settings = this.getSharedPreferences(AppConstants.activityData, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.clear();
        editor.apply();
        startActivity(i);
        this.finish();
    }

    public void guardarEnBD(){

        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String codigo_dis = settings.getString("dis","-1");
        int bandera = settings.getInt("banBD",0);

        Log.d("bandera", String.valueOf(bandera));

        if (bandera == 0) {

            LocalDataBase localDataBase = new LocalDataBase(this);
            ContentValues contentValues = new ContentValues();
            SQLiteDatabase sqLiteDatabase = localDataBase.getWritableDatabase();

            contentValues.put(Estructura.COLUMN_NAME_CLIENTE, "-128");
            contentValues.put(Estructura.COLUMN_NAME_SATCLI, "-128");
            contentValues.put(Estructura.COLUMN_NAME_CODIGODIS, codigo_dis);
            contentValues.put(Estructura.COLUMN_NAME_COORDENADAS, "-128");
            contentValues.put(Estructura.COLUMN_NAME_FECHA, "-128");
            contentValues.put(Estructura.COLUMN_NAME_PRODUCTOS, "-128");

            sqLiteDatabase.insert(Estructura.TABLE_NAME2, null, contentValues);

            sqLiteDatabase.close();

            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("banBD",1);

        }

    }

    public void eliminarBasura(){

        String sqlConcat = " where satcli = '-128'";

        LocalDataBase localDataBase;
        localDataBase = new LocalDataBase(this);
        SQLiteDatabase sqLiteDatabase = localDataBase.getWritableDatabase();
        sqLiteDatabase.execSQL("delete from "+ Estructura.TABLE_NAME2 + " " + sqlConcat);

        sqLiteDatabase.close();

    }

    public void checkFecha(){

        mDatabase = FirebaseDatabase.getInstance();
        mReference = mDatabase.getReference().child("online_vRuta").child(AppConstants.micdis);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        final String fecha = format.format(c.getTime());

        mReference.child("checkAdeudo").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (!fecha.equals(dataSnapshot.getValue())){
//                    Log.d("valor", dataSnapshot.getValue().toString()+ " > " + fecha);
                    checkAdeudos();
                    mReference.child("checkAdeudo").setValue(fecha);
                } else {

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mReference.child("recordatorios").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(fecha)){
                    View view = findViewById(R.id.drawer_layout);
                    try {
                        Snackbar.make(view, "RECORDATORIOS PARA EL DÍA DE HOY", Snackbar.LENGTH_LONG)
                                .setAction("VER", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        showListaP(dataSnapshot, fecha);
                                    }
                                })
                                .setActionTextColor(getResources().getColor(R.color.fallo))
                                .setDuration(5000)
                                .show();
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void showListaP(DataSnapshot dataSnapshot, String fecha) {

        final ArrayList<Pendiente> lista = new ArrayList<>();

        mAdapter = new ReminderAdapter(getApplicationContext(),lista);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.setAdapter(mAdapter);

        for (DataSnapshot data : dataSnapshot.child(fecha).getChildren()){
            Pendiente pendiente = new Pendiente();
            pendiente.setNombre(data.getKey());
            for (DataSnapshot data2 : data.getChildren()){
                if (data2.getKey().equals("id")){
                    pendiente.setId(data2.getValue().toString());
                } else {
                    pendiente.setMensaje(data2.getValue().toString());
                }
            }
            lista.add(pendiente);
        }

        final Intent x = new Intent(this, DetalleProspectoActivity.class);

        mList.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                final String title1 = lista.get(position).getId();
                x.putExtra("folio",title1);
                startActivity(x);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        mAdapter.notifyDataSetChanged();

        muestraLista();

    }

    public void muestraLista(){

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        if (mView.getParent()!=null){
            ((ViewGroup) mView.getParent()).removeView(mView);
        }
        builder.setView(mView);
        builder.setCancelable(false);

        final Dialog dialog;
        dialog = builder.create();

        ImageView btnCanc = mView.findViewById(R.id.img_cerrarlay2);

        btnCanc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void checkAdeudos(){
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");

        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/login.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (!response.toLowerCase().contains("wrong")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipal.this);
                        builder.setTitle("AVISO");
                        builder.setMessage(response.replace("null",""));
                        builder.setCancelable(false);
                        builder.setNegativeButton("ACEPTAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        builder.show();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","checkAdeudos");
                    params.put("micdis",dis);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void registraCambio(final String evento, final String latitud, final String longitud){
        SharedPreferences settings = this.getSharedPreferences(prefencesN, this.MODE_PRIVATE);
        final String uid = settings.getString("id", "-1");

        if (!uid.equals("-1")) {

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VisitasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //Log.d("Response",response.toString());
                    if (response.contains("DONE")){
                        Toast.makeText(MenuPrincipal.this, "MOVIMIENTO REGISTRADO.", Toast.LENGTH_SHORT).show();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(MenuPrincipal.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();

                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String formattedDate = simpleDateFormat.format(c.getTime());

                    params.put("method", "registrarVisita");
                    params.put("fecha", formattedDate);
                    params.put("latitud",String.valueOf(latitud));
                    params.put("longitud",String.valueOf(longitud));
                    params.put("descripcion",evento);
                    params.put("user_id", uid);
                    params.put("pv","-1");
                    params.put("pendiente","404");
                    return params;
                }
            };
            try{
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        500000,
                        -1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
            } catch (Exception e){
                e.printStackTrace();
            }
            requestQueue.add(stringRequest);
        }
    }

    public void getSucursales(){

        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("usuario","-1");

        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VisitasA.php";
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Response",response);
                sucursales = response.split(",");
                Log.d("MUNDITO", String.valueOf(sucursales.length));
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("method","getSucursales");
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

}
