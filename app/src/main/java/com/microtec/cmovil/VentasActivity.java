package com.microtec.cmovil;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.microtec.cmovil.Interactor.VentasAdapter;
import com.microtec.cmovil.Model.PuntoVenta;
import com.microtec.cmovil.Utils.RecyclerTouchListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class VentasActivity extends AppCompatActivity {

    private static final String prefencesN = "userdata";

    private RecyclerView mList;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private List<PuntoVenta> ventasLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventas);

        mList = (RecyclerView) findViewById(R.id.rView_Ventas);
        ventasLista = new ArrayList<>();
        mAdapter = new VentasAdapter(getApplicationContext(),ventasLista);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(),linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(mAdapter);

        final Intent i = new Intent(this,DetalleVentaActivity.class);

        mList.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                final String title1 = ((TextView) mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.venta_folio)).getText().toString();
                i.putExtra("folio",title1);
                startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        listarVentas();
    }

    private void listarVentas(){
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");

        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VentasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //Log.d("Response",response.toString());
                    try{
                        String cad = response.substring(1,response.length());
                        JSONObject object = new JSONObject(cad);

                        if(object.length() != 0){
                            generaListV(object);
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    mAdapter.notifyDataSetChanged();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(VentasActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","listarVentas");
                    params.put("uid",dis);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void generaListV(JSONObject object) throws JSONException {
        Iterator x = object.keys();
        JSONArray jsonArray = new JSONArray();

        String[] fecha; String[] total; String[] folio;
        String[] tipo; String[] nombre;

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(object.get(key));
        }

        fecha = (jsonArray.getString(0).replace("[","").replace("]","")).split(",");
        total = (jsonArray.getString(1).replace("[","").replace("]","")).split(",");
        folio = (jsonArray.getString(2).replace("[","").replace("]","")).split(",");
        tipo = (jsonArray.getString(3).replace("[","").replace("]","")).split(",");
        nombre = (jsonArray.getString(4).replace("[","").replace("]","")).split(",");

        for (int i=0; i<nombre.length; i++){
            PuntoVenta ventaReales = new PuntoVenta();
            ventaReales.setNombre(nombre[i].replace("\"",""));
            ventaReales.setTotal(total[i].replace("\"","").concat(" productos"));
            ventaReales.setFecha(fecha[i].replace("\"",""));
            ventaReales.setTipo(tipo[i].replace("\"",""));
            ventaReales.setFolio(folio[i].replace("\"",""));
            ventasLista.add(ventaReales);
        }

    }
}
