package com.microtec.cmovil;

import android.app.ActivityManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.view.Menu;
import android.widget.RemoteViews;

import com.microtec.cmovil.Service.LocationUpdatesIntentService;
import com.microtec.cmovil.Utils.AppConstants;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Implementation of App Widget functionality.
 */
public class CMovilWidget extends AppWidgetProvider {

    public static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                       int appWidgetId) throws PackageManager.NameNotFoundException {

        Intent intent = new Intent(context, MenuPrincipal.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.cmovil_widget);
        views.setOnClickPendingIntent(R.id.appwidget_text, pendingIntent);
        views.setOnClickPendingIntent(R.id.sesion_activa, pendingIntent);
        views.setOnClickPendingIntent(R.id.version_ult, pendingIntent);
        views.setOnClickPendingIntent(R.id.appwidget_text2, pendingIntent);

        String data = context.getSharedPreferences(AppConstants.userDataBase,Context.MODE_PRIVATE).getString("ltlng","INACTIVO");

        String activo = "";
        PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(),0);
        if (!data.contains("INACTIVO")) {
            try {
                Calendar hora = Calendar.getInstance();
                SimpleDateFormat format = new SimpleDateFormat("HH:mm");
                Date date = format.parse(data);
                String fecha = format.format(hora.getTime());
                String fecha2 = format.format(date);
                if (fecha.equals(fecha2)) {
                    activo = "ACTIVO";
                } else {
                    activo = "INACTIVO";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            activo = "INACTIVO";
        }

        if (isMyServiceRunning(LocationUpdatesIntentService.class, context)){
            views.setImageViewResource(R.id.indicador_widget, R.drawable.w_activo);
            views.setTextViewText(R.id.sesion_activa, activo);
            views.setTextViewText(R.id.version_ult, "Versión: "+packageInfo.versionName);
            views.setTextViewText(R.id.appwidget_text, data);
        } else {
            views.setImageViewResource(R.id.indicador_widget, R.drawable.w_inactivo);
            views.setTextViewText(R.id.sesion_activa, activo);
            views.setTextViewText(R.id.version_ult, "Versión: "+packageInfo.versionName);
            views.setTextViewText(R.id.appwidget_text, "SIN ACTIVIDAD");
        }

        appWidgetManager.updateAppWidget(appWidgetId, views);

    }

    public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            try {
                updateAppWidget(context, appWidgetManager, appWidgetId);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

