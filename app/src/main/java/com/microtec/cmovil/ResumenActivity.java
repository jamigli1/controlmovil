package com.microtec.cmovil;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.microtec.cmovil.Interactor.ProductosAdapter;
import com.microtec.cmovil.Model.Producto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResumenActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener{

    private static final String prefencesN = "userdata";

    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double lat, lon;

    private RecyclerView mList;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private ProductosAdapter filtro;
    private List<Producto> pProductos;

    FloatingActionButton btnTermina;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resumen);

        buildGoogleApiclient();

        mList = (RecyclerView) findViewById(R.id.rView_Resumen);
        pProductos = new ArrayList<>();
        filtro = new ProductosAdapter(getApplicationContext(),pProductos);

        btnTermina = (FloatingActionButton) findViewById(R.id.btn_termina_resumen);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(),linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(filtro);

        try {
            listarProductos();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void listarProductos() throws JSONException {
        Bundle extras = getIntent().getExtras();
        String json = extras.getString("json");

        JSONArray jsonArray = new JSONArray(json);

        for (int i=0; i<jsonArray.length(); i++){
            Producto producto = new Producto();
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            producto.setMarca(jsonObject.getString("marca"));
            producto.setModelo(jsonObject.getString("modelo"));
            producto.setSerie(jsonObject.getString("serie"));
            producto.setPrecio(jsonObject.getString("precio"));
            producto.setPrecioR(jsonObject.getString("precioR"));
            pProductos.add(producto);
        }
    }

    public void onClickConfirmaVentaTodos(View view){

        btnTermina.setVisibility(View.GONE);

        Bundle extras = getIntent().getExtras();
        String json = extras.getString("json");
        String tipo = extras.getString("tipoDeVenta","ninguna");

        if (tipo.equals("prodchips")){
            selectorMenu("Seleccione método de pago",2,json);
            //Log.d("TIPO VENTA","ENTRÓ CON TIPO PROD Y CHIPS");
        } else if (tipo.equals("solochips")){
            selectorMenu("Seleccione método de pago",2,json);
        } else if (tipo.equals("soloacc")){
            selectorMenu("Seleccione método de pago",2,json);
        } else if (tipo.equals("ninguna")){
            Toast.makeText(this, "Error al guardar su venta, reintente.", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this,MenuPrincipal.class));
            finish();
        }

    }

    public void sendVentaProdChips(final String jsonProductos, final String metodo, final String tarjeta){

        //Log.d("Coordenadas",""+lat+" "+lon);

        Bundle extras = getIntent().getExtras();
        final String satcli = extras.getString("satcli","ninguno");

        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");
        final String agente = settings.getString("agente","-1");
        final String id_us = settings.getString("id","-1");

        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VentasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //Log.d("RESPUESTA",response);
                    showResponse(response.replace("null",""));
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            btnTermina.setVisibility(View.VISIBLE);
                            Toast.makeText(ResumenActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","guardarVenta");
                    params.put("productos",jsonProductos);
                    params.put("satcli",satcli);
                    params.put("codigo_dis",dis);
                    params.put("agente",agente);
                    params.put("id_us",id_us);
                    params.put("mPago",metodo);
                    params.put("nTarjeta",tarjeta);
                    params.put("latitud", String.valueOf(lat));
                    params.put("longitud", String.valueOf(lon));
                    return params;
                }
            };
            try{
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        500000,
                        -1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
            } catch (Exception e){
                e.printStackTrace();
            }
            requestQueue.add(stringRequest);
        }
    }

    public void sendVentaAccesorios(final String jsonProductos, final String metodo, final String tarjeta){
        Bundle extras = getIntent().getExtras();
        final String satcli = extras.getString("satcli","ninguno");

        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");
        final String agente = settings.getString("agente","-1");
        final String id_us = settings.getString("id","-1");


        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VentasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("RESPUESTA",response);
                    showResponse(response.replace("null",""));
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            btnTermina.setVisibility(View.VISIBLE);
                            Toast.makeText(ResumenActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","guardarVentaAccesorios");
                    params.put("productos",jsonProductos);
                    params.put("satcli",satcli);
                    params.put("codigo_dis",dis);
                    params.put("agente",agente);
                    params.put("id_us",id_us);
                    params.put("mPago",metodo);
                    params.put("nTarjeta",tarjeta);
                    params.put("latitud", String.valueOf(lat));
                    params.put("longitud", String.valueOf(lon));
                    return params;
                }
            };
            try{
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        500000,
                        -1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
            } catch (Exception e){
                e.printStackTrace();
            }
            requestQueue.add(stringRequest);
        }
    }

    public void sendVentaSolChips(final String jsonProductos, final String metodo, final String tarjeta){

        Bundle extras = getIntent().getExtras();
        final String satcli = extras.getString("satcli","");
        final String detallista = extras.getString("b_detallista","");
        final String costo = extras.getString("b_costo","");
        final String ruta = extras.getString("b_ruta","");
        final String micd = extras.getString("micd","");
        final String promo = extras.getString("promo_doble","0");

        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");
        final String agente = settings.getString("agente","-1");
        final String id_us = settings.getString("id","-1");

        /*Log.d("PROMOCION", promo);

        Log.d("satcli",satcli);
        Log.d("detallista",detallista);
        Log.d("costo",costo);
        Log.d("ruta",ruta);
        Log.d("micd",micd);*/

        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VentasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    showResponse(response.replace("null",""));
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            btnTermina.setVisibility(View.VISIBLE);
                            Toast.makeText(ResumenActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","guardarVenta2");
                    params.put("productos",jsonProductos);
                    params.put("satcli",satcli);
                    params.put("codigo_dis",dis);
                    params.put("agente",agente);
                    params.put("mPago",metodo);
                    params.put("nTarjeta",tarjeta);
                    params.put("id_us",id_us);
                    params.put("u_costo",costo);
                    params.put("u_detallista",detallista);
                    params.put("u_ruta",ruta);
                    params.put("micMicro",micd);
                    params.put("latitud", String.valueOf(lat));
                    params.put("longitud", String.valueOf(lon));
                    params.put("promo_venta",promo);
                    return params;
                }
            };

            try{
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        500000,
                        -1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
            } catch (Exception e){
                e.printStackTrace();
            }

            requestQueue.add(stringRequest);
        }

    }

    public void showResponse(String response){
        View mView = getLayoutInflater().inflate(R.layout.alert_response,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        dialog.setCancelable(false);

        Button btnopc1 = (Button) mView.findViewById(R.id.al_response_ok);
        TextView textView = (TextView) mView.findViewById(R.id.al_response);

        textView.setText(response);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startActivity(new Intent(ResumenActivity.this,MenuPrincipal.class));
                finish();
            }
        });

        dialog.show();

    }

    public void selectorMenu(String titulo, final int tipo, final String jsonproductos){

        Bundle extras = getIntent().getExtras();
        final String venta = extras.getString("tipoDeVenta");

        View mView = getLayoutInflater().inflate(R.layout.alert_select_producto,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.btn_nwProd_ok);
        final Button btnopc2 = (Button) mView.findViewById(R.id.btn_nwProd_cancel);
        TextView textView = (TextView) mView.findViewById(R.id.nwprod_header);
        final EditText editText = (EditText) mView.findViewById(R.id.new_prod_cant);

        textView.setText(titulo);

        if (tipo == 1) {
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        } else if (tipo == 2){
            editText.setVisibility(View.GONE);
            btnopc1.setText("Efectivo");
            btnopc2.setText("Transferencia");
        }

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (venta.equals("prodchips")) {
                    //Log.d("venta","productos y chips");
                    if (tipo == 2) {
                        sendVentaProdChips(jsonproductos, "Efectivo", "");
                        dialog.dismiss();
                    } else if (tipo == 1) {
                        sendVentaProdChips(jsonproductos, "Transferencia", editText.getText().toString());
                        dialog.dismiss();
                    }
                } else if (venta.equals("solochips")){
                    //Log.d("venta","chips");
                    if (tipo == 2) {
                        sendVentaSolChips(jsonproductos, "Efectivo", "");
                        dialog.dismiss();
                    } else if (tipo == 1) {
                        sendVentaSolChips(jsonproductos, "Transferencia", editText.getText().toString());
                        dialog.dismiss();
                    }
                } else if (venta.equals("soloacc")){
                    //Log.d("venta","chips");
                    if (tipo == 2) {
                        sendVentaAccesorios(jsonproductos, "Efectivo", "");
                        dialog.dismiss();
                    } else if (tipo == 1) {
                        sendVentaAccesorios(jsonproductos, "Transferencia", editText.getText().toString());
                        dialog.dismiss();
                    }
                }
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (venta.equals("prodchips")) {
                    //Log.d("venta","productos y chips");
                    if (tipo == 2) {
                        selectorMenu("Ingrese numero de tarjeta", 1, jsonproductos);
                        dialog.dismiss();
                    } else if (tipo == 1) {
                        dialog.cancel();
                    }
                } else if (venta.equals("solochips")){
                    //Log.d("venta","chips");
                    if (tipo == 2) {
                        selectorMenu("Ingrese numero de tarjeta", 1, jsonproductos);
                        dialog.dismiss();
                    } else if (tipo == 1) {
                        dialog.cancel();
                    }
                }
            }
        });

        dialog.show();
    }

    public void setCoordenadas(){
        lat = mLastLocation.getLatitude();
        lon = mLastLocation.getLongitude();
        if (lat == 0.0){
            //Log.d("COORDS","Tomadas desde la ultima act");
            String coordenadas = PreferenceManager.getDefaultSharedPreferences(this).getString("location-update-result","");
            String [] partes = coordenadas.split(",");
            lat = Double.parseDouble(partes[0]);
            lon = Double.parseDouble(partes[1]);
        }
    }

    private boolean isMyPositionSuccess = false;
    @Override
    public void onLocationChanged(Location location) {
        if (!isMyPositionSuccess) {
            mLastLocation = location;
            setCoordenadas();
            isMyPositionSuccess =true;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(500);
        mLocationRequest.setFastestInterval(500);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    protected synchronized void buildGoogleApiclient(){
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

}
