package com.microtec.cmovil.Helper;

import android.app.Application;
import android.content.Context;

import com.microtec.cmovil.Utils.AppConstants;

public class ControlMovil extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        ControlMovil.context = this;
    }

    public static Context getAppContext(){
        return AppConstants.context;
    }

}
