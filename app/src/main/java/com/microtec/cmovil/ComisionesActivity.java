package com.microtec.cmovil;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.microtec.cmovil.Utils.AlertResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ComisionesActivity extends AppCompatActivity {

    private static final String prefencesN = "userdata";
    private ProgressBar cargaComision;

    String fechaDetalle;

    private TextView fechacomision;
    private TextView micropPuedes, micropYa;
    private TextView microfPuedes, microfYa;
    private TextView franPuedes, franYa;
    private Button btnAnterior, btnSiguiente;
    private TextView chips;
    private int index = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comisiones);

        initElementos();

    }

    public void onClickAnterior(View view){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH,1);
        calendar.add(Calendar.MONTH, -index);
        index++;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        String fechaRequest = simpleDateFormat1.format(calendar.getTime());
        String fecha = simpleDateFormat.format(calendar.getTime());
        disableNext();
        fechacomision.setText(fecha);
        fechaDetalle = fechaRequest;
        sendComisionesRequest(fechaRequest);
    }

    public void onClickSiguiente(View view){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -index);
        calendar.set(Calendar.DAY_OF_MONTH,1);
        calendar.add(Calendar.MONTH, 2);
        index--;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        String fechaRequest = simpleDateFormat1.format(calendar.getTime());
        String fecha = simpleDateFormat.format(calendar.getTime());
        disableNext();
        fechacomision.setText(fecha);
        fechaDetalle = fechaRequest;
        sendComisionesRequest(fechaRequest);
    }

    public void disableNext(){
        if (index==1){
            btnSiguiente.setVisibility(View.GONE);
        } else {
            btnSiguiente.setVisibility(View.VISIBLE);
        }
    }

    public void initElementos(){

        cargaComision = (ProgressBar) findViewById(R.id.comisiones_progressbar);

        btnAnterior = (Button) findViewById(R.id.btn_comis_anterior);
        btnSiguiente = (Button) findViewById(R.id.btn_comis_siguiente);
        fechacomision = (TextView) findViewById(R.id.fecha_comisiones);

        micropPuedes = (TextView) findViewById(R.id.deposito_microp_montoP);
        micropYa = (TextView) findViewById(R.id.deposito_monto_microp_montoY);
        microfPuedes = (TextView) findViewById(R.id.deposito_microf_microfP);
        microfYa = (TextView) findViewById(R.id.deposito_microf_microfY);
        franPuedes = (TextView) findViewById(R.id.deposito_franquicia_franqP);
        franYa = (TextView) findViewById(R.id.deposito_franquicia_franqY);
        chips = findViewById(R.id.deposito_chips);

        btnSiguiente.setVisibility(View.GONE);

        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH,1);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String fecha = simpleDateFormat.format(c.getTime());

        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        String fechaRequest = simpleDateFormat1.format(c.getTime());

        //Log.d("fechaRequest",fechaRequest);

        fechaDetalle = fechaRequest;

        fechacomision.setText(fecha);

        sendComisionesRequest(fechaRequest);

    }

    public void sendComisionesRequest(final String fecha){
        showProgressBar();
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String agente = settings.getString("agente","-1");
        final String dis = settings.getString("dis","-1");

        final AlertResponse alertResponse = new AlertResponse(ComisionesActivity.this);

        if(!agente.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/ComisionesA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try{
                        JSONObject object = new JSONObject(response);
                        if(object.length() != 0){
                            checkResponse(object);
                            hideProgressBar();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideProgressBar();
//                            Toast.makeText(ComisionesActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                            alertResponse.showResponse("TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET");
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","getComisiones");
                    params.put("agente",agente);
                    params.put("dis",dis);
                    params.put("fecha",fecha);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void checkResponse(JSONObject jsonObject) throws JSONException {
        micropPuedes.setText("$"+jsonObject.getString("mp"));
        micropYa.setText("$"+jsonObject.getString("mp_a"));
        microfPuedes.setText("$"+jsonObject.getString("mf"));
        microfYa.setText("$"+jsonObject.getString("mf_a"));
        franPuedes.setText("$"+jsonObject.getString("fran"));
        franYa.setText("$"+jsonObject.getString("fran_a"));
        if (!jsonObject.get("chips").toString().equals("null")) {
            chips.setText("$" + jsonObject.get("chips"));
        }
    }

    public void verDetalle(String tipo){
        Intent detalle = new Intent(this, DetalleComisionesActivity.class);
        detalle.putExtra("fecha",fechaDetalle);
        detalle.putExtra("tipo",tipo);
        startActivity(detalle);
    }

    public void onClickMicrop(View view){
        verDetalle("MP");
    }

    public void onClickMicrof(View view){
        verDetalle("MF");
    }

    public void onClickFran(View view){
        verDetalle("FRAN");
    }

    public void showProgressBar(){
        cargaComision.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar(){
        cargaComision.setVisibility(View.GONE);
    }

}
