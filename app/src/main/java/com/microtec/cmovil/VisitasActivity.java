package com.microtec.cmovil;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.microtec.cmovil.Interactor.VisitasAdapter;
import com.microtec.cmovil.Model.Visita;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class VisitasActivity extends AppCompatActivity  {

    private static final String prefencesN = "userdata";

    private RecyclerView mList;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private List<Visita> oVisitas;

    private double latitud, longitud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitas);

        mList = (RecyclerView) findViewById(R.id.rView_Visitas);
        oVisitas = new ArrayList<>();
        mAdapter = new VisitasAdapter(getApplicationContext(),oVisitas);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(),0);

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(mAdapter);

        listarVisitas();
    }

    public void listarVisitas(){

        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String uid = settings.getString("id","-1");


        if(!uid.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VisitasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //Log.d("Response",response.toString());
                    try{
                        String cad = response.substring(1,response.length());
                        JSONObject object = new JSONObject(cad);

                        if(object.length() != 0){
                            generaListV(object);
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    mAdapter.notifyDataSetChanged();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(VisitasActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","listarVisitas");
                    params.put("uid",uid);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }

    }

    public void generaListV(JSONObject objecto) throws JSONException {
        Iterator x = objecto.keys();
        JSONArray jsonArray = new JSONArray();

        String[] nombre; String[] fecha; String[] descripcion;

        while(x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(objecto.get(key));
        }

        nombre = (jsonArray.getString(0).replace("[","").replace("]","")).split(",");
        descripcion = (jsonArray.getString(1).replace("[","").replace("]","")).split(",");
        fecha = (jsonArray.getString(2).replace("[","").replace("]","")).split(",");

        for (int i=0; i<nombre.length; i++){
            Visita ovisitas = new Visita();
            String[] hora = fecha[i].split(" ");
            ovisitas.setNombre(nombre[i].replace("\"",""));
            ovisitas.setHora(hora[1].replace("\"",""));
            ovisitas.setFecha(hora[0].replace("\"",""));
            ovisitas.setDescripcion(descripcion[i].replace("\"",""));
            oVisitas.add(ovisitas);
        }

    }

    public void onClickNewVisita(View v){
        Intent i = new Intent(this, NuevaVisitaActivity.class);
        startActivity(i);
        finish();
    }
}
