package com.microtec.cmovil;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.transition.Slide;
import android.support.transition.Transition;
import android.support.transition.TransitionManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.microtec.cmovil.Utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class CapturaGarantiasActivity extends AppCompatActivity implements View.OnClickListener {

    private String reingreso = "", ffc = "",
            imeinuevo = "", folio = "",
            detalles = "", aplica = "",
            dVta = "";

    private View ffc_personal, ffc_equipo;
    private TextView marca, fecha, cliente, aplicag, modelo, imei, direccion;
    private EditText email_gar, telefono_gar;
    private RadioButton rbT, rbB, rbM, rbC, rbCj, rbL, rbCd, rbSa;
    private EditText danios, fallos, comentarios;
    private RadioGroup estadofisico;
    private Handler handler;
    private int tipoimei, tiporeingreso;
    private boolean tipoffc;
    public int tipogar;

    private ProgressDialog progressDialog;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_garantias);
        initElementos();
        progressDialog.show();
        selectorMenu();
    }

    public void initElementos(){

        ffc_personal = findViewById(R.id.infopersonal);
        ffc_equipo = findViewById(R.id.infoequipo);

        //Textview layout info personal
        marca = ffc_personal.findViewById(R.id.marcacel_cambia);
        fecha = ffc_personal.findViewById(R.id.fecha_cambia);
        cliente = ffc_personal.findViewById(R.id.cliente_cambia);
        aplicag = ffc_personal.findViewById(R.id.aplica_cambia);
        modelo = ffc_personal.findViewById(R.id.modelo_cambia);
        imei = ffc_personal.findViewById(R.id.imei_cambia);
        direccion = ffc_personal.findViewById(R.id.direccion_cambia);

        //Edittext layout info personal
        telefono_gar = ffc_personal.findViewById(R.id.edit_telf_garantia);
        email_gar = ffc_personal.findViewById(R.id.edit_email_garantia);

        //Radiobuttons layout info personal
        rbT = ffc_personal.findViewById(R.id.rb_opc1);
        rbB = ffc_personal.findViewById(R.id.rb_opc2);
        rbM = ffc_personal.findViewById(R.id.rb_opc3);
        rbC = ffc_personal.findViewById(R.id.rb_opc4);
        rbCj = ffc_personal.findViewById(R.id.rb_opc5);
        rbL = ffc_personal.findViewById(R.id.rb_opc6);
        rbCd = ffc_personal.findViewById(R.id.rb_opc7);
        rbSa = ffc_personal.findViewById(R.id.rb_opc8);

        //Edittext layout info estado fisico
        danios = ffc_equipo.findViewById(R.id.edit_descripcion1);
        fallos = ffc_equipo.findViewById(R.id.edit_descripcion2);
        comentarios = ffc_equipo.findViewById(R.id.edit_comentarios);

        //Radiogroup layout info estado fisico
        estadofisico = ffc_equipo.findViewById(R.id.radiogroupestado);

        rbT.setOnClickListener(this);
        rbB.setOnClickListener(this);
        rbM.setOnClickListener(this);
        rbC.setOnClickListener(this);
        rbCj.setOnClickListener(this);
        rbL.setOnClickListener(this);
        rbCd.setOnClickListener(this);
        rbSa.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("CARGANDO...");

        rbSa.setChecked(true);
        tipoffc = false;

    }

    public boolean validaDatos(){
        int count = 0;
        if (TextUtils.isEmpty(email_gar.getText())){
            email_gar.setError("¡¡CAMPO REQUERIDO!!");
        } else {
            count++;
        }

        if (TextUtils.isEmpty(telefono_gar.getText())){
            telefono_gar.setError("¡¡CAMPO REQUERIDO!!");
        } else {
            count++;
        }

        if (count == 2){
            return true;
        }

        return false;
    }

    public boolean validaDescripcion(){

        int count = 0;

        if (TextUtils.isEmpty(danios.getText())){
            danios.setError("¡¡CAMPO REQUERIDO!!");
        } else {
            count++;
        }

        if (TextUtils.isEmpty(comentarios.getText())){
            danios.setError("¡¡CAMPO REQUERIDO!!");
        } else {
            count++;
        }

        if (TextUtils.isEmpty(fallos.getText())){
            danios.setError("¡¡CAMPO REQUERIDO!!");
        } else {
            count++;
        }

        if (count == 3){
            return true;
        }

        return false;
    }

    public void checkListQ(){
        if (rbSa.isChecked()){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("ACCESORIOS");
            builder.setMessage("NO HA SELECCIONADO NINGÚN ACCESORIO");
            builder.setPositiveButton("CONTINUAR", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    transicionesLayout(2, ffc_personal);
                    handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            transicionesLayout(1, ffc_equipo);
                        }
                    },600);
                }
            });
            builder.setNegativeButton("AGREGAR ACCESORIOS", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            builder.show();
        } else {
            transicionesLayout(2, ffc_personal);
            handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    transicionesLayout(1, ffc_equipo);
                }
            },600);
        }
    }

    public void onClickGarantias(View view){

        switch (view.getId()){
            case R.id.siguiente_ipersonal:
                if (validaDatos()){
                    checkListQ();
                } else {
                    Toast.makeText(this, "COMPLETE LOS CAMPOS REQUERIDOS", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.volver_gar:
                transicionesLayout(2, ffc_equipo);
                handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        transicionesLayout(1, ffc_personal);
                    }
                },600);
                break;
            case R.id.enviar_registrogar:
                if (validaDescripcion()){
                    enviarDatos();
                }
                break;
        }

    }

    public String makeChecklist(){

        String checklist = "";

        if (rbT.isChecked()){checklist += rbT.getText()+",";}
        if (rbB.isChecked()){checklist += rbB.getText()+",";}
        if (rbM.isChecked()){checklist += rbM.getText()+",";}
        if (rbC.isChecked()){checklist += rbC.getText()+",";}
        if (rbCj.isChecked()){checklist += rbCj.getText()+",";}
        if (rbL.isChecked()){checklist += rbL.getText()+",";}
        if (rbCd.isChecked()){checklist += rbCd.getText()+",";}
        if (rbSa.isChecked()){checklist += rbSa.getText()+",";}

        return checklist;

    }

    public void enviarDatos(){

        final String metodo;

        if (tipogar == 1){
            if (tiporeingreso == 1){
                metodo = "reingresoFFC";
            } else {
                metodo = "guardarFFC";
            }
        } else {
            metodo = "addNew";
        }

        SharedPreferences settings = this.getSharedPreferences(AppConstants.userPreferences,this.MODE_PRIVATE);
        final String user = settings.getString("usuario","-1");

        //Prueba Datos de la consulta
        Log.d("imei",imei.getText().toString());
        Log.d("fventa", fecha.getText().toString());
        Log.d("folio", folio);
        Log.d("marca", marca.getText().toString());
        Log.d("modelo", modelo.getText().toString());
        Log.d("direccion",direccion.getText().toString());
        Log.d("Reingreso", String.valueOf(tiporeingreso));
        Log.d("Tipoimei", String.valueOf(tipoimei));

        final RadioButton selected = estadofisico.findViewById(estadofisico.getCheckedRadioButtonId());

        //Prueba Datos del form
        Log.d("email",email_gar.getText().toString());
        Log.d("telefono", telefono_gar.getText().toString());
        Log.d("checklist", makeChecklist());
        Log.d("comment", comentarios.getText().toString());
        Log.d("fallo", fallos.getText().toString());
        Log.d("estado", selected.getText().toString());

        //Termina prueba

        //tipo garantía
        Log.d("TIPOGAR", metodo);

        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/GarantiasA.php";
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(String response) {
                Log.d("Response",response);
                showResponse(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CapturaGarantiasActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //prueba 352579083359213
                Map<String, String> params = new HashMap<>();
                //Dataset AddNew
                params.put("method",metodo);
                params.put("checklist",makeChecklist());
                params.put("user",user);
                params.put("garan_user",user);
                params.put("cliente",cliente.getText().toString());
                params.put("comment",comentarios.getText().toString());
                params.put("danios",danios.getText().toString());
                params.put("direccion",direccion.getText().toString());
                params.put("email",email_gar.getText().toString());
                params.put("estado",selected.getText().toString());
                params.put("fallo",fallos.getText().toString());
                params.put("fecha_venta",fecha.getText().toString());
                params.put("folio_r",folio);
                params.put("imei",imei.getText().toString());
                params.put("marca",marca.getText().toString());
                params.put("modelo",modelo.getText().toString());
                params.put("reingreso", String.valueOf(tiporeingreso));
                params.put("telefono",telefono_gar.getText().toString());
                //Dataset reingreso
                params.put("tipoimei", String.valueOf(tipoimei));
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    public void transicionesLayout(final int tipo, View target){
        Transition transition = new Slide(Gravity.BOTTOM);
        transition.setDuration(250);
        transition.addTarget(target);

        ViewGroup main = findViewById(R.id.mainlayout_garantias);

        TransitionManager.beginDelayedTransition(main, transition);
        if (tipo == 1) {
            target.setVisibility(View.VISIBLE);
        } else {
            target.setVisibility(View.GONE);
        }
    }

    public void selectorMenu(){

        View mView = getLayoutInflater().inflate(R.layout.alert_dialog,null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.btnDialog_opc1);
        Button btnopc2 = (Button) mView.findViewById(R.id.btnDialog_opc2);
        Button btncancel = (Button) mView.findViewById(R.id.btnDialog_cancel);
        final EditText rfc = (EditText) mView.findViewById(R.id.et_rfc);
        TextView header = (TextView) mView.findViewById(R.id.header);

        rfc.setFilters(new InputFilter[] { new InputFilter.LengthFilter(15) });

        rfc.setHint("INGRESE IMEI");

        dialog.setCancelable(false);

        rfc.setVisibility(View.VISIBLE);
        rfc.setInputType(InputType.TYPE_CLASS_NUMBER);

        btnopc1.setText("VALIDAR IMEI");
        btnopc2.setText(R.string.cancelar);
        header.setText("CAPTURAR IMEI\n\nRecuerde el IMEI consta de 15 digitos y solo se permiten numeros.");

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(rfc.getText()) && rfc.getText().length() == 15){
                    rellenaGarantia(rfc.getText().toString());
                    dialog.dismiss();
                } else {
                    Toast.makeText(CapturaGarantiasActivity.this, "EL IMEI DEBE CONTENER AL MENOS 15 CARACTERES NUMERICOS...", Toast.LENGTH_SHORT).show();
                    rfc.setError("REVISAR IMEI");
                }
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });

        btncancel.setVisibility(View.GONE);

        dialog.show();
    }

    private void rellenaGarantia(final String imei) {
        SharedPreferences settings = this.getSharedPreferences(AppConstants.userPreferences,this.MODE_PRIVATE);
        final String dis = settings.getString("usuario","-1");

        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/mgr-garantias/php/garantia.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(String response) {
                    Log.d("ResponseGG",response);
                    if (!response.toLowerCase().contains("imei invalido")) {
                        generaResponse(response);
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(CapturaGarantiasActivity.this, "IMEI NO ENCONTRADO, REINTENTE.", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(CapturaGarantiasActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("function","checkImei");
                    params.put("data",imei);
                    params.put("usuario",dis);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "ERROR", Toast.LENGTH_SHORT).show();
        }
    }

    public void restauraRB(){
        rbB.setChecked(false);
        rbT.setChecked(false);
        rbM.setChecked(false);
        rbC.setChecked(false);
        rbCj.setChecked(false);
        rbL.setChecked(false);
        rbCd.setChecked(false);
    }

    public void setData(){
        JSONObject object = null;

        /*Log.d("DatosCel",dVta);*/

        try{
            object = new JSONObject(dVta);
        }catch (Exception e){
            e.printStackTrace();
        }

        if (object.length() != 0 || object != null){
            Iterator x = object.keys();
            JSONArray jsonArray = new JSONArray();

            while(x.hasNext()){
                String key = String.valueOf(x.next());
                try{
                    jsonArray.put(object.get(key));
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }

            try{
                marca.setText(jsonArray.getString(0));
                modelo.setText(jsonArray.getString(1));
                fecha.setText(jsonArray.getString(2));
                imei.setText(jsonArray.getString(3));
                cliente.setText(jsonArray.getString(4));
                direccion.setText(jsonArray.getString(5));
            }catch (Exception e){
                e.printStackTrace();
            }

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void generaResponse(String response){

        JSONObject object = null;
        try{
            object = new JSONObject(response);
        }catch (Exception e){
            e.printStackTrace();
        }

        if (object != null){
            Iterator x = object.keys();
            JSONArray jsonArray = new JSONArray();

            while (x.hasNext()){
                String key = String.valueOf(x.next());
                try {
                    jsonArray.put(object.get(key));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            try {
                reingreso = jsonArray.getString(0);
                ffc = jsonArray.getString(1);
                imeinuevo = jsonArray.getString(2);
                folio = jsonArray.getString(3);
                detalles = jsonArray.getString(4);
                aplica = jsonArray.getString(5);
                dVta = jsonArray.getString(6);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (imeinuevo.toLowerCase().equals("false")){
                tipoimei = 1;
            } else {
                tipoimei = 2;
            }

            if (reingreso.toLowerCase().equals("false")){
                tiporeingreso = 0;
            } else{
                tiporeingreso = 1;
            }

            makeQuestion(aplica);

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void showResponses(String headerT, String opc1, String opc2, final int tipo, int btnffc){

        View mView = getLayoutInflater().inflate(R.layout.alert_dialog,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        dialog.setCancelable(false);

        final Button btnopc1 = (Button) mView.findViewById(R.id.btnDialog_opc1);
        final Button btnopc2 = (Button) mView.findViewById(R.id.btnDialog_opc2);
        Button btncancel = (Button) mView.findViewById(R.id.btnDialog_cancel);
        btncancel.setVisibility(View.GONE);
        TextView header = (TextView) mView.findViewById(R.id.header);

        header.setTextSize(16);

        btnopc1.setBackground(getDrawable(R.drawable.buttonred));
        btnopc2.setBackground(getDrawable(R.drawable.buttonblue2));

        if (tipo == 2){
            btnopc2.setBackground(getDrawable(R.drawable.buttonred));
            btnopc1.setBackground(getDrawable(R.drawable.buttonblue2));
        }

        btnopc1.setText(opc1);
        btnopc2.setText(opc2);
        header.setText(headerT);

        final String texto2 = "El IMEI ya estuvo en Garantías\n\n¿Desea reingresar a garantía?";
        String texto3 = "El IMEI capturado ya estuvo en garantía.";
        if (btnffc == 2){
            texto3 = "El equipo será capturado como garantía normal.";
        }
        final String finalTexto = texto3;
        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tipo == 1){
                    tipogar = 1;
                    dialog.dismiss();
                    Log.d("opcion",btnopc1.getText().toString());
                    showResponses(texto2, "Si, reingresar", "Cancelar",2, 1);
                    tipoffc = true;
                } else if (tipo == 2){
                    dialog.dismiss();
                    falloFFC(finalTexto);
                }
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tipo == 1){
                    tipogar = 2;
                    dialog.dismiss();
                    showResponses(texto2, "Si, reingresar", "Cancelar",2, 2);
                } else if (tipo == 2){
                    dialog.dismiss();
                    finish();
                }
            }
        });

        dialog.show();

    }

    public void falloFFC(String texto){
        AlertDialog.Builder builder = new AlertDialog.Builder(CapturaGarantiasActivity.this);
        builder.setTitle("DETALLES");
        builder.setMessage(texto);
        builder.setCancelable(false);
        builder.setNegativeButton("ACEPTAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                setData();
                transicionesLayout(1, ffc_personal);
                progressDialog.dismiss();
            }
        });
        builder.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void makeQuestion(String apl){
        if (apl.toLowerCase().equals("true")){
            if (ffc.toLowerCase().equals("true")){
                String texto = "El IMEI está evaluado como Garantía Fallo Fuera de Caja\n¿Desea registrarlo como Fallo Fuera de Caja o como Garantía normal?";
                showResponses(texto, "Fallo Fuera de Caja", "Garantía Normal", 1, 0);
            }
        }
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.rb_opc8) {
            restauraRB();
        } else {
            rbSa.setChecked(false);
        }
    }

    public void showResponse(String response){
        View mView = getLayoutInflater().inflate(R.layout.alert_response,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        builder.setCancelable(false);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.al_response_ok);
        TextView textView = (TextView) mView.findViewById(R.id.al_response);

        textView.setText(response);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });

        dialog.show();
    }

}
