package com.microtec.cmovil;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.microtec.cmovil.Interactor.ChipsAdapter;
import com.microtec.cmovil.Interactor.ProductosAdapter;
import com.microtec.cmovil.Model.LocalDataBase;
import com.microtec.cmovil.Model.Producto;
import com.microtec.cmovil.Utils.Estructura;
import com.microtec.cmovil.Utils.ItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ProductosActivity extends AppCompatActivity {

    private static final String prefencesN = "userdata";

    private RecyclerView mList;
    private RecyclerView mListC;
    private EditText buscaEquipo;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private LinearLayoutManager linearLayoutManager2;
    private DividerItemDecoration dividerItemDecoration2;
    private RecyclerView.Adapter mAdapter;
    private ProductosAdapter filtro;
    private List<Producto> pProductos;
    private List<Producto> backup;

    private RecyclerView.Adapter mAdapterChips;
    private List<Producto> pChips;

    private List<String> serie;

    int cuentaAudifonos = 1;
    int cantidadDispo = 0;
    String promocionText = "";
    String texto;

    private List<Integer> productosVenta;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos);

        mList = (RecyclerView) findViewById(R.id.rView_Productos);
        mListC = (RecyclerView) findViewById(R.id.rView_Chips);

        pProductos = new ArrayList<>();
        pChips = new ArrayList<>();
        serie = new ArrayList<>();
        productosVenta = new ArrayList<>();
        backup = new ArrayList<>();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando lista de productos...");

        progressDialog.show();

        consultaDisponibles();

        mAdapter = new ProductosAdapter(getApplicationContext(),pProductos);
        mAdapterChips = new ChipsAdapter(getApplicationContext(),pChips);

        filtro = new ProductosAdapter(getApplicationContext(),pProductos);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(),linearLayoutManager.getOrientation());

        linearLayoutManager2 = new LinearLayoutManager(this);
        linearLayoutManager2.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration2 = new DividerItemDecoration(mListC.getContext(),linearLayoutManager2.getOrientation());

        mList.setHasFixedSize(true);
        mListC.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mListC.setLayoutManager(linearLayoutManager2);
        mList.addItemDecoration(dividerItemDecoration);
        mListC.addItemDecoration(dividerItemDecoration2);
        //mList.setAdapter(mAdapter);
        mList.setAdapter(filtro);
        mListC.setAdapter(mAdapterChips);

        buscaEquipo = (EditText) findViewById(R.id.busca_equipos);

        buscaEquipo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

        mList.addOnItemTouchListener(new ItemClickListener(getApplicationContext(), new ItemClickListener.OnItemClickListener() {
            @Override
            public void OnItemClick(View view, int position) {
                selectorMenu(position);
            }
        }));

        listarProductos();
    }

    private void filter(String text){
        List<Producto> temp = new ArrayList();
        for (Producto producto : pProductos){
            if (producto.getMarca().toLowerCase().contains(text.toLowerCase()) || producto.getSerie().toLowerCase().equals(text.toLowerCase())){
                temp.add(producto);
            }
        }
        backup = temp;
        filtro.updateList(temp);
    }

    public void makeAlert(String titulo, String cuerpo){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(cuerpo);
        builder.setCancelable(true);
        builder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alerta = builder.create();
        alerta.show();
    }

    public void onClickGetChips(View view){

        boolean bandera = false;

        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");
        final String id_us = settings.getString("usuario","-1");

        final EditText etSerie = (EditText) findViewById(R.id.producto_chip_serie);
        final EditText etCantidad = (EditText) findViewById(R.id.producto_chip_cantidad);

        if (!TextUtils.isEmpty(etSerie.getText()) && !TextUtils.isEmpty(etCantidad.getText())){
            if (!serie.contains(etSerie.getText().toString())) {
                serie.add(etSerie.getText().toString());
                bandera = true;
            } else {
                makeAlert("CONTROL MÓVIL","Esta serie ya existe en el listado");
            }
        } else {
            makeAlert("CONTROL MÓVIL","Los campos no deben estar vacíos");
        }

        if (bandera) {

            if (!dis.equals("-1")) {

                String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VentasA.php";
                RequestQueue requestQueue = Volley.newRequestQueue(this);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.d("RESPONSE",response);
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object.length() != 0) {
                                generaListC(object);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ProductosActivity.this, "No se pudo encontrar esa serie.", Toast.LENGTH_SHORT).show();
                        }
                        mAdapterChips.notifyDataSetChanged();
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(ProductosActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("method", "getChips");
                        params.put("codigo_dis", dis);
                        params.put("id_us", id_us);
                        params.put("cantidad", etCantidad.getText().toString());
                        params.put("serie", etSerie.getText().toString());
                        return params;
                    }
                };
                requestQueue.add(stringRequest);
            }

        }

    }

    private void listarProductos() {

        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");
        final String id_us = settings.getString("usuario","-1");

        /*Log.d("controlmovi - dis",dis);
        Log.d("controlmovi - usuario",id_us);*/


        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VentasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try{
                        JSONObject object = new JSONObject(response);
                        if(object.length() != 0){
                            generaListP(object);
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    filtro.notifyDataSetChanged();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(ProductosActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","getInvent");
                    params.put("codigo_dis",dis);
                    params.put("id_us",id_us);
                    params.put("op",String.valueOf(1));
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void generaListC(JSONObject jsonObject) throws JSONException {

        Iterator x = jsonObject.keys();
        JSONArray jsonArray = new JSONArray();

        String[] marca; String[] modelo; String[] serie;
        String[] precio; String[] precioR; String[] precioK;
        String[] tipo;

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(jsonObject.get(key));
        }

        marca = (jsonArray.getString(0).replace("[","").replace("]","")).split(",");
        modelo = (jsonArray.getString(1).replace("[","").replace("]","")).split(",");
        serie = (jsonArray.getString(2).replace("[","").replace("]","")).split(",");
        precio = (jsonArray.getString(3).replace("[","").replace("]","")).split(",");
        precioR = (jsonArray.getString(4).replace("[","").replace("]","")).split(",");
        precioK = (jsonArray.getString(5).replace("[","").replace("]","")).split(",");
        tipo = (jsonArray.getString(6).replace("[","").replace("]","")).split(",");

        for (int i=0; i<marca.length; i++){
            Producto producto = new Producto();
            producto.setMarca(marca[i].replace("\"",""));
            producto.setModelo(modelo[i].replace("\"",""));
            producto.setSerie(serie[i].replace("\"",""));
            producto.setPrecio(precio[i].replace("\"",""));
            producto.setPrecioR(precioR[i].replace("\"",""));
            producto.setPrecioK(precioK[i].replace("\"",""));
            producto.setTipo(tipo[i].replace("\"",""));

            if (!this.serie.contains(serie[i].replace("\"",""))){
                this.serie.add(serie[i].replace("\"",""));
            }

            pChips.add(producto);
        }

    }

    public void generaListP(JSONObject jsonObject) throws JSONException {

        Iterator x = jsonObject.keys();
        JSONArray jsonArray = new JSONArray();

        String[] marca; String[] modelo; String[] serie;
        String[] precio; String[] precioR; String[] tipo;

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(jsonObject.get(key));
        }

        marca = (jsonArray.getString(0).replace("[","").replace("]","")).split(",");
        modelo = (jsonArray.getString(1).replace("[","").replace("]","")).split(",");
        serie = (jsonArray.getString(2).replace("[","").replace("]","")).split(",");
        precio = (jsonArray.getString(3).replace("[","").replace("]","")).split(",");
        precioR = (jsonArray.getString(4).replace("[","").replace("]","")).split(",");
        tipo = (jsonArray.getString(5).replace("[","").replace("]","")).split(",");

        for (int i=0; i<marca.length; i++){
            Producto producto = new Producto();
            producto.setMarca(marca[i].replace("\"",""));
            producto.setModelo(modelo[i].replace("\"",""));
            producto.setSerie(serie[i].replace("\"",""));
            producto.setPrecio(precio[i].replace("\"",""));
            producto.setPrecioR(precioR[i].replace("\"",""));
            producto.setPrecioK(precioR[i].replace("\"",""));
            producto.setTipo(tipo[i].replace("\"",""));
            pProductos.add(producto);
        }

        progressDialog.dismiss();
        backup = pProductos;

    }

    public void onClickEquipos(View v){
        LinearLayout equipos = (LinearLayout) findViewById(R.id.linear_equipos);
        LinearLayout chips = (LinearLayout) findViewById(R.id.linear_chips);
        LinearLayout sEquipos = (LinearLayout) findViewById(R.id.search_equipos);
        LinearLayout sChips = (LinearLayout) findViewById(R.id.search_chips);
        Button btnSChips = (Button) findViewById(R.id.btn_busca_chips);

        equipos.setVisibility(View.VISIBLE);
        sEquipos.setVisibility(View.VISIBLE);
        chips.setVisibility(View.GONE);
        sChips.setVisibility(View.GONE);
        btnSChips.setVisibility(View.GONE);
    }

    public void onClickChips(View v){
        LinearLayout equipos = (LinearLayout) findViewById(R.id.linear_equipos);
        LinearLayout chips = (LinearLayout) findViewById(R.id.linear_chips);
        LinearLayout sEquipos = (LinearLayout) findViewById(R.id.search_equipos);
        LinearLayout sChips = (LinearLayout) findViewById(R.id.search_chips);
        Button btnSChips = (Button) findViewById(R.id.btn_busca_chips);

        equipos.setVisibility(View.GONE);
        sEquipos.setVisibility(View.GONE);
        chips.setVisibility(View.VISIBLE);
        sChips.setVisibility(View.VISIBLE);
        btnSChips.setVisibility(View.VISIBLE);
    }

    public void selectorMenu(final int index){

        final String precioR = backup.get(index).getPrecio();
        final String precio = backup.get(index).getPrecioR();

        View mView = getLayoutInflater().inflate(R.layout.alert_select_producto,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        /*Log.d("Promo",promocionText);

        Log.d("Cantidad", String.valueOf(cantidadDispo));*/

        String[] partes = promocionText.split("<");
        texto = partes[0];
        cantidadDispo = Integer.parseInt(partes[1].replace(" ",""));

        String nuevo = "Fija un precio entre $".concat(precio).concat(" y ").concat(precioR);

        Button btnopc1 = (Button) mView.findViewById(R.id.btn_nwProd_ok);
        Button btnopc2 = (Button) mView.findViewById(R.id.btn_nwProd_cancel);
        TextView textView = (TextView) mView.findViewById(R.id.nwprod_header);
        final EditText editText = (EditText) mView.findViewById(R.id.new_prod_cant);

        textView.setText(nuevo);
        editText.setText(precio);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Float.parseFloat(editText.getText().toString()) >= Float.parseFloat(precio) && Float.parseFloat(editText.getText().toString()) <= Float.parseFloat(precioR)) {
                    saveProductos(index, editText.getText().toString());
                    if (Float.parseFloat(precio) >= 999 && (cantidadDispo>0) && (cantidadDispo-cuentaAudifonos>=0)){
                        //TODO Borrar esto si es necesario. Son promociones.
                        makePromo();
                    }
                } else {
                    Toast.makeText(ProductosActivity.this, "¡PRECIO FUERA DE RANGO!", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    public void saveProductos(int index, String precio){

        String seriex = "";

        if (productosVenta.size()>0) {
            //Log.d("INDEX", String.valueOf(productosVenta.indexOf(index)));
            if (productosVenta.indexOf(index) != -1) {
                seriex = pProductos.get(productosVenta.indexOf(index)).getSerie();
            }
        }

        //Log.d("serie",seriex);

        //if (!seriex.equals(backup.get(index).getSerie())){
        if (!productosVenta.contains(pProductos.indexOf(backup.get(index)))){
            backup.get(index).setPrecioR(precio);
            mAdapter.notifyDataSetChanged();
            productosVenta.add(pProductos.indexOf(backup.get(index)));
        } else {
            makeAlert("PRODUCTOS","PRODUCTO YA AÑADIDO");
            //Log.d("SERIEX",seriex);
            //Log.d("Serie original",backup.get(index).getSerie());
        }
    }

    public void onClickTerminarVenta(View view) throws JSONException {

        Bundle extras = getIntent().getExtras();
        String satcli = extras.getString("satcli","ninguno");

        float chipKitGratis = 0, chipsExpressGratis = 0, equiposKit = 0, equiposExpress = 0;
        float subtotalEquipos = 0, subtotalEquiposR = 0, subtotalChips = 0, subtotalChipsR = 0;
        float totalVenta = 0, totalVentaR = 0;

        ArrayList<Producto> ventaFinal = new ArrayList<>();

        for (int i=0; i<productosVenta.size(); i++){
            ventaFinal.add(pProductos.get(productosVenta.get(i)));
        }

        JSONArray jsonProductos = new JSONArray();

        for (int i=0; i<ventaFinal.size(); i++){
            JSONObject objeto = new JSONObject();
            objeto.put("marca",ventaFinal.get(i).getMarca());
            objeto.put("modelo",ventaFinal.get(i).getModelo());
            objeto.put("serie",ventaFinal.get(i).getSerie());
            objeto.put("precio",ventaFinal.get(i).getPrecioK());
            objeto.put("precioR",ventaFinal.get(i).getPrecioR());
            objeto.put("tipo",ventaFinal.get(i).getTipo());
            objeto.put("precioK",ventaFinal.get(i).getPrecioK());

            if (ventaFinal.get(i).getTipo().equals("K")){
                equiposKit++;
            } else if (ventaFinal.get(i).getTipo().equals("B")){
                equiposExpress++;
            }

            subtotalEquipos += Float.parseFloat(ventaFinal.get(i).getPrecio().replace("$",""));
            subtotalEquiposR += Float.parseFloat(ventaFinal.get(i).getPrecioR().replace("$",""));

            jsonProductos.put(objeto);
        }

        for (int i=0; i<pChips.size(); i++){

            if (!pChips.get(i).getModelo().toLowerCase().equals("chip express") && chipKitGratis<equiposKit){
                pChips.get(i).setPrecio("0");
                pChips.get(i).setPrecioR("0");
                mAdapterChips.notifyDataSetChanged();
                chipKitGratis++;
            } else if (pChips.get(i).getModelo().toLowerCase().equals("chip express") && chipsExpressGratis<equiposExpress){
                pChips.get(i).setPrecio("0");
                pChips.get(i).setPrecioR("0");
                mAdapterChips.notifyDataSetChanged();
                chipsExpressGratis++;
            }

            /*Log.d("Precio",pChips.get(i).getPrecio());
            Log.d("PrecioR",pChips.get(i).getPrecioR());
            Log.d("PrecioK",pChips.get(i).getPrecioK());*/

            JSONObject objeto = new JSONObject();
            objeto.put("marca",pChips.get(i).getMarca());
            objeto.put("modelo",pChips.get(i).getModelo());
            objeto.put("serie",pChips.get(i).getSerie());
            objeto.put("precio",pChips.get(i).getPrecio());
            objeto.put("precioR",pChips.get(i).getPrecio());
            objeto.put("tipo",pChips.get(i).getTipo());
            objeto.put("precioK",pChips.get(i).getPrecioR());

            subtotalChips += Float.parseFloat(pChips.get(i).getPrecio().replace("$",""));
            subtotalChipsR += Float.parseFloat(pChips.get(i).getPrecioR().replace("$",""));

            jsonProductos.put(objeto);
        }

        totalVenta = subtotalEquipos + subtotalChips;
        totalVentaR = subtotalChipsR + subtotalEquiposR;

        /*Log.d("Total venta", String.valueOf(totalVenta));
        Log.d("Total venta R", String.valueOf(totalVentaR));

        Log.d("JSON OBJECT",jsonProductos.toString());*/

        if (jsonProductos.length()>0) {

            if (cuentaAudifonos-1 > 0){
                for (int i=0; i<cuentaAudifonos-1; i++){
                    try {
                        jsonProductos.put(agregaAudifonos());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            Intent resumen = new Intent(this, ResumenActivity.class);
            resumen.putExtra("json", jsonProductos.toString());
            resumen.putExtra("satcli", satcli);
            resumen.putExtra("tipoDeVenta", "prodchips");
            startActivity(resumen);

        } else {
            Toast.makeText(this, "Debe añadir productos para continuar...", Toast.LENGTH_SHORT).show();
        }

    }

    public void onClickPendiente(View view) throws JSONException {

        float chipKitGratis = 0, chipsExpressGratis = 0, equiposKit = 0, equiposExpress = 0;
        float subtotalEquipos = 0, subtotalEquiposR = 0, subtotalChips = 0, subtotalChipsR = 0;
        float totalVenta = 0, totalVentaR = 0;

        ArrayList<Producto> ventaFinal = new ArrayList<>();

        for (int i=0; i<productosVenta.size(); i++){
            ventaFinal.add(pProductos.get(productosVenta.get(i)));
        }

        JSONArray jsonProductos = new JSONArray();

        for (int i=0; i<ventaFinal.size(); i++){
            JSONObject objeto = new JSONObject();
            objeto.put("marca",ventaFinal.get(i).getMarca());
            objeto.put("modelo",ventaFinal.get(i).getModelo());
            objeto.put("serie",ventaFinal.get(i).getSerie());
            objeto.put("precio",ventaFinal.get(i).getPrecio());
            objeto.put("precioR",ventaFinal.get(i).getPrecioR());
            objeto.put("tipo",ventaFinal.get(i).getTipo());
            objeto.put("precioK",ventaFinal.get(i).getPrecioK());

            if (ventaFinal.get(i).getTipo().equals("K")){
                equiposKit++;
            } else if (ventaFinal.get(i).getTipo().equals("B")){
                equiposExpress++;
            }

            subtotalEquipos += Float.parseFloat(ventaFinal.get(i).getPrecio().replace("$",""));
            subtotalEquiposR += Float.parseFloat(ventaFinal.get(i).getPrecioR().replace("$",""));

            jsonProductos.put(objeto);
        }

        for (int i=0; i<pChips.size(); i++){
            if (!pChips.get(i).getModelo().equals("Chip Express") && chipKitGratis<equiposKit){
                pChips.get(i).setPrecio("0");
                pChips.get(i).setPrecioR("0");
                mAdapterChips.notifyDataSetChanged();
                chipKitGratis++;
            } else if (pChips.get(i).getModelo().equals("Chip Express") && chipsExpressGratis<equiposExpress){
                pChips.get(i).setPrecio("0");
                pChips.get(i).setPrecioR("0");
                mAdapterChips.notifyDataSetChanged();
                chipsExpressGratis++;
            }

            JSONObject objeto = new JSONObject();

            objeto.put("marca",pChips.get(i).getMarca());
            objeto.put("modelo",pChips.get(i).getModelo());
            objeto.put("serie",pChips.get(i).getSerie());
            objeto.put("precio",pChips.get(i).getPrecio());
            objeto.put("precioR",pChips.get(i).getPrecio());
            objeto.put("tipo",pChips.get(i).getTipo());
            objeto.put("precioK",pChips.get(i).getPrecioK());

            subtotalChips += Float.parseFloat(pChips.get(i).getPrecio().replace("$",""));
            subtotalChipsR += Float.parseFloat(pChips.get(i).getPrecioR().replace("$",""));

            jsonProductos.put(objeto);
        }

        totalVenta = subtotalEquipos + subtotalChips;
        totalVentaR = subtotalChipsR + subtotalEquiposR;

        /*Log.d("Total venta", String.valueOf(totalVenta));
        Log.d("Total venta R", String.valueOf(totalVentaR));*/

        if (productosVenta.size() > 0 || pChips.size() > 0){
            guardarPendiente(jsonProductos.toString());
            Toast.makeText(this, "VENTA GUARDADA CORRECTAMENTE", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, PuntoVentaActivity.class).putExtra("tipovta1",0));
            finish();
        } else {
            Toast.makeText(this, "NO HAY PRODUCTOS AGREGADOS A LA VENTA", Toast.LENGTH_SHORT).show();
        }

    }

    public void guardarPendiente(String jproductos){

        Calendar c = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String fecha = simpleDateFormat.format(c.getTime());
        simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        String hora = simpleDateFormat.format(c.getTime());

        String completa = fecha + " " + hora;

        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String codigo_dis = settings.getString("dis","-1");

        LocalDataBase localDataBase = new LocalDataBase(this);
        ContentValues contentValues = new ContentValues();
        SQLiteDatabase sqLiteDatabase = localDataBase.getWritableDatabase();

        Bundle extras = getIntent().getExtras();
        String cliente = extras.getString("cliente");
        String satcli = extras.getString("satcli");

        contentValues.put(Estructura.COLUMN_NAME_CLIENTE,cliente);
        contentValues.put(Estructura.COLUMN_NAME_SATCLI,satcli);
        contentValues.put(Estructura.COLUMN_NAME_CODIGODIS,codigo_dis);
        contentValues.put(Estructura.COLUMN_NAME_COORDENADAS,"coordenadas");
        contentValues.put(Estructura.COLUMN_NAME_FECHA,completa);
        contentValues.put(Estructura.COLUMN_NAME_PRODUCTOS,jproductos);

        sqLiteDatabase.insert(Estructura.TABLE_NAME2,null,contentValues);

        sqLiteDatabase.close();

    }

    public void makePromo(){

        View mView = getLayoutInflater().inflate(R.layout.alert_audifonos,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.btn_promo_ok);
        Button btnopc2 = (Button) mView.findViewById(R.id.btn_promo_cancelar);
        TextView textView = (TextView) mView.findViewById(R.id.new_promocion_promo);

        textView.setText(texto);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cuentaAudifonos++;
                dialog.dismiss();
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        dialog.show();

    }

    public JSONObject agregaAudifonos() throws JSONException {
        JSONObject objeto = new JSONObject();
        objeto.put("marca","AUDIFONOS MANOS LIBRES RECARGABLES i7S");
        objeto.put("modelo","TWS");
        objeto.put("serie","1520006");
        objeto.put("precio","150");
        objeto.put("precioR","150");
        objeto.put("tipo","");
        objeto.put("precioK","130");
        return objeto;
    }

    public void consultaDisponibles() {

        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");

        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VentasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("resposn",response.replace("null",""));
                    //cantidadDispo = Integer.parseInt(response.replace("null",""));
                    promocionText = response.replace("null","");
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(ProductosActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","getPromo");
                    params.put("micd",dis);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }
}
