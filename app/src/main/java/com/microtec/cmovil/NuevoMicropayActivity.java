package com.microtec.cmovil;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class NuevoMicropayActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private static final String prefencesN = "userdata";

    private EditText nombre, appaterno, apmaterno, celular;
    private EditText email, rfc, nombrenego, municipio;
    private EditText ciudad, colonia, copostal, calle, numext, numint;
    private TextView medioventa;
    private Button nw_cliente_send;

    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double lat, lon;

    private Spinner estadosr7, porcentaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_micropay);
        buildGoogleApiclient();

        initElementos();

    }

    public void initElementos(){

        nw_cliente_send = (Button) findViewById(R.id.nw_cliente_send);

        nombre = (EditText) findViewById(R.id.newCliente_nombre);
        appaterno = (EditText) findViewById(R.id.newCliente_apellidoP);
        apmaterno = (EditText) findViewById(R.id.newCliente_apellidoM);
        celular = (EditText) findViewById(R.id.newCliente_celular);
        email = (EditText) findViewById(R.id.newCliente_email);
        rfc = (EditText) findViewById(R.id.newCliente_RFC);
        nombrenego = (EditText) findViewById(R.id.newCliente_nombreNegocio);
        medioventa = (TextView) findViewById(R.id.newCliente_medioVenta);
        municipio = (EditText) findViewById(R.id.newCliente_municipio);
        ciudad = (EditText) findViewById(R.id.newCliente_ciudad);
        colonia = (EditText) findViewById(R.id.newCliente_Colonia);
        copostal = (EditText) findViewById(R.id.newCliente_copostal);
        calle = (EditText) findViewById(R.id.newCliente_calle);
        numext = (EditText) findViewById(R.id.newCliente_exterior);
        numint = (EditText) findViewById(R.id.newCliente_interior);

        estadosr7 = (Spinner) findViewById(R.id.estador7_spinner);
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this,R.array.estados_array, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        estadosr7.setAdapter(arrayAdapter);

        porcentaje = (Spinner) findViewById(R.id.porcentaje_spinner);
        ArrayAdapter<CharSequence> arrayAdapter2 = ArrayAdapter.createFromResource(this,R.array.porcentaje_array, android.R.layout.simple_spinner_item);
        arrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        porcentaje.setAdapter(arrayAdapter2);

    }

    public void onClickEnviarSolicitud(View view){

        boolean estados = false;
        boolean porcen = false;

        nonEmpty(nombre); nonEmpty(appaterno); nonEmpty(apmaterno); nonEmpty(celular);
        nonEmpty(municipio); nonEmpty(ciudad); nonEmpty(colonia);
        nonEmpty(copostal); nonEmpty(calle); nonEmpty(rfc);

        if (estadosr7.getSelectedItem().toString().contains("Seleccione")){
            estados = false;
            Toast.makeText(this, "El campo ESTADO es requerido.", Toast.LENGTH_SHORT).show();

        } else {
            estados = true;
        }

        if (nonEmpty(nombre) && nonEmpty(appaterno) && nonEmpty(apmaterno) && nonEmpty(celular) && nonEmpty(municipio) && nonEmpty(ciudad) && nonEmpty(colonia) &&
        nonEmpty(copostal)&& nonEmpty(calle) && nonEmpty(rfc) && estados){

            nw_cliente_send.setEnabled(false);
            sendNClienteRequest();

        } else {
            Toast.makeText(this, "LLENE LOS CAMPOS REQUERIDOS", Toast.LENGTH_SHORT).show();
        }

    }

    public boolean nonEmpty(EditText editText){
        if (!TextUtils.isEmpty(editText.getText())){
            return true;
        } else {
            editText.setError("Error: Este campo es requerido.");
            return false;
        }
    }

    public void sendNClienteRequest(){
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");
        final String agente = settings.getString("agente","-1");

        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/MicropayA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("RESPUESTA",response);
                    if (response.contains("DONE")){
                        Toast.makeText(NuevoMicropayActivity.this, response.replace("null","").replace("DONE",""), Toast.LENGTH_SHORT).show();
                        showResponse(response.replace("null","").replace("DONE",""));
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    nw_cliente_send.setEnabled(true);
                    Toast.makeText(NuevoMicropayActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","crearCliente");
                    params.put("dis",dis);
                    params.put("agente",agente);
                    params.put("nom",nombre.getText().toString());
                    params.put("paterno",appaterno.getText().toString());
                    params.put("materno",apmaterno.getText().toString());
                    params.put("cel",celular.getText().toString());
                    params.put("email",email.getText().toString());
                    params.put("rfc",rfc.getText().toString());
                    params.put("nompdv","");
                    params.put("medio","RE");
                    params.put("porcentaje","0");
                    params.put("estado", estadosr7.getSelectedItem().toString());
                    params.put("municipio",municipio.getText().toString());
                    params.put("ciudad",ciudad.getText().toString());
                    params.put("colonia",colonia.getText().toString());
                    params.put("cp",copostal.getText().toString());
                    params.put("calle",calle.getText().toString());
                    params.put("numext","0");
                    params.put("numint","0");
                    return params;
                }
            };
            try{
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        500000,
                        -1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
            } catch (Exception e){
                e.printStackTrace();
            }
            requestQueue.add(stringRequest);
        }
    }

    public void showResponse(String response){
        View mView = getLayoutInflater().inflate(R.layout.alert_response,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.al_response_ok);
        TextView textView = (TextView) mView.findViewById(R.id.al_response);

        textView.setText(response);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startActivity(new Intent(NuevoMicropayActivity.this,MenuPrincipal.class));
                finish();
            }
        });

        dialog.show();

    }

    public void setCoordenadas(){
        lat = mLastLocation.getLatitude();
        lon = mLastLocation.getLongitude();
        if (lat == 0.0){
            //Log.d("COORDS","Tomadas desde la ultima act");
            String coordenadas = PreferenceManager.getDefaultSharedPreferences(this).getString("location-update-result","");
            String [] partes = coordenadas.split(",");
            lat = Double.parseDouble(partes[0]);
            lon = Double.parseDouble(partes[1]);
        }

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());

        try{
            List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
            Log.d("Municipio:", addresses.get(0).getLocality());
            municipio.setText(addresses.get(0).getLocality());
            ciudad.setText(addresses.get(0).getLocality());
            Log.d("CP:", addresses.get(0).getPostalCode());
            copostal.setText(addresses.get(0).getPostalCode());
            Log.d("Colonia:", addresses.get(0).getSubLocality());
            colonia.setText(addresses.get(0).getSubLocality());
            Log.d("---","---");
            Log.d("Estado:", addresses.get(0).getAdminArea());
            if (!addresses.get(0).getAdminArea().contains("null")){
                ArrayAdapter arrayAdapter = (ArrayAdapter) estadosr7.getAdapter();
                int position = arrayAdapter.getPosition(addresses.get(0).getAdminArea());
                estadosr7.setSelection(position);
            }
            calle.setText(addresses.get(0).getAddressLine(0).split(",")[0]);
        } catch(Exception e){
            e.printStackTrace();
        }

    }

    private boolean isMyPositionSuccess = false;
    @Override
    public void onLocationChanged(Location location) {
        if (!isMyPositionSuccess) {
            mLastLocation = location;
            setCoordenadas();
            isMyPositionSuccess =true;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(500);
        mLocationRequest.setFastestInterval(500);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    protected synchronized void buildGoogleApiclient(){
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

}
