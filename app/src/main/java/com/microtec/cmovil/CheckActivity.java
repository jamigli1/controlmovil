package com.microtec.cmovil;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.microtec.cmovil.Interactor.AdapterBuscarFolio;
import com.microtec.cmovil.Model.BuscarFolioCheckIn;
import com.microtec.cmovil.Model.Inventario;
import com.microtec.cmovil.Presenter.CheckInPresenter;
import com.microtec.cmovil.Presenter.CheckInPresenterImpl;
import com.microtec.cmovil.Utils.AppConstants;
import com.microtec.cmovil.View.CheckInView;

import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;

public class CheckActivity extends AppCompatActivity implements CheckInView {

    private Spinner spinnerSelectTipo;
    private Spinner spinnerSelectTipoDoc;
    private TextInputLayout inputFolioContentCheckIn;
    private RecyclerView rvResultCheckin;

    private CheckInPresenter presenter;
    private String _TIPO_1, _TIPO_2,_FOLIO,_CHECK;
    private int _TIPO;
    private String[] tipo = {"Garantía","Transferencia","Pedido franquicia","TAR","TAF","ASS","Paquete"};
    private String[] tipo_to_send = {"garantia","transferencia","pedido_franquicia","TAR","TAF","ASS","paquete"};
    private String[] tipo_2 = {"TAR","TAF","ASS"};
    private FloatingActionButton fabCheckOut;
    private FloatingActionButton fabCheckIn;
    private Dialog dialogIngresarComentario;
    private EditText inputIngresarComentario;
    private Button btnCancelar, btnConfirmar;
    private AlertDialog dialog;
    private List<Inventario> elementos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check);

        AppConstants.context2 = this;

        spinnerSelectTipo = findViewById(R.id.spinner_select_tipo);
        spinnerSelectTipoDoc = findViewById(R.id.spinner_select_tipo_doc);
        inputFolioContentCheckIn = findViewById(R.id.inputFolioContentCheckIn);
        rvResultCheckin = findViewById(R.id.rv_result_checkin);

        presenter = new CheckInPresenterImpl(this);

        fabCheckIn = findViewById(R.id.fabCheckIn);
        fabCheckOut = findViewById(R.id.fabCheckOut);

        spinnerSelectTipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                _TIPO_1 = tipo[i];
                _TIPO = i;
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });

        spinnerSelectTipoDoc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                _TIPO_2 = tipo_2[i];
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });

        fabCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _CHECK = "checkin";
                dialogIngresarComentario.show();
            }
        });

        fabCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _CHECK = "checkout";
                dialogIngresarComentario.show();
            }
        });

        dialogIngresarComentario = new Dialog(this);
        dialogIngresarComentario.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogIngresarComentario.setCancelable(false);
        dialogIngresarComentario.setContentView(R.layout.dialog_ingresar_comentario);
        dialogIngresarComentario.create();

        inputIngresarComentario = dialogIngresarComentario.findViewById(R.id.inputIngresarComentario);

        btnCancelar = dialogIngresarComentario.findViewById(R.id.btnCancelarIngresarComentario);
        btnConfirmar = dialogIngresarComentario.findViewById(R.id.btnConfirmarIngresarComentario);

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputIngresarComentario.setText("");
                dialogIngresarComentario.dismiss();
            }
        });
        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String comentario = inputIngresarComentario.getText().toString();
                String tipo_tramite = tipo_to_send[_TIPO];
                if (_TIPO!=6) {
                    if (!comentario.isEmpty()) {
                        dialogIngresarComentario.dismiss();
                        presenter.checkInOut(_FOLIO, comentario, _CHECK, tipo_tramite);
                    } else {
                        inputIngresarComentario.setError("¡¡Campo requerido!!");
                    }
                }else{
                    if (!comentario.isEmpty()) {
                        dialogIngresarComentario.dismiss();
                        presenter.checkInOutPaquete(_FOLIO, comentario, _CHECK, tipo_tramite, elementos);
                    } else {
                        inputIngresarComentario.setError("¡¡Campo requerido!!");
                    }
                }
            }
        });

        dialog = new SpotsDialog.Builder().setContext(CheckActivity.this).build();
        dialog.setMessage("Espere...");

        rvResultCheckin.setHasFixedSize(false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        rvResultCheckin.setLayoutManager(layoutManager);

    }

    public void onBtnBuscarFolioCheckInClick(View view) {
        _FOLIO = inputFolioContentCheckIn.getEditText().getText().toString();
        presenter.buscarFolioCheckIn(_FOLIO,_TIPO_1,_TIPO_2);
    }


    @Override
    public void showInfo(List<BuscarFolioCheckIn> resultado){
        try{
            if(resultado.size()>0){
                if (_TIPO ==6){
                    elementos = new ArrayList<>();
                    for (int i = 1; i < resultado.size(); i++) {
                        Inventario in = new Inventario();
                        in.setFolio(resultado.get(i).getUno());
                        in.setTipo(resultado.get(i).getDos());
                        elementos.add(in);
                    }
                }

                AdapterBuscarFolio adapter = new AdapterBuscarFolio(resultado);
                rvResultCheckin.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                return;
            }else{
                presenter.showMessage(getResources().getString(R.string.empty_list));
            }
        }catch (Exception e){
            presenter.showMessage(getResources().getString(R.string.empty_list));
        }

    }

    @Override
    public void showMessage(String mensaje){
        Snackbar.make(inputFolioContentCheckIn,mensaje,Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void showButtonCheckIn(boolean isCheckIn){
        if (isCheckIn){
            fabCheckIn.show();
            fabCheckOut.hide();
        }else{
            fabCheckOut.show();
            fabCheckIn.hide();
        }
    }


    @Override
    public void showProgressBar(){
        dialog.show();
    }

    @Override
    public void hideProgressBar(){
        dialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        Intent intentStartRoot = new Intent().setComponent(new ComponentName(this, MenuPrincipal.class));
        intentStartRoot.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intentStartRoot);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            Intent intentStartRoot = new Intent().setComponent(new ComponentName(this, MenuPrincipal.class));
            intentStartRoot.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intentStartRoot);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
