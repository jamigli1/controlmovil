package com.microtec.cmovil;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.microtec.cmovil.Utils.AlertResponse;
import com.microtec.cmovil.Utils.DatePickerFragment;
import com.microtec.cmovil.Utils.Utils;
import com.microtec.cmovil.Utils.VolleyMultipartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.microtec.cmovil.NuevoProspectoActivity.REQUEST_IMAGE_CAPTURE;

public class CapturarDepositoActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String prefencesN = "userdata";

    private EditText datePicker1, monto;
    private EditText datePicker2, movimiento;
    private Spinner bancos, cuentas;
    private Button btnSave;
    private int id_cuenta;
    private boolean banVoucher;
    private String mCurrentPath;
    public Bitmap imgvoucher;
    private List<String> banco, cuenta, empresa, banco2, idcuenta, idcuentatmp;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capturar_deposito);

        id_cuenta = 0;
        mCurrentPath = "";
        banVoucher = false;

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Guardando deposito...");

        datePicker1 = (EditText) findViewById(R.id.datePicker1);
        datePicker2 = (EditText) findViewById(R.id.datePicker2);
        monto = (EditText) findViewById(R.id.nw_dep_monto);
        movimiento = (EditText) findViewById(R.id.nw_dep_movimiento);

        datePicker1.setOnClickListener(this);
        datePicker2.setOnClickListener(this);

        btnSave = (Button) findViewById(R.id.nw_dep_btnsave);
        btnSave.setOnClickListener(this);

        getBancos();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.datePicker1:
                showDatePickerDialog(datePicker1);
                break;
            case R.id.datePicker2:
                showDatePickerDialog(datePicker2);
                break;
            case R.id.nw_dep_btnsave:
                progressDialog.show();
                btnSave.setEnabled(false);
                saveDeposito();
                break;
        }
    }

    public void showDatePickerDialog(final EditText editText) {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                final String selectedDate = dayOfMonth
                        + "-" + (month + 1) + "-" + year;
                editText.setText(selectedDate);
            }
        });

        newFragment.show(CapturarDepositoActivity.this.getSupportFragmentManager(), "datePicker");
    }

    public void saveDeposito() {
        nonEmpty((EditText) findViewById(R.id.nw_dep_monto));
        nonEmpty((EditText) findViewById(R.id.nw_dep_movimiento));
        nonEmpty((EditText) findViewById(R.id.datePicker1));
        nonEmpty((EditText) findViewById(R.id.datePicker2));

        if (nonEmpty((EditText) findViewById(R.id.nw_dep_monto)) && nonEmpty((EditText) findViewById(R.id.nw_dep_movimiento))
                && nonEmpty((EditText) findViewById(R.id.datePicker1)) && nonEmpty((EditText) findViewById(R.id.datePicker2))
                    && !bancos.getSelectedItem().toString().toLowerCase().contains("selecciona") && !cuentas.getSelectedItem().toString().toLowerCase().contains("selecciona")) {
            //TODO Checar depositos.
            sendDepositoRequest();
            /*Log.d("FECHA1",datePicker1.getText().toString());
            Log.d("FECHA2",datePicker2.getText().toString());*/
        } else {
            Toast.makeText(this, "COMPLETA EL FORMULARIO, POR FAVOR.", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean nonEmpty(EditText editText) {
        if (!TextUtils.isEmpty(editText.getText())) {
            return true;
        } else {
            editText.setError("Error: Este campo es requerido.");
            return false;
        }
    }

    public void sendDepositoRequest() {
        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/DepositosA.php";
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        final Handler handler = new Handler();
        final AlertResponse alertResponse = new AlertResponse(CapturarDepositoActivity.this);

        SharedPreferences settings = this.getSharedPreferences(prefencesN, this.MODE_PRIVATE);
        final String dis = settings.getString("dis", "-1");
        final String agente = settings.getString("agente", "-1");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("IDMOV", response);
                if (response.contains("DONE")) {
                    if (banVoucher){
                        getNUploadF();
                        uploadBitmap(imgvoucher, response.split(" ")[1]);
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(CapturarDepositoActivity.this, "DEPOSITO REALIZADO", Toast.LENGTH_SHORT).show();
                        btnSave.setEnabled(false);
                        finish();
                    }
                } else if (response.contains("wrong")) {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            alertResponse.showResponse("Hubo un error al capturar su deposito; reintente.");
                        }
                    }, 2255);
//                    Toast.makeText(CapturarDepositoActivity.this, "Hubo un error al capturar su deposito; reintente.", Toast.LENGTH_SHORT).show();
                    btnSave.setEnabled(true);
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CapturarDepositoActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("method", "guardarDepositoNuevo");
                params.put("dis", dis);
                params.put("agente", agente);
                params.put("fecha", datePicker1.getText().toString());
                params.put("monto", monto.getText().toString());
                params.put("banco", bancos.getSelectedItem().toString());
                params.put("movimiento", movimiento.getText().toString());
                params.put("fecha2", datePicker2.getText().toString());
                params.put("idcuenta", idcuentatmp.get(id_cuenta));
                return params;
            }
        };
        try {
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    500000,
                    -1,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
        } catch (Exception e) {
            e.printStackTrace();
        }
        requestQueue.add(stringRequest);
    }

    public void getBancos() {
        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/DepositosA.php";
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("DepositosC", response);
                try {
                    setBancos(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CapturarDepositoActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("method", "getBancos");
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    public void setBancos(String jsonObject) throws JSONException {
        JSONObject object = new JSONObject(jsonObject);
        Log.d("Bancos", object.get("banco").toString());

        banco2 = new ArrayList<String>();
        idcuentatmp = new ArrayList<String>();

        banco = Arrays.asList(object.get("banco").toString().replace("[", "").replace("]", "").replace("\"", "").split(","));
        cuenta = Arrays.asList(object.get("cuenta").toString().replace("[", "").replace("]", "").replace("\"", "").split(","));
        empresa = Arrays.asList(object.get("empresa").toString().replace("[", "").replace("]", "").replace("\"", "").split(","));
        idcuenta = Arrays.asList(object.get("idcuenta").toString().replace("[", "").replace("]", "").replace("\"", "").split(","));

        banco2.add("Selecciona banco...");
        for (int i = 0; i < banco.size(); i++) {
            if (!banco2.contains(banco.get(i))) {
                banco2.add(banco.get(i));
            }
        }

        bancos = (Spinner) findViewById(R.id.banco_spinner);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, banco2);
        // ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this,R.array.bancos_array, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bancos.setAdapter(arrayAdapter);

        bancos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i!=0) {
                    setCuentas(i);
                } else {
                    setCuentas(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void setCuentas(int idbanco) {
        ArrayList<String> temporal = new ArrayList<String>();
        idcuentatmp.clear();
        temporal.add("Selecciona cuenta...");
        for (int i = 0; i < banco.size(); i++) {
            if (banco.get(i).equals(banco2.get(idbanco))) {
                temporal.add(astericosC(cuenta.get(i)).concat(" -> ").concat(empresa.get(i)));
                idcuentatmp.add(idcuenta.get(i));
            }
        }
        cuentas = (Spinner) findViewById(R.id.cuenta_spinner);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, temporal);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cuentas.setAdapter(arrayAdapter);
        cuentas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    id_cuenta = i-1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public String astericosC(String cuenta){
        String base = "***";
        for (int i=cuenta.length()-4; i<cuenta.length(); i++){
            base += (String.valueOf(cuenta.charAt(i)));
        }
        return base;
    }

    public void dispatchTakePictureIntent(View view) {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try{
                photoFile = createImage();
                findViewById(R.id.btn_fotoVoucher).setEnabled(false);
            } catch (IOException e){
                e.printStackTrace();
            }

            if (photoFile!=null){
                Uri photoUri = FileProvider.getUriForFile(this, "com.microtec.cmovil.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,photoUri);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }

    }

    private File createImage() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "CMOVIL_"+timeStamp+"_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg",storageDir);
        mCurrentPath = image.getAbsolutePath();
        banVoucher = true;
        return image;
    }

    public void uploadBitmap(final Bitmap bitmap, final String idmov){

        final String nombre = "voucher_"+idmov+".jpg";

        final String method = "actualizarImagenes";

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/DepositosA.php",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        if (new String(response.data).contains("\"ruta\"")) {
                            Log.d("Response", new String(response.data));
                            progressDialog.dismiss();
                            new AlertDialog.Builder(CapturarDepositoActivity.this).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    finish();
                                }
                            }).setTitle("Deposito Capturado").show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CapturarDepositoActivity.this, "TIEMPO DE ESPERA AGOTADO; REVISA TU CONEXIÓN A INTERNET", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError{
                Map<String,String> params = new HashMap<>();
                params.put("method",method);
                params.put("idMov",idmov);
                return params;
            }

            @Override
            protected Map<String,DataPart> getByteData(){
                Map<String,DataPart> params = new HashMap<>();
                params.put("imagen",new DataPart(nombre, getFileDataFromDrawable(bitmap)));
                return params;
            }
        };
        try{
            volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    500000,
                    -1,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
        } catch (Exception e){
            e.printStackTrace();
        }
        Volley.newRequestQueue(this).add(volleyMultipartRequest);

    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public void getNUploadF(){

        Bitmap imagenC;

        File file = new File(mCurrentPath);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        Bitmap imageBitmap = null;
        ByteArrayOutputStream out = new ByteArrayOutputStream();


        try {
            imageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.fromFile(file));
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 50, out);
            imagenC = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
            imageBitmap = Bitmap.createScaledBitmap(imagenC,(int)(imagenC.getWidth()*0.6), (int)(imagenC.getHeight()*0.6), true);
            file.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }

        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 10, bytes);
        if (imageBitmap != null) {
            imgvoucher = imageBitmap;
        }

    }

}
