package com.microtec.cmovil.Interactor;

import com.microtec.cmovil.Model.Inventario;

import java.util.List;

public interface CheckInInteractor {
    void buscarFolioCheckIn(String folio, String tipo1, String tipo2);

    void checkInOut(String folio, String comentario, String tipo, String tipo_tramite);

    void checkInOutPaquete(String folio, String comentario, String tipo, String tipo_tramite, List<Inventario> elementos);
}
