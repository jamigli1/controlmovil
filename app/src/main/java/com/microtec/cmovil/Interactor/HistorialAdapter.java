package com.microtec.cmovil.Interactor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.vipulasri.timelineview.TimelineView;
import com.microtec.cmovil.Model.Garantia;
import com.microtec.cmovil.Model.Historial;
import com.microtec.cmovil.R;

import java.util.ArrayList;
import java.util.List;

public class HistorialAdapter extends RecyclerView.Adapter<HistorialAdapter.ViewHolder> {

    private Context context;
    private List<Historial> list;

    public HistorialAdapter(ArrayList<Historial> historials){
        this.list = historials;
    }

    public HistorialAdapter(Context context, List<Historial> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public HistorialAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.single_historial, parent, false);
        return new HistorialAdapter.ViewHolder(v, viewType);
    }

    @Override
    public void onBindViewHolder(HistorialAdapter.ViewHolder holder, int position){
        Historial historial = list.get(position);
        holder.fecha.setText(historial.getFecha());
        holder.descripcion.setText(historial.getDescripcion());
    }

    @Override
    public int getItemCount(){
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView descripcion, fecha;
        private TimelineView timelineView;
        public ViewHolder (View itemView, int viewType){
            super(itemView);
            descripcion = itemView.findViewById(R.id.garantia_descripcion);
            fecha = itemView.findViewById(R.id.garantia_fecha);
            timelineView = (TimelineView) itemView.findViewById(R.id.timeline_historial);
            timelineView.initLine(viewType);
        }
    }

    @Override
    public int getItemViewType(int position){
        return TimelineView.getTimeLineViewType(position,getItemCount());
    }

}