package com.microtec.cmovil.Interactor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.microtec.cmovil.Model.Garantia;
import com.microtec.cmovil.Model.Transferencia;
import com.microtec.cmovil.R;

import java.util.List;

public class DetTransfAdapter extends RecyclerView.Adapter<DetTransfAdapter.ViewHolder> {

    private Context context;
    private List<Garantia> transferencias;
    private int row_index;

    public DetTransfAdapter(Context context, List<Garantia> transferencias){
        this.context = context;
        this.transferencias = transferencias;
    }

    @Override
    public DetTransfAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.single_dettransf, parent, false);
        return new DetTransfAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DetTransfAdapter.ViewHolder holder, final int position){
        Garantia garantia = transferencias.get(position);
        holder.tvFolio.setText(garantia.getFolio());
        holder.tvGrupoFecha.setText(garantia.getMarca()+"\n"+garantia.getModelo()+"\n"+garantia.getImei());
        holder.tvGrupoDestino.setText(garantia.getChecklist());
    }

    public void updateList(List<Garantia> list){
        transferencias = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount(){
        return transferencias.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvFolio, tvGrupoFecha, tvGrupoDestino;
        public ViewHolder (View itemView){
            super(itemView);
            tvFolio = itemView.findViewById(R.id.folio_transf_det);
            tvGrupoFecha = itemView.findViewById(R.id.grupocel_dettransf);
            tvGrupoDestino = itemView.findViewById(R.id.accesorios_det);
        }
    }

}
