package com.microtec.cmovil.Interactor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.microtec.cmovil.Model.Garantia;
import com.microtec.cmovil.Model.PuntoVenta;
import com.microtec.cmovil.Model.Transferencia;
import com.microtec.cmovil.R;

import java.util.ArrayList;
import java.util.List;

public class GarantiaAdapter extends RecyclerView.Adapter<GarantiaAdapter.ViewHolder> {

    private Context context;
    private List<Garantia> list;

    public GarantiaAdapter(ArrayList<Garantia> garantias){
        this.list = garantias;
    }

    public GarantiaAdapter(Context context, List<Garantia> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public GarantiaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.single_garantia, parent, false);
        return new GarantiaAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(GarantiaAdapter.ViewHolder holder, int position){
        Garantia garantia = list.get(position);
        holder.fecha.setText(garantia.getFecha());
        holder.folio.setText(garantia.getFolio()+" | "+garantia.getStatus());
        holder.estado.setText(garantia.getDescripcion());
        holder.imei.setText(garantia.getImei());
        holder.marcamod.setText(garantia.getMarca()+" "+garantia.getModelo());
    }

    public void updateList(List<Garantia> list){
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount(){
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView estado, fecha, imei, folio, marcamod;
        public ViewHolder (View itemView){
            super(itemView);
            estado = itemView.findViewById(R.id.gar_estado);
            fecha = itemView.findViewById(R.id.gar_fecha);
            imei = itemView.findViewById(R.id.gar_imei);
            folio = itemView.findViewById(R.id.gar_folio);
            marcamod = itemView.findViewById(R.id.gar_marcamodelo);
        }
    }
}
