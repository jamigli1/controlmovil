package com.microtec.cmovil.Interactor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.microtec.cmovil.Model.Transferencia;
import com.microtec.cmovil.R;

import java.util.List;

public class TransferenciaRAdapter extends RecyclerView.Adapter<TransferenciaRAdapter.ViewHolder> {

    private Context context;
    private List<Transferencia> transferencias;
    private int row_index;

    public TransferenciaRAdapter(Context context, List<Transferencia> transferencias){
        this.context = context;
        this.transferencias = transferencias;
        this.row_index = -1;
    }

    @Override
    public TransferenciaRAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.single_transferenciarel, parent, false);
        return new TransferenciaRAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TransferenciaRAdapter.ViewHolder holder, final int position){
        Transferencia transferencia = transferencias.get(position);
        holder.tvFolio.setText(transferencia.getFolio());
        holder.tvGrupoFecha.setText(transferencia.getOrigen()+"\n"+transferencia.getFecha());
        holder.tvGrupoCel.setText(transferencia.getMarca()+" "+transferencia.getModelo()+"\n"+transferencia.getImei());

    }

    @Override
    public int getItemCount(){
        return transferencias.size();
    }

    public void updateList(List<Transferencia> list){
        transferencias = list;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvFolio, tvGrupoFecha, tvGrupoCel;
        public ViewHolder (View itemView){
            super(itemView);
            tvFolio = itemView.findViewById(R.id.folio_transf_t1);
            tvGrupoFecha = itemView.findViewById(R.id.grupoFecha_t1);
            tvGrupoCel = itemView.findViewById(R.id.grupoDestino_t1);
        }
    }
}
