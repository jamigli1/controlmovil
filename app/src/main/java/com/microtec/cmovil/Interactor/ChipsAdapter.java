package com.microtec.cmovil.Interactor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.microtec.cmovil.Model.Producto;
import com.microtec.cmovil.R;

import java.util.List;

public class ChipsAdapter extends RecyclerView.Adapter<ChipsAdapter.ViewHolder> {

    private Context context;
    private List<Producto> list;
    private int row_index;

    public ChipsAdapter(Context context, List<Producto> list) {
        this.context = context;
        this.list = list;
        this.row_index = -1;
    }

    @Override
    public ChipsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.single_chip,parent,false);
        return new ChipsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ChipsAdapter.ViewHolder holder, final int position) {
        Producto producto = list.get(position);
        holder.marca.setText(producto.getMarca());
        holder.precio.setText("$".concat(producto.getPrecio()));
        holder.precioR.setText(producto.getPrecio());
        holder.precioK.setText(producto.getPrecioK());
        holder.serie.setText(producto.getSerie());
        holder.tipo.setText(producto.getTipo());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView marca, precio, precioR, serie, tipo, precioK;
        public ViewHolder(View itemView) {
            super(itemView);
            marca = itemView.findViewById(R.id.chip_marca);
            precio = itemView.findViewById(R.id.chip_monto);
            precioR = itemView.findViewById(R.id.chip_precioR);
            serie = itemView.findViewById(R.id.chip_serie);
            tipo = itemView.findViewById(R.id.chip_tipo);
            precioK = itemView.findViewById(R.id.chip_precioK);
        }
    }

}
