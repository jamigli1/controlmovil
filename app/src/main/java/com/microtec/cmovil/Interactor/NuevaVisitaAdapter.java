package com.microtec.cmovil.Interactor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.microtec.cmovil.Model.PuntoVenta;
import com.microtec.cmovil.R;

import java.util.ArrayList;
import java.util.List;

public class NuevaVisitaAdapter extends RecyclerView.Adapter<NuevaVisitaAdapter.ViewHolder> {

    private Context context;
    private List<PuntoVenta> list;

    public NuevaVisitaAdapter(ArrayList<PuntoVenta> pventas){
        this.list = pventas;
    }

    public NuevaVisitaAdapter(Context context, List<PuntoVenta> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public NuevaVisitaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.single_nuevavisita, parent, false);
        return new NuevaVisitaAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NuevaVisitaAdapter.ViewHolder holder, int position){
        PuntoVenta puntoVenta = list.get(position);
        holder.textTitle.setText(puntoVenta.getNombre());
        holder.textCliente.setText(puntoVenta.getCodigo());
        holder.txtIdPv.setText(puntoVenta.getId());
    }

    @Override
    public int getItemCount(){
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textTitle, textCliente, txtIdPv;
        public ViewHolder (View itemView){
            super(itemView);
            textTitle = itemView.findViewById(R.id.nvisita_nombe);
            textCliente = itemView.findViewById(R.id.nvisita_codigo);
            txtIdPv = itemView.findViewById(R.id.nvisita_idpv);
        }
    }
}
