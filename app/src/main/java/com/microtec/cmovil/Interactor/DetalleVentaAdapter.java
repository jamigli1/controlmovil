package com.microtec.cmovil.Interactor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.microtec.cmovil.Model.PuntoVenta;
import com.microtec.cmovil.R;

import java.util.List;

public class DetalleVentaAdapter extends RecyclerView.Adapter<DetalleVentaAdapter.ViewHolder> {

    private Context context;
    private List<PuntoVenta> list;

    public DetalleVentaAdapter(Context context, List<PuntoVenta> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public DetalleVentaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.single_det_venta, parent, false);
        return new DetalleVentaAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DetalleVentaAdapter.ViewHolder holder, int position){
        PuntoVenta puntoVenta = list.get(position);
        holder.txtDecripcion.setText(puntoVenta.getNombre());
        holder.txtFolio.setText(puntoVenta.getCodigo());
        holder.txtMonto.setText("$".concat(puntoVenta.getTotal()));
    }

    @Override
    public int getItemCount(){
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtDecripcion, txtFolio, txtMonto;
        public ViewHolder (View itemView){
            super(itemView);
            txtDecripcion = itemView.findViewById(R.id.det_venta_descripcion);
            txtFolio = itemView.findViewById(R.id.det_venta_folio);
            txtMonto = itemView.findViewById(R.id.det_venta_monto);
        }
    }

}
