package com.microtec.cmovil.Interactor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.microtec.cmovil.Model.Cliente;
import com.microtec.cmovil.R;

import java.util.List;

public class SolicitudesAdapter extends RecyclerView.Adapter<SolicitudesAdapter.ViewHolder> {

    private Context context;
    private List<Cliente> list;

    public SolicitudesAdapter(Context context, List<Cliente> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public SolicitudesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.single_solicitud,parent,false);
        return new SolicitudesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SolicitudesAdapter.ViewHolder holder, int position){
        Cliente clienteC = list.get(position);
        holder.txtUser.setText(clienteC.getNombre());
        holder.txtUser.setText(clienteC.getTipo());
    }

    @Override
    public int getItemCount(){
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView txtUser, txtNneg;

        public ViewHolder(View itemView) {
            super(itemView);
            txtUser = itemView.findViewById(R.id.solicitud_user);
            txtNneg = itemView.findViewById(R.id.solicitud_nneg);
        }
    }
}
