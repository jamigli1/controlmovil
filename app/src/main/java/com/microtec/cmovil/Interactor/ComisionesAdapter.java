package com.microtec.cmovil.Interactor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.microtec.cmovil.Model.Comision;
import com.microtec.cmovil.R;

import java.util.List;

public class ComisionesAdapter extends RecyclerView.Adapter<ComisionesAdapter.ViewHolder> {

    private Context context;
    private List<Comision> list;

    public ComisionesAdapter(Context context, List<Comision> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ComisionesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.single_comision, parent, false);
        return new ComisionesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ComisionesAdapter.ViewHolder holder, int position){
        Comision comision = list.get(position);
        holder.fecha.setText(comision.getFecha());
        holder.concepto.setText(comision.getConcepto());
        holder.serie.setText(comision.getDistribuidor());;
        holder.status.setText(comision.getStatus());
        holder.montobtm.setText(comision.getComision());
        if (!comision.getSerie().equals("0")) {
            holder.relat.setVisibility(View.VISIBLE);
            holder.cliente.setText(comision.getSerie());
        }
    }

    @Override
    public int getItemCount(){
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView fecha, concepto, serie, status, montobtm, cliente;
        public View relat;

        public ViewHolder(View itemView){
            super(itemView);
            concepto = itemView.findViewById(R.id.single_comis_concepto);
            serie = itemView.findViewById(R.id.single_comis_serie);
            fecha = itemView.findViewById(R.id.single_comis_fecha);
            montobtm = itemView.findViewById(R.id.single_comis_montobottom);
            status = itemView.findViewById(R.id.single_comis_status);
            cliente = itemView.findViewById(R.id.single_comis_cliente);
            relat = itemView.findViewById(R.id.single_comis_clientelay);
        }

    }
}
