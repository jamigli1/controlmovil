package com.microtec.cmovil.Interactor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.microtec.cmovil.Model.PuntoVenta;
import com.microtec.cmovil.R;

import java.util.List;

public class VentasAdapter extends RecyclerView.Adapter<VentasAdapter.ViewHolder>{

    private Context context;
    private List<PuntoVenta> list;

    public VentasAdapter(Context context, List<PuntoVenta> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public VentasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.single_venta, parent, false);
        return new VentasAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(VentasAdapter.ViewHolder holder, int position){
        PuntoVenta puntoVenta = list.get(position);
        holder.textNombre.setText(puntoVenta.getNombre());
        holder.textFecha.setText(puntoVenta.getFecha());
        holder.textTotal.setText(puntoVenta.getTotal());
        holder.textFolio.setText(puntoVenta.getFolio());
    }

    @Override
    public int getItemCount(){
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textNombre, textFecha, textTotal, textFolio;
        public ViewHolder (View itemView){
            super(itemView);
            textNombre = itemView.findViewById(R.id.venta_nombre);
            textFecha = itemView.findViewById(R.id.venta_fecha);
            textTotal = itemView.findViewById(R.id.venta_total);
            textFolio = itemView.findViewById(R.id.venta_folio);

        }
    }
}
