package com.microtec.cmovil.Interactor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.microtec.cmovil.Model.Producto;
import com.microtec.cmovil.R;

import java.util.List;

public class ProductosAdapter extends RecyclerView.Adapter<ProductosAdapter.ViewHolder> {

    private Context context;
    private List<Producto> list;
    private int row_index;

    public ProductosAdapter(Context context, List<Producto> list) {
        this.context = context;
        this.list = list;
        this.row_index = -1;
    }

    public void updateList(List<Producto> productos){
        list = productos;
        notifyDataSetChanged();
    }

    @Override
    public ProductosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.single_producto,parent,false);
        return new ProductosAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Producto producto = list.get(position);
        holder.marca.setText(producto.getMarca());
        holder.modelo.setText(producto.getModelo());
        holder.precio.setText("$".concat(producto.getPrecioR()));
        holder.precioR.setText(producto.getPrecio());
        holder.serie.setText(producto.getSerie());
        holder.tipo.setText(producto.getTipo());

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = position;
                notifyDataSetChanged();
            }
        });

        if (row_index == position){
            holder.linearLayout.setBackgroundResource(R.drawable.lyt_rounded_selected);
            holder.imageView.setImageResource(R.drawable.ic_select);
        } else {
            holder.linearLayout.setBackgroundResource(R.color.whiteColor);
            holder.imageView.setImageResource(R.drawable.ic_unselect);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView marca, modelo, precio, precioR, serie, tipo;
        public LinearLayout linearLayout;
        public ImageView imageView;
        public ViewHolder(View itemView) {
            super(itemView);
            marca = itemView.findViewById(R.id.prod_marca);
            modelo = itemView.findViewById(R.id.prod_modelo);
            precio = itemView.findViewById(R.id.prod_monto);
            precioR = itemView.findViewById(R.id.prod_precioR);
            serie = itemView.findViewById(R.id.prod_serie);
            tipo = itemView.findViewById(R.id.prod_tipo);
            linearLayout = itemView.findViewById(R.id.sngle_prdcto);
            imageView = itemView.findViewById(R.id.img_checked);
        }
    }
}
