package com.microtec.cmovil.Interactor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.microtec.cmovil.Model.Transferencia;
import com.microtec.cmovil.R;

import java.util.List;

public class TransferenciaAdapter extends RecyclerView.Adapter<TransferenciaAdapter.ViewHolder> {

    private Context context;
    private List<Transferencia> transferencias;
    private int row_index;

    public TransferenciaAdapter(Context context, List<Transferencia> transferencias){
        this.context = context;
        this.transferencias = transferencias;
        this.row_index = -1;
    }

    @Override
    public TransferenciaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.single_transferencia, parent, false);
        return new TransferenciaAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TransferenciaAdapter.ViewHolder holder, final int position){
        Transferencia transferencia = transferencias.get(position);
        holder.tvFolio.setText(transferencia.getFolio());
        holder.tvGrupoFecha.setText(transferencia.getFecha()+"\nDías desde la captura: "+transferencia.getDias());
        holder.tvGrupoDestino.setText(transferencia.getDest_nombre()+"\n"+transferencia.getDestino());
        holder.tvNoEquipos.setText(transferencia.getN_equipos());

    }

    public void updateList(List<Transferencia> list){
        transferencias = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount(){
        return transferencias.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvFolio, tvGrupoFecha, tvGrupoDestino, tvNoEquipos;
        public ViewHolder (View itemView){
            super(itemView);
            tvFolio = itemView.findViewById(R.id.folio_transf);
            tvGrupoFecha = itemView.findViewById(R.id.grupoFecha);
            tvGrupoDestino = itemView.findViewById(R.id.grupoDestino);
            tvNoEquipos = itemView.findViewById(R.id.cantidadTransf);
        }
    }
}
