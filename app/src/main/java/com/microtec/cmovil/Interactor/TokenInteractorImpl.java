package com.microtec.cmovil.Interactor;

import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.util.Log;

import com.evgenii.jsevaluator.JsEvaluator;
import com.evgenii.jsevaluator.interfaces.JsCallback;
import com.microtec.cmovil.Helper.ControlMovil;
import com.microtec.cmovil.Presenter.TokenPresenter;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class TokenInteractorImpl implements TokenInteractor {
    JsEvaluator mJsEvaluator;
    private Scanner scanner;
    String jsCode;
    TokenPresenter presenter;
    ObtenerTokenAsync task = new ObtenerTokenAsync();

    public TokenInteractorImpl(TokenPresenter presenter) {
        this.presenter = presenter;
        mJsEvaluator = new JsEvaluator(ControlMovil.getAppContext());
    }

    @Override
    public  void obtenerToken(String key){
        this.key_auth = key;
        evaluateAndDisplay(key_auth);
    }
    @Override
    public void stopTask(){
        task.cancel(true);
    }
    private String loadJs(String fileName) {
        try {
            return ReadFile(fileName);
        } catch (final IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected String ReadFile(String fileName) throws IOException {
        final AssetManager am = ControlMovil.getAppContext().getAssets();
        final InputStream inputStream = am.open(fileName);

        scanner = new Scanner(inputStream, "UTF-8");
        return scanner.useDelimiter("\\A").next();
    }

    protected void testCryptoJs(String key_auth) {
        jsCode = "var jsEvaluatorResult = ''; ";
        jsCode += loadJs("javascript/codigo.js");
        jsCode += "; ";
        long  time = System.currentTimeMillis();
        jsCode += "var jsEvaluatorResult = token('"+key_auth+"','"+time+"');";
        jsCode += "jsEvaluatorResult;";
    }

    private String key_auth;


    private static final String TAG = "TokenInteractorImpl";
    public void evaluateAndDisplay(String key_auth) {
        testCryptoJs(key_auth);
        mJsEvaluator.evaluate(jsCode, new JsCallback() {
            @Override
            public void onResult(final String resultValue) {
                presenter.setTokenInView(resultValue);
            }

            @Override
            public void onError(String errorMessage) {
                Log.i(TAG, "onError: "+errorMessage);
            }
        });
    }

    class ObtenerTokenAsync extends AsyncTask<String, Long, String> {
        @Override
        protected String doInBackground(String... voids) {
            while (true){
                long time = System.currentTimeMillis()/1000;
                publishProgress(time);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            super.onProgressUpdate(values);
            long time = values[0].longValue();
            int cont = (int)(30 - (time%30));
            if (values[0]%30 == 0){
                evaluateAndDisplay(key_auth);
            }
            presenter.setSecondsInView(""+cont);
        }
    }
}
