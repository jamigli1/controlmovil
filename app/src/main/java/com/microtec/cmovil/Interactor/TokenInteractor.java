package com.microtec.cmovil.Interactor;

public interface TokenInteractor {

    void obtenerToken(String key);

    void stopTask();

}
