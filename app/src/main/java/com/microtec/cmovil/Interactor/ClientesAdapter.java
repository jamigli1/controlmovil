package com.microtec.cmovil.Interactor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.microtec.cmovil.Model.Cliente;
import com.microtec.cmovil.R;

import java.util.List;

public class ClientesAdapter extends RecyclerView.Adapter<ClientesAdapter.ViewHolder> {

    private Context context;
    private List<Cliente> list;

    public ClientesAdapter(Context context, List<Cliente> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ClientesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.single_puntoventa,parent,false);
        return new ClientesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ClientesAdapter.ViewHolder holder, int position){
        Cliente clienteC = list.get(position);
        holder.textCliente.setText(clienteC.getNombre());
        holder.textTipo.setText(clienteC.getDis());
        if (!TextUtils.isEmpty(clienteC.getTipo())) {
            holder.textFecha.setText(clienteC.getTipo());
            holder.textFecha.setVisibility(View.VISIBLE);
        }
    }

    public void updateList(List<Cliente> clientes){
        list = clientes;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount(){
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textCliente, textTipo, textFecha;

        public ViewHolder(View itemView){
            super(itemView);
            textCliente = itemView.findViewById(R.id.pventa_nombre);
            textTipo = itemView.findViewById(R.id.pventa_tipo);
            textFecha = itemView.findViewById(R.id.pventa_fecha);
        }

    }

}
