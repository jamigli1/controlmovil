package com.microtec.cmovil.Interactor;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.Api;
import com.google.gson.Gson;
import com.microtec.cmovil.Helper.ControlMovil;
import com.microtec.cmovil.Model.BuscarFolioCheckIn;
import com.microtec.cmovil.Model.Inventario;
import com.microtec.cmovil.Presenter.CheckInPresenter;
import com.microtec.cmovil.Utils.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheckInInteractorImpl implements CheckInInteractor {
    private Api service;
//    private Call<RequestResponse> call;
    private CheckInPresenter presenter;
    public CheckInInteractorImpl(CheckInPresenter presenter) {
        this.presenter = presenter;
//        service = RetrofitClient.createService(Api.class);
    }

    @Override
    public void buscarFolioCheckIn(final String folio, final String tipo1, final String tipo2){

        final String dis = AppConstants.micdis;

        if (!dis.equals("-1")) {
            String url = "https://www.micro-tec.com.mx/pagina/app/clases/soportec/api/api_soportec.php";
            RequestQueue requestQueue = Volley.newRequestQueue(ControlMovil.getAppContext());

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("Respuesta", response);
                    try{
                        JSONObject jsonObject = new JSONObject(response);
                        ArrayList<BuscarFolioCheckIn> list = new ArrayList<>();
                        for (int i=0; i<jsonObject.getJSONArray("r_checkin").length(); i++){
                            Gson gson = new Gson();
                            list.add(gson.fromJson(jsonObject.getJSONArray("r_checkin").get(i).toString(), BuscarFolioCheckIn.class));
                        }
                        presenter.showInfo(list);
                        try{
                            if (jsonObject.get("is_check").equals(null) || jsonObject.get("is_check").equals("checkout")){
                                presenter.showButtonCheckIn(true);
                            } else {
                                presenter.showButtonCheckIn(false);
                            }
                        } catch (Exception e){
                            presenter.showButtonCheckIn(true);
                        }
                    }catch(JSONException exception){
                        presenter.showMessage("Error buscando folio...");
                    }

                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            presenter.showMessage("Error, revisa tu conexión a internet...");
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("callback","buscarFolioCheckIn");
                    params.put("mpy_dis",dis);
                    params.put("mpy_folio",folio);
                    params.put("mpy_tipo",tipo1);
                    params.put("mpy_tipo2","algo");
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }

    }

    @Override
    public void checkInOut(final String folio, final String comentario, final String tipo, final String tipo_tramite){

        final String dis = AppConstants.micdis;
        SharedPreferences settings = ControlMovil.getAppContext().getSharedPreferences(AppConstants.userPreferences, ControlMovil.getAppContext().MODE_PRIVATE);
        final String agente = settings.getString("agente", "-1");

        Log.d("Tipotramite", tipo_tramite);

        if (!dis.equals("-1")) {
            String url = "https://www.micro-tec.com.mx/pagina/app/clases/soportec/api/api_soportec.php";
//            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/GarantiasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(ControlMovil.getAppContext());

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("RespuestaG", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        AlertDialog.Builder builder = new AlertDialog.Builder(AppConstants.context2);
                        builder.setTitle("MENSAJE");
                        builder.setMessage(jsonObject.get("msg").toString());
                        builder.show();
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            presenter.showMessage("Error, revisa tu conexión a internet...");
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("callback","checkIn_Out");
                    params.put("mpy_dis",dis);
                    params.put("mpy_folio",folio);
                    params.put("mpy_tipo",tipo);
                    params.put("mpy_agente",agente);
                    params.put("mpy_nom_sucursal",tipo);
                    params.put("mpy_comentario",comentario);
                    params.put("mpy_tipo_tramite",tipo_tramite);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        } else {
            Log.d("aver", "fffffffff");
        }

    }

    @Override
    public void checkInOutPaquete(String folio, String comentario, String tipo, String tipo_tramite, List<Inventario> lista){
        /*String agente = Utils.getSharedPref().getString(Constantes.PREF_MPY_NOMBRE,"");
        String sucursal = Utils.getSharedPref().getString(Constantes.PREF_MPY_SUC,"");
        String dis = Utils.getSharedPref().getString(Constantes.PREF_MPY_DIS,"");

        JSONArray myarray = new JSONArray();
        for(int i=0;i<lista.size(); i++){
            myarray.put(lista.get(i).toJsonObject());
        }


        call = service.checkInOut(folio,agente,sucursal,comentario,dis,tipo,tipo_tramite,myarray,Api.CHECKIN_CHECKOUT_PAQUETE);
        call.enqueue(new Callback<RequestResponse>() {
            @Override
            public void onResponse(Call<RequestResponse> call, Response<RequestResponse> response) {

            }

            @Override
            public void onFailure(Call<RequestResponse> call, Throwable t) {

            }
        });*/
    }
}
