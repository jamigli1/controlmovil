package com.microtec.cmovil.Interactor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.microtec.cmovil.Model.Pendiente;
import com.microtec.cmovil.Model.PuntoVenta;
import com.microtec.cmovil.R;

import java.util.List;

public class ReminderAdapter extends RecyclerView.Adapter<ReminderAdapter.ViewHolder> {

    private Context context;
    private List<Pendiente> list;

    public ReminderAdapter(Context context, List<Pendiente> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ReminderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.single_pendiente, parent, false);
        return new ReminderAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ReminderAdapter.ViewHolder holder, int position) {
        Pendiente pendiente = list.get(position);
        holder.textNombre.setText(pendiente.getNombre());
        holder.textComment.setText(pendiente.getMensaje());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textNombre, textComment;

        public ViewHolder(View itemView) {
            super(itemView);
            textNombre = itemView.findViewById(R.id.nombre_pv_reminder);
            textComment = itemView.findViewById(R.id.comentario_pv_reminder);
        }
    }
}