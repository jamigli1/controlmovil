package com.microtec.cmovil.Interactor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.vipulasri.timelineview.TimelineView;
import com.microtec.cmovil.Model.Visita;
import com.microtec.cmovil.R;

import java.util.List;

public class VisitasAdapter extends RecyclerView.Adapter<VisitasAdapter.ViewHolder> {

    private Context context;
    private List<Visita> list;
    private String[] meses = new String[]{
            "Enero",
            "Febebro",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"};

    public VisitasAdapter(Context context, List<Visita> list){
        this.context = context;
        this.list = list;
    }

    @Override
    public VisitasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(context).inflate(R.layout.single_visita,parent,false);
        return new VisitasAdapter.ViewHolder(v, viewType);
    }

    @Override
    public void onBindViewHolder(VisitasAdapter.ViewHolder holder, int position){
        Visita oVisitas = list.get(position);
        holder.cliente.setText(oVisitas.getNombre());
        String [] arrFecha = oVisitas.getFecha().split("-");
        holder.hora.setText(oVisitas.getDescripcion().concat(" a las ").concat(oVisitas.getHora()));
        holder.dia.setText(arrFecha[2]);
        holder.mes.setText(meses[Integer.parseInt(arrFecha[1])-1]);
        holder.anio.setText(arrFecha[0]);
    }

    @Override
    public int getItemCount(){
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView cliente, hora, dia, mes, anio;
        public TimelineView timelineView;

        public ViewHolder(View itemView, int viewType){
            super(itemView);
            cliente = itemView.findViewById(R.id.visita_nombre);
            hora = itemView.findViewById(R.id.visita_hora);
            dia = itemView.findViewById(R.id.visita_dia);
            mes = itemView.findViewById(R.id.visita_mes);
            anio = itemView.findViewById(R.id.visita_anio);
            timelineView = (TimelineView) itemView.findViewById(R.id.timeline);
            timelineView.initLine(viewType);
        }
    }

    @Override
    public int getItemViewType(int position){
        return TimelineView.getTimeLineViewType(position,getItemCount());
    }
}
