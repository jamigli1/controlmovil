package com.microtec.cmovil.Interactor;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.microtec.cmovil.Helper.ControlMovil;
import com.microtec.cmovil.Model.BuscarFolioCheckIn;
import com.microtec.cmovil.R;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class AdapterBuscarFolio extends RecyclerView.Adapter<AdapterBuscarFolio.ViewHolder>{
    List<BuscarFolioCheckIn> lista;
    public AdapterBuscarFolio(List<BuscarFolioCheckIn> inventarios) {
        this.lista = inventarios;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_paquete_check_in, parent, false);
        return new AdapterBuscarFolio.ViewHolder(v);    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String origen = lista.get(position).getDos();
        String destino = lista.get(position).getTres();
        try {
            origen = new String(lista.get(position).getDos().getBytes("ISO-8859-15"), "UTF-8");
            destino= new String(lista.get(position).getTres().getBytes("ISO-8859-15"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Log.d("Adapter", lista.get(position).getUno());

        holder.itemPaqueteContentTvOne.setText(lista.get(position).getUno());
        holder.itemPaqueteContentTvTwo.setText(origen);
        if (lista.get(position).getTres().equals("")){
            holder.itemPaqueteContentTvThree.setVisibility(View.GONE);
            holder.itemPaqueteContentCheckin.setCardBackgroundColor(ControlMovil.getAppContext().getResources().getColor(R.color.color_bg_card));
        }
        else
            holder.itemPaqueteContentTvThree.setText(destino);

        holder.itemPaqueteTitleTvOne.setText(lista.get(position).getUno_a());
        holder.itemPaqueteTitleTvTwo.setText(lista.get(position).getDos_a());
        if (lista.get(position).getTres_a().equals(""))
            holder.itemPaqueteTitleTvThree.setVisibility(View.GONE);
        else
            holder.itemPaqueteTitleTvThree.setText(lista.get(position).getTres_a());

//        holder.cardContent1ResultCheckIn.setVisibility(View.VISIBLE);
    }

    public List<BuscarFolioCheckIn> getCheckList() {
        return lista;
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        CardView itemPaqueteContentCheckin;
        TextView itemPaqueteTitleTvOne;
        TextView itemPaqueteContentTvOne;
        TextView itemPaqueteTitleTvTwo;
        TextView itemPaqueteContentTvTwo;
        TextView itemPaqueteTitleTvThree;
        TextView itemPaqueteContentTvThree;

        ViewHolder(View view) {
            super(view);
            itemPaqueteContentCheckin = view.findViewById(R.id.item_paquete_content_checkin);
            itemPaqueteTitleTvOne = view.findViewById(R.id.item_paquete_title_tv_one);
            itemPaqueteContentTvOne = view.findViewById(R.id.item_paquete_content_tv_one);
            itemPaqueteTitleTvTwo = view.findViewById(R.id.item_paquete_title_tv_two);
            itemPaqueteContentTvTwo = view.findViewById(R.id.item_paquete_content_tv_two);
            itemPaqueteTitleTvThree = view.findViewById(R.id.item_paquete_title_tv_three);
            itemPaqueteContentTvThree = view.findViewById(R.id.item_paquete_content_tv_three);
        }
    }
}
