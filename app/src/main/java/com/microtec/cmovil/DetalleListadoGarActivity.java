package com.microtec.cmovil;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.transition.Slide;
import android.support.transition.Transition;
import android.support.transition.TransitionManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.microtec.cmovil.Interactor.HistorialAdapter;
import com.microtec.cmovil.Model.Garantia;
import com.microtec.cmovil.Model.Historial;
import com.microtec.cmovil.Utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DetalleListadoGarActivity extends AppCompatActivity {

    private View inc1, inc2, lin1, lin2, lin3, lin4, lin5, lin6;
    private TextView header, label1, label2, label3, label4;
    private TextView label1_1, label2_2, label3_3, label4_4, label5_5, label6_6, label7_7, label8_8;
    private TextView foliogar, tipogar;

    private RecyclerView mList;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private List<Historial> historials;

    private ArrayList<String> datos;

    Bundle extras;
    Garantia objeto;

    private BottomNavigationView menubtm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_listadogar);
        initElementos();
        verDetalles();
        Log.d("OBSER",objeto.getComentarios());
    }

    public void setHistorials(int tipo) {
        historials.clear();
        String[] historialX;

        if (tipo == 0) {
            historialX = objeto.getHistorial().split("<br>");
        } else {
            historialX = objeto.getComentarios().split("<br>");
        }

        if (historialX.length>1) {

            for (int i = historialX.length - 1; i >= 0; i--) {
                Historial historial = new Historial();
                historial.setFecha(historialX[i].substring(0, 19));
                historial.setDescripcion(historialX[i].substring(20));
                historials.add(historial);
            }

        }

        mAdapter.notifyDataSetChanged();
    }

    public void makeComment(final String comentario){

        SharedPreferences settings = this.getSharedPreferences(AppConstants.userPreferences, this.MODE_PRIVATE);
        final String dis = settings.getString("dis", "-1");
        final String nombre = settings.getString("agente", "-1");

        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/GarantiasA.php";
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        final String folio = extras.getString("folio","-1");

        if (!dis.equals("-1") && !folio.equals("-1")) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("Respuesta", response);
                    Toast.makeText(DetalleListadoGarActivity.this, response, Toast.LENGTH_SHORT).show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    },2000);
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(DetalleListadoGarActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method", "addCommentsSingle");
                    params.put("folio", folio);
                    params.put("comment", comentario);
                    params.put("evento", "Comentario");
                    params.put("garan_nombre", nombre);
                    params.put("garan_dis", dis);
                    return params;
                }
            };
            requestQueue.add(stringRequest);

        }
    }

    public void initElementos(){

        extras = getIntent().getExtras();
        objeto = (Garantia) extras.getSerializable("objeto");

        datos = new ArrayList<>();

        inc1 = findViewById(R.id.datos_detallegar);
        inc2 = findViewById(R.id.historial_gar);
        header = inc1.findViewById(R.id.tipo_informacion);
        lin1 = inc1.findViewById(R.id.layout1_info);
        lin2 = inc1.findViewById(R.id.layout2_info);
        lin3 = inc1.findViewById(R.id.layout3_info);
        lin4 = inc1.findViewById(R.id.layout4_info);
        lin5 = inc1.findViewById(R.id.layout5_info);
        lin6 = inc1.findViewById(R.id.layout6_info);
        label1 = inc1.findViewById(R.id.text_label1);
        label1_1 = inc1.findViewById(R.id.text_label11);
        label2 = inc1.findViewById(R.id.text_label2);
        label2_2 = inc1.findViewById(R.id.text_label22);
        label3 = inc1.findViewById(R.id.text_label3);
        label3_3 = inc1.findViewById(R.id.text_label33);
        label4 = inc1.findViewById(R.id.text_label4);
        label4_4 = inc1.findViewById(R.id.text_label44);
        label5_5 = inc1.findViewById(R.id.text_label55);
        label6_6 = inc1.findViewById(R.id.text_label66);
        label7_7 = inc1.findViewById(R.id.text_label77);
        label8_8 = inc1.findViewById(R.id.text_label88);

        foliogar = findViewById(R.id.folio_gardet);
        tipogar = findViewById(R.id.tipo_gardet);

        menubtm = findViewById(R.id.navbottom_elemento);

        mList = inc2.findViewById(R.id.rView_historial);
        historials = new ArrayList<>();
        mAdapter = new HistorialAdapter(getApplicationContext(),historials);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(),0);

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(mAdapter);

        menubtm.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.navbar_equipo:
                        try {
                            refreshData(0);
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                        inc2.setVisibility(View.GONE);
                        inc1.setVisibility(View.VISIBLE);
                        return true;
                    case R.id.navbar_cliente:
                        inc2.setVisibility(View.GONE);
                        inc1.setVisibility(View.VISIBLE);
                        try {
                            refreshData(1);
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                        return true;
                    case R.id.navbar_fallos:
                        try {
                            refreshData(2);
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                        inc2.setVisibility(View.GONE);
                        inc1.setVisibility(View.VISIBLE);
                        return true;
                    case R.id.navbar_historial:
                        setHistorials(0);
                        inc1.setVisibility(View.GONE);
                        inc2.setVisibility(View.VISIBLE);
                        return true;
                    case R.id.navbar_observaciones:
                        setHistorials(1);
                        inc1.setVisibility(View.GONE);
                        inc2.setVisibility(View.VISIBLE);
                        return true;
                }
                return false;
            }
        });

        if (objeto.getFcaja().contains("1")){
            tipogar.setText("Fallo fuera de caja");
        }

        foliogar.setText(objeto.getFolio());

        primerForm();

    }

    public void primerForm(){
        try {
            header.setText("Datos del equipo");
            label1.setText("IMEI");
            label1_1.setText(objeto.getImei());
            label2.setText(objeto.getMarca());
            label2_2.setText(objeto.getModelo());
            label3.setText("Fecha de captura");
            label3_3.setText(objeto.getFecha());
            label4.setText("Fecha de compra");
            label4_4.setText(objeto.getFcompra());
            label5_5.setText(objeto.getFventa());
            label6_6.setText(objeto.getFactelcel());
            label7_7.setText(objeto.getFacmicro());
        } catch (Exception e){
            Log.d("EL ERROR", e.getMessage());
        }
    }

    public void refreshData(int tipo){
        if (tipo == 0){ //Primer form
            primerForm();
            label8_8.setText(datos.get(0));
            lin1.setVisibility(View.VISIBLE);
            lin2.setVisibility(View.VISIBLE);
            lin3.setVisibility(View.VISIBLE);
            lin4.setVisibility(View.VISIBLE);
            lin5.setVisibility(View.VISIBLE);
            lin6.setVisibility(View.VISIBLE);
        } else if (tipo == 1){ //Segundo form
            header.setText("Datos del cliente");
            label1.setText("Nombre");
            label1_1.setText(datos.get(2));
            label2.setText("Direccion");
            label2_2.setText(datos.get(9));
            label3.setText("Telefono");
            label3_3.setText(objeto.getTelefono());
            label4.setText("Correo electronico");
            label4_4.setText(objeto.getEmail());
            lin3.setVisibility(View.GONE);
            lin4.setVisibility(View.GONE);
            lin5.setVisibility(View.GONE);
            lin6.setVisibility(View.GONE);
        } else { //Tercer form
            header.setText("Accesorios y fallo del equipo");
            label1.setText("Accesorios");
            label1_1.setText(objeto.getChecklist());
            label2.setText("Fallo del equipo");
            label2_2.setText(objeto.getFallo());
            lin1.setVisibility(View.GONE);
            lin2.setVisibility(View.GONE);
            lin3.setVisibility(View.GONE);
            lin4.setVisibility(View.GONE);
            lin5.setVisibility(View.GONE);
            lin6.setVisibility(View.GONE);
        }
    }

    public void verDetalles() {

        final String imei = extras.getString("imei","");
        final String folio = extras.getString("folio","");

        if (folio != null) {
            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/GarantiasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("Respuesta", response);
                    try{
                        JSONObject object = new JSONObject(response);
                        putDetalles(object);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(DetalleListadoGarActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method", "getDetalles2");
                    params.put("folio", folio);
                    params.put("imei", imei);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void putDetalles(JSONObject object) throws JSONException {
        Iterator x = object.keys();
        JSONArray jsonArray = new JSONArray();

        while (x.hasNext()){
            String key = (String) x.next();
            Log.d("OBJETO",object.get(key).toString());
            jsonArray.put(object.get(key));
        }

        object = jsonArray.getJSONObject(0);
        x = object.keys();

        while (x.hasNext()){
            String key = (String) x.next();
            Log.d("OBJETO",object.get(key).toString());
            datos.add(object.get(key).toString());
        }

        label8_8.setText(datos.get(0));

    }

    public void hacerComentario(View view){
        View mView = getLayoutInflater().inflate(R.layout.alert_dialog,null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.btnDialog_opc1);
        Button btnopc2 = (Button) mView.findViewById(R.id.btnDialog_opc2);
        Button btncancel = (Button) mView.findViewById(R.id.btnDialog_cancel);
        final EditText rfc = (EditText) mView.findViewById(R.id.et_rfc);
        TextView header = (TextView) mView.findViewById(R.id.header);

        rfc.setHint("INGRESE COMENTARIO");

        dialog.setCancelable(false);

        rfc.setVisibility(View.VISIBLE);
        rfc.setInputType(InputType.TYPE_CLASS_TEXT);

        btnopc1.setText("AGREGAR");
        btnopc2.setText(R.string.cancelar);
        header.setText("INGRESE UN COMENTARIO PARA ESTA GARANTÍA");

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(rfc.getText())){
                    makeComment(rfc.getText().toString());
                    dialog.dismiss();
                } else {
                    Toast.makeText(DetalleListadoGarActivity.this, "El comentario no puede estar vacío.", Toast.LENGTH_SHORT).show();
                    rfc.setError("INGRESA COMENTARIO");
                }
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btncancel.setVisibility(View.GONE);

        dialog.show();
    }
}
