package com.microtec.cmovil;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.microtec.cmovil.Interactor.DetTransfAdapter;
import com.microtec.cmovil.Interactor.GarantiaAdapter;
import com.microtec.cmovil.Model.Garantia;
import com.microtec.cmovil.Utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DetalleTransfActivity extends AppCompatActivity {

    private RecyclerView mList;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private List<Garantia> list;
    private List<Garantia> backup;

    private DetTransfAdapter adapter;
    private EditText buscaGar;
    private ProgressDialog progressDialog;

    private FloatingActionButton acciones;

    Bundle extras;

    private int tipo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_transf);
        initElementos();
        verDetalles();
    }

    public void initElementos(){

        extras = getIntent().getExtras();
        tipo = extras.getInt("tipo");
        acciones = findViewById(R.id.fbuton_acciones);

        if (tipo == 1 || tipo == 2 ) {
            acciones.hide();
            acciones.setEnabled(false);
        }

        buscaGar = findViewById(R.id.busquedaTransDet);

        backup = new ArrayList<>();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando lista de garantías...");
        progressDialog.show();

        mList = (RecyclerView) findViewById(R.id.rView_ListaDetTransf);
        list = new ArrayList<>();

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        adapter = new DetTransfAdapter(getApplicationContext(), list);

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(adapter);

        buscaGar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filter(editable.toString());
            }
        });

    }

    private void filter(String text){
        List<Garantia> temp = new ArrayList();
        for (Garantia garantia : list){
            if (garantia.getFolio().toLowerCase().contains(text.toLowerCase())){
                temp.add(garantia);
            }
        }
        backup = temp;
        adapter.updateList(temp);
    }

    public void onClickAcciones(View view){
        switch (tipo){
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                makeQuestion("Aceptar transferencia", "¿Desea aceptar esta transferencia?", 1);
                break;
        }
    }

    public void makeQuestion(String titulo, String mensaje, final int tipo){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setCancelable(false);
        builder.setMessage(mensaje);
        builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (tipo){
                    case 0:
                        mObserv();
                        break;
                    case 1:
                        sendAcceptRequest();
                        break;
                }
            }
        });
        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.show();
    }

    public void mObserv(){
        View mView = getLayoutInflater().inflate(R.layout.alert_image,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);
        builder.setCancelable(false);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = mView.findViewById(R.id.btn_img_ok);
        Button btnopc2 = mView.findViewById(R.id.btn_img_cancel);
        final EditText editText = mView.findViewById(R.id.modif_observ);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(editText.getText())){
                    dialog.dismiss();
                } else {
                    editText.setError("CAMPO REQUERIDO");
                }
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void sendAcceptRequest(){
        SharedPreferences settings = this.getSharedPreferences(AppConstants.userPreferences,this.MODE_PRIVATE);
        final String agente = settings.getString("agente","-1");
        final String dis = settings.getString("dis","-1");
        final String folio = extras.getString("folio");

        if (folio != null && !dis.equals("-1")) {
            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/GarantiasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(final String response) {
                    Log.d("Respuesta",response);
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetalleTransfActivity.this);
                    builder.setTitle("¡MENSAJE!");
                    builder.setMessage(response);
                    builder.setCancelable(false);
                    builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (response.toLowerCase().contains("error")){
                                dialogInterface.dismiss();
                            } else {
                                dialogInterface.dismiss();
                                finish();
                            }
                        }
                    });
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(DetalleTransfActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","acceptTransfer");
                    params.put("folio",folio);
                    params.put("garan_dis",dis);
                    params.put("garan_nombre",agente);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void verDetalles(){

        final String folio = extras.getString("folio");
        final String tipolista = extras.getString("list");

        if (folio != null) {
            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/GarantiasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("Respuesta",response);
                    try{
                        JSONObject object = new JSONObject(response.substring(1));
                        putDetalles(object);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(DetalleTransfActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","getDetalles");
                    params.put("folio",folio);
                    params.put("list",tipolista);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void putDetalles(JSONObject object) throws JSONException {
        Iterator x = object.keys();
        JSONArray jsonArray = new JSONArray();

        while (x.hasNext()){
            String key = (String) x.next();
            Log.d("OBJETO",object.get(key).toString());
            jsonArray.put(object.get(key));
        }

        String[] folio; String[] marca; String[] modelo; String[] imei; String[] accesorios;

        folio = (jsonArray.getString(0).replace("[","").replace("]","")).split(",");
        imei = (jsonArray.getString(1).replace("[","").replace("]","")).split(",");
        marca = (jsonArray.getString(2).replace("[","").replace("]","")).split(",");
        modelo = (jsonArray.getString(3).replace("[","").replace("]","")).split(",");
        accesorios = (jsonArray.getString(4).replace("[","").replace("]","")).split(",");

        for (int i=0; i< folio.length; i++){
            Garantia garantia = new Garantia();
            garantia.setFolio(folio[i].replace("\"",""));
            garantia.setMarca(marca[i].replace("\"",""));
            garantia.setModelo(modelo[i].replace("\"",""));
            garantia.setImei(imei[i].replace("\"",""));
            garantia.setChecklist(accesorios[i].replace("\"","").replace("|", ","));
            list.add(garantia);
        }

        progressDialog.dismiss();

        adapter.notifyDataSetChanged();

    }
}
