package com.microtec.cmovil;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.microtec.cmovil.Interactor.ChipsAdapter;
import com.microtec.cmovil.Model.LocalDataBase;
import com.microtec.cmovil.Model.Producto;
import com.microtec.cmovil.Utils.AlertResponse;
import com.microtec.cmovil.Utils.Estructura;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ChipsActivity extends AppCompatActivity {

    private static final String prefencesN = "userdata";

    private RecyclerView mListC;

    String ucosto, uruta, udetallista;

    int promodoble = 0, acepta = 0;

    Intent nueva = null;

    private LinearLayoutManager linearLayoutManager2;
    private DividerItemDecoration dividerItemDecoration2;

    private RecyclerView.Adapter mAdapterChips;
    private List<Producto> pChips;

    private List<String> serie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chips);

        mListC = (RecyclerView) findViewById(R.id.rView_VChips);
        pChips = new ArrayList<>();
        serie = new ArrayList<>();

        getPromo();

        mAdapterChips = new ChipsAdapter(getApplicationContext(),pChips);

        linearLayoutManager2 = new LinearLayoutManager(this);
        linearLayoutManager2.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration2 = new DividerItemDecoration(mListC.getContext(),linearLayoutManager2.getOrientation());

        mListC.setHasFixedSize(true);
        mListC.setLayoutManager(linearLayoutManager2);
        mListC.addItemDecoration(dividerItemDecoration2);
        mListC.setAdapter(mAdapterChips);
    }

    public void onClickGetChipsVenta(View view){

        boolean bandera = false;
        final AlertResponse alertResponse = new AlertResponse(ChipsActivity.this);

        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");
        final String agente = settings.getString("agente","-1");

        final EditText etSerie = (EditText) findViewById(R.id.chips_chip_serie);
        final EditText etCantidad = (EditText) findViewById(R.id.chips_chip_cantidad);

        if (!TextUtils.isEmpty(etSerie.getText()) && !TextUtils.isEmpty(etCantidad.getText()) && etSerie.getText().toString().length() == 18){
            if (!serie.contains(etSerie.getText().toString())) {
                serie.add(etSerie.getText().toString());
                bandera = true;
            } else {
                makeAlert("CONTROL MÓVIL","Esta serie ya existe en el listado");
            }
        } else if (etSerie.getText().toString().length() != 18){
            makeAlert("CONTROL MÓVIL", "La serie ingresada no es válida.");
        } else {
            makeAlert("CONTROL MÓVIL","Los campos no deben estar vacíos.");
        }

        if (bandera) {

            if (!dis.equals("-1")) {

                String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VentasA.php";
                RequestQueue requestQueue = Volley.newRequestQueue(this);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object.length() != 0) {
                                generaListC(object);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            alertResponse.showResponse("No se pudo encontrar esa serie");

                        }
                        mAdapterChips.notifyDataSetChanged();
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(ChipsActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("method", "getChips2");
                        params.put("codigo_dis", dis);
                        params.put("id_us", agente);
                        params.put("cantidad", etCantidad.getText().toString());
                        params.put("serie", etSerie.getText().toString());
                        return params;
                    }
                };
                requestQueue.add(stringRequest);
            }

        }
    }

    public void makeAlert(String titulo, String cuerpo){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(cuerpo);
        builder.setCancelable(true);
        builder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alerta = builder.create();
        alerta.show();
    }

    public void generaListC(JSONObject jsonObject) throws JSONException {

        Iterator x = jsonObject.keys();
        JSONArray jsonArray = new JSONArray();

        String[] marca; String[] modelo; String[] serie;
        String[] precio; String[] precioR; String[] precioK;
        String[] tipo; String detallista; String ruta; String costo;

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(jsonObject.get(key));
        }

        marca = (jsonArray.getString(0).replace("[","").replace("]","")).split(",");
        modelo = (jsonArray.getString(1).replace("[","").replace("]","")).split(",");
        serie = (jsonArray.getString(2).replace("[","").replace("]","")).split(",");
        precio = (jsonArray.getString(3).replace("[","").replace("]","")).split(",");
        precioR = (jsonArray.getString(4).replace("[","").replace("]","")).split(",");
        precioK = (jsonArray.getString(5).replace("[","").replace("]","")).split(",");
        tipo = (jsonArray.getString(6).replace("[","").replace("]","")).split(",");
        detallista = jsonArray.getString(9);
        ruta = jsonArray.getString(8);
        costo = jsonArray.getString(10);

        for (int i=0; i<marca.length; i++){
            Producto producto = new Producto();
            producto.setMarca(marca[i].replace("\"",""));
            producto.setModelo(modelo[i].replace("\"",""));
            producto.setSerie(serie[i].replace("\"",""));
            producto.setPrecio(precio[i].replace("\"",""));
            producto.setPrecioR(precioR[i].replace("\"",""));
            producto.setPrecioK(precioK[i].replace("\"",""));
            producto.setTipo(tipo[i].replace("\"",""));
            producto.setRuta(ruta);
            producto.setDetallista(detallista);
            producto.setCosto(costo);

            if (!this.serie.contains(serie[i].replace("\"",""))){
                this.serie.add(serie[i].replace("\"",""));
            }

            pChips.add(producto);
        }
    }

    public void onClickTerminarVentaChips(View view) throws JSONException {
        JSONArray jsonProductos = new JSONArray();

        String detallista = "";
        String ruta = "";
        String costo = "";

        for (int i=0; i<pChips.size(); i++){

            JSONObject objeto = new JSONObject();
            objeto.put("marca",pChips.get(i).getMarca());
            objeto.put("modelo",pChips.get(i).getModelo());
            objeto.put("serie",pChips.get(i).getSerie());
            objeto.put("precio",pChips.get(i).getPrecio());
            objeto.put("precioR",pChips.get(i).getPrecio());
            objeto.put("tipo",pChips.get(i).getTipo());
            objeto.put("precioK",pChips.get(i).getPrecioR());
            objeto.put("ruta",pChips.get(i).getRuta());
            objeto.put("detallista",pChips.get(i).getDetallista());
            objeto.put("costo",pChips.get(i).getCosto());
            detallista = pChips.get(i).getDetallista();
            ruta = pChips.get(i).getRuta();
            costo = pChips.get(i).getCosto();
            jsonProductos.put(objeto);
        }

        Bundle extras = getIntent().getExtras();
        String satcli = extras.getString("satcli");
        String micd = extras.getString("micd");

        Intent resumen = new Intent(this, ResumenActivity.class);
        resumen.putExtra("tipoDeVenta", "solochips");
        resumen.putExtra("satcli", satcli);
        resumen.putExtra("b_ruta", ruta);
        resumen.putExtra("b_costo", costo);
        resumen.putExtra("b_detallista", detallista);
        resumen.putExtra("json", jsonProductos.toString());
        resumen.putExtra("micd", micd);

        nueva = resumen;

        if (jsonProductos.length()>0) {

            if (promodoble == 1){
                makePromo(resumen);
            } else {
                startActivity(resumen);
            }

        } else {
            Toast.makeText(this, "Debe añadir productos para continuar...", Toast.LENGTH_SHORT).show();
        }

    }

    public void onClickPendienteChips(View view) throws JSONException {
        JSONArray jsonProductos = new JSONArray();

        for (int i=0; i<pChips.size(); i++){
            JSONObject objeto = new JSONObject();
            objeto.put("marca",pChips.get(i).getMarca());
            objeto.put("modelo",pChips.get(i).getModelo());
            objeto.put("serie",pChips.get(i).getSerie());
            objeto.put("precio",pChips.get(i).getPrecio());
            objeto.put("precioR",pChips.get(i).getPrecioR());
            objeto.put("tipo",pChips.get(i).getTipo());
            objeto.put("precioK",pChips.get(i).getPrecioK());
            objeto.put("ruta",pChips.get(i).getRuta());
            objeto.put("detallista",pChips.get(i).getDetallista());
            objeto.put("costo",pChips.get(i).getCosto());
            jsonProductos.put(objeto);
        }

        if (pChips.size() > 0){
            guardarPendiente(jsonProductos.toString());
            Toast.makeText(this, "VENTA GUARDADA CORRECTAMENTE", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, VentaChipActivity.class));
            finish();
        } else {
            Toast.makeText(this, "NO HAY PRODUCTOS AGREGADOS A LA VENTA", Toast.LENGTH_SHORT).show();
        }
    }

    public void guardarPendiente(String jproductos){

        Calendar c = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String fecha = simpleDateFormat.format(c.getTime());
        simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        String hora = simpleDateFormat.format(c.getTime());

        String completa = fecha + " " + hora;

        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String codigo_dis = settings.getString("dis","-1");

        LocalDataBase localDataBase = new LocalDataBase(this);
        ContentValues contentValues = new ContentValues();
        SQLiteDatabase sqLiteDatabase = localDataBase.getWritableDatabase();

        Bundle extras = getIntent().getExtras();
        String cliente = extras.getString("cliente");
        String satcli = extras.getString("satcli");

        contentValues.put(Estructura.COLUMN_NAME_CLIENTE,cliente);
        contentValues.put(Estructura.COLUMN_NAME_SATCLI,satcli);
        contentValues.put(Estructura.COLUMN_NAME_CODIGODIS,codigo_dis);
        contentValues.put(Estructura.COLUMN_NAME_COORDENADAS,"coordenadas");
        contentValues.put(Estructura.COLUMN_NAME_FECHA,completa);
        contentValues.put(Estructura.COLUMN_NAME_PRODUCTOS,jproductos);

        sqLiteDatabase.insert(Estructura.TABLE_NAME2,null,contentValues);

        sqLiteDatabase.close();

    }

    public void getPromo(){
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");

        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VentasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    promodoble = Integer.parseInt(response.replace("null",""));
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(ChipsActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","getPromoDoble");
                    params.put("micd",dis);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void nueva(){
        nueva.putExtra("promo_doble","1");
        startActivity(nueva);
    }

    public void makePromo(final Intent resumen){
        View mView = getLayoutInflater().inflate(R.layout.alert_audifonos,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.btn_promo_ok);
        Button btnopc2 = (Button) mView.findViewById(R.id.btn_promo_cancelar);
        TextView textView = (TextView) mView.findViewById(R.id.new_promocion_promo);

        textView.setText("¿Desea entregar estos chips con promoción al doble?");

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                confirmaPromo(resumen,"ESTA VENTA DE CHIPS TIENE PROMOCIÓN AL DOBLE", true);
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acepta = 0;
                dialog.cancel();
                //startActivity(resumen);
                confirmaPromo(resumen,"ESTA VENTA DE CHIPS NO TIENE PROMOCIÓN AL DOBLE", false);
            }
        });

        dialog.show();
    }

    public void confirmaPromo(final Intent resumen, String mensaje, final boolean bandera){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setCancelable(false);
        builder.setTitle("¡¡ATENCIÓN!!");
        builder.setMessage(mensaje);

        builder.setPositiveButton("CONTINUAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                if (bandera){
                    nueva.putExtra("promo_doble","1");
                    startActivity(nueva);
                } else {
                    startActivity(resumen);
                }
            }
        });

        if (!bandera){
            builder.setNegativeButton("REINTENTAR", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
        }

        builder.show();

    }
}
