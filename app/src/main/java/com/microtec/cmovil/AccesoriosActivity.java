package com.microtec.cmovil;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.microtec.cmovil.Interactor.ProductosAdapter;
import com.microtec.cmovil.Model.Producto;
import com.microtec.cmovil.Utils.AppConstants;
import com.microtec.cmovil.Utils.ItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class AccesoriosActivity extends AppCompatActivity {

    private RecyclerView mList;
    private EditText buscaEquipo;

    private List<Integer> productosVenta;
    private List<Producto> pProductos;
    private List<Producto> backup;

    private RecyclerView.Adapter mAdapter;

    private ProductosAdapter filtro;

    private ProgressDialog progressDialog;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accesorios);
        initElements();
    }

    public void initElements(){

        mList = (RecyclerView) findViewById(R.id.rView_Productos);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando lista de productos...");

        progressDialog.show();

        productosVenta = new ArrayList<>();
        pProductos = new ArrayList<>();
        backup = new ArrayList<>();

        mAdapter = new ProductosAdapter(getApplicationContext(),pProductos);
        filtro = new ProductosAdapter(getApplicationContext(),pProductos);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(),linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(filtro);

        buscaEquipo = (EditText) findViewById(R.id.busca_equipos);

        buscaEquipo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

        mList.addOnItemTouchListener(new ItemClickListener(getApplicationContext(), new ItemClickListener.OnItemClickListener() {
            @Override
            public void OnItemClick(View view, int position) {
                selectorMenu(position);
            }
        }));

        listarProductos();

    }

    public void selectorMenu(final int position){
        final NumberPicker numberPicker = new NumberPicker(AccesoriosActivity.this);
        numberPicker.setMaxValue(Integer.parseInt(backup.get(position).getTipo()));
        numberPicker.setMinValue(1);
        AlertDialog.Builder builder = new AlertDialog.Builder(AccesoriosActivity.this);
        builder.setView(numberPicker);
        builder.setTitle("¿Cuántos deseas agregar?");
        builder.setMessage("Selecciona la cantidad de productos a vender:");

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                fijaPrecio(position, String.valueOf(numberPicker.getValue()));
            }
        });

        builder.show();
    }

    public void fijaPrecio(final int index, final String cantidad){

        final String precioR = backup.get(index).getPrecio();
        final String precio = backup.get(index).getPrecioR();

        View mView = getLayoutInflater().inflate(R.layout.alert_select_producto,null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        String nuevo = "Fija un precio entre $".concat(precio).concat(" y ").concat(precioR).concat("\n\n[ Precio asignado se verá reflejado en nota de venta. ]");

        Button btnopc1 = (Button) mView.findViewById(R.id.btn_nwProd_ok);
        Button btnopc2 = (Button) mView.findViewById(R.id.btn_nwProd_cancel);
        TextView textView = (TextView) mView.findViewById(R.id.nwprod_header);
        final EditText editText = (EditText) mView.findViewById(R.id.new_prod_cant);

        textView.setText(nuevo);
        editText.setText(precio);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Float.parseFloat(editText.getText().toString()) >= Float.parseFloat(precio) && Float.parseFloat(editText.getText().toString()) <= Float.parseFloat(precioR)) {
                    saveProductos(index, cantidad, editText.getText().toString());
                } else {
                    Toast.makeText(AccesoriosActivity.this, "¡PRECIO FUERA DE RANGO!", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    private void filter(String text){
        List<Producto> temp = new ArrayList();
        for (Producto producto : pProductos){
            if (producto.getMarca().toLowerCase().contains(text.toLowerCase()) || producto.getSerie().toLowerCase().equals(text.toLowerCase())){
                temp.add(producto);
            }
        }
        backup = temp;
        filtro.updateList(temp);
    }

    public void onClickTerminarVenta(View view) throws JSONException {

        Bundle extras = getIntent().getExtras();
        String satcli = extras.getString("satcli","ninguno");

        ArrayList<Producto> ventaFinal = new ArrayList<>();

        for (int i=0; i<productosVenta.size(); i++){
            ventaFinal.add(pProductos.get(productosVenta.get(i)));
        }

        JSONArray jsonProductos = new JSONArray();

        for (int i=0; i<ventaFinal.size(); i++){
            JSONObject objeto = new JSONObject();
            objeto.put("marca",ventaFinal.get(i).getMarca());
            objeto.put("modelo",ventaFinal.get(i).getModelo());
            objeto.put("serie",ventaFinal.get(i).getSerie());
            objeto.put("precio",ventaFinal.get(i).getPrecio());
            objeto.put("precioR",ventaFinal.get(i).getPrecioR());
            objeto.put("tipo",ventaFinal.get(i).getTipo());
            objeto.put("precioK",ventaFinal.get(i).getPrecioK());
            objeto.put("cantidad", ventaFinal.get(i).getCosto());
            jsonProductos.put(objeto);
        }

        if (jsonProductos.length()>0) {

            Log.d("JSON PROD", jsonProductos.toString());

            Intent resumen = new Intent(this, ResumenActivity.class);
            resumen.putExtra("json", jsonProductos.toString());
            resumen.putExtra("satcli", satcli);
            resumen.putExtra("tipoDeVenta", "soloacc");
            startActivity(resumen);

        } else {
            Toast.makeText(this, "Debe añadir productos para continuar...", Toast.LENGTH_SHORT).show();
        }

    }

    public void saveProductos(int index, String cantidad, String precio){

        String seriex = "";

        if (productosVenta.size()>0) {
            //Log.d("INDEX", String.valueOf(productosVenta.indexOf(index)));
            if (productosVenta.indexOf(index) != -1) {
                seriex = pProductos.get(productosVenta.indexOf(index)).getSerie();
            }
        }

        if (!productosVenta.contains(pProductos.indexOf(backup.get(index)))){
            backup.get(index).setPrecioR(precio);
            backup.get(index).setCosto(cantidad);
            mAdapter.notifyDataSetChanged();
            productosVenta.add(pProductos.indexOf(backup.get(index)));
        } else {
            Toast.makeText(this, "PRODUCTO YA AÑADIDO", Toast.LENGTH_SHORT).show();
        }
    }

    private void listarProductos() {

        SharedPreferences settings = this.getSharedPreferences(AppConstants.userPreferences,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");
        final String id_us = settings.getString("usuario","-1");

        if(!dis.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VentasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try{
                        JSONObject object = new JSONObject(response);
                        if(object.length() != 0){
                            generaListP(object);
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    filtro.notifyDataSetChanged();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(AccesoriosActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","getInventAcc");
                    params.put("codigo_dis",dis);
                    params.put("id_us",id_us);
                    params.put("op",String.valueOf(1));
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void generaListP(JSONObject jsonObject) throws JSONException {

        Iterator x = jsonObject.keys();
        JSONArray jsonArray = new JSONArray();

        String[] marca; String[] modelo; String[] serie;
        String[] precio; String[] precioR; String[] tipo;

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(jsonObject.get(key));
        }

        marca = (jsonArray.getString(0).replace("[","").replace("]","")).split(",");
        modelo = (jsonArray.getString(1).replace("[","").replace("]","")).split(",");
        serie = (jsonArray.getString(2).replace("[","").replace("]","")).split(",");
        //Precio SAT
        precio = (jsonArray.getString(3).replace("[","").replace("]","")).split(",");
        //Precio Ticket
        precioR = (jsonArray.getString(4).replace("[","").replace("]","")).split(",");
        tipo = (jsonArray.getString(5).replace("[","").replace("]","")).split(",");

        for (int i=0; i<marca.length; i++){
            Producto producto = new Producto();
            producto.setMarca(marca[i].replace("\"",""));
            producto.setModelo(modelo[i].replace("\"",""));
            producto.setSerie(serie[i].replace("\"",""));
            producto.setPrecio(precio[i].replace("\"",""));
            producto.setPrecioR(precioR[i].replace("\"",""));
            producto.setPrecioK(precioR[i].replace("\"",""));
            producto.setTipo(tipo[i].replace("\"",""));
            pProductos.add(producto);
        }

        progressDialog.dismiss();
        backup = pProductos;

    }

}
