package com.microtec.cmovil;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.microtec.cmovil.Interactor.NuevaVisitaAdapter;
import com.microtec.cmovil.Model.PuntoVenta;
import com.microtec.cmovil.Utils.AppConstants;
import com.microtec.cmovil.Utils.RecyclerTouchListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MiRutaActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleMap mMap;

    private LatLng coordenadasCamara;
    private Location mLastLocation;
    private Marker myPositionMarker;
    private LatLng pickupLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private RecyclerView mList;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private List<PuntoVenta> list;
    private double lat, lon;
    String idpv, idReg;

    private RelativeLayout mapalayout;
    private LinearLayout linearData;

    Location usuario, pventa;
    Context context;

    private TextView nota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mi_ruta);

        mapalayout = (RelativeLayout) findViewById(R.id.relative_mapa);
        linearData = (LinearLayout) findViewById(R.id.linearData);

        nota = (TextView) findViewById(R.id.textoNota);
        context = this;

        mList = (RecyclerView) findViewById(R.id.rView_DonePendiente);
        list = new ArrayList<>();
        mAdapter = new NuevaVisitaAdapter(getApplicationContext
                (), list);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(mAdapter);

        mList.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                mostrarMapa(position);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        getPendientes();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
        .findFragmentById(R.id.mapPrueba);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onBackPressed(){
        if (mapalayout.getVisibility() == View.VISIBLE){
            mapalayout.setVisibility(View.GONE);
            linearData.setVisibility(View.VISIBLE);
        } else {
            finish();
        }
    }

    public double getDistance(Location usuario, Location tienda){
        return usuario.distanceTo(tienda);
    }

    public void onClickRegistra(View v){
        Log.d("dist",""+getDistance(usuario,pventa));
        if (getDistance(usuario,pventa)<=15){
            registrarVisita(idpv, idReg);
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("UBICACIÓN")
                    .setMessage("Debe estar en un radio de 15 metros cerca del PV. Distancia actual: "+Math.floor(getDistance(usuario,pventa) * 100)/100+" metros.")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .setIcon(R.drawable.ic_gpslocation)
                    .show();
        }
    }

    public void getPendientes(){

        SharedPreferences settings = this.getSharedPreferences(AppConstants.userPreferences, this.MODE_PRIVATE);

        final String dis = settings.getString("dis", "-1");

        if (!dis.equals("-1")) {
            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VisitasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        String cad = response.substring(1, response.length());
                        JSONObject object = new JSONObject(cad);
                        generaList(object);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mAdapter.notifyDataSetChanged();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(MiRutaActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","listarPendientes");
                    params.put("agente",dis);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void generaList(JSONObject object) throws JSONException {
        Iterator x = object.keys();
        JSONArray jsonArray = new JSONArray();

        Log.d("tam", String.valueOf(jsonArray.length()));

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(object.get(key));
        }

        String codigo = jsonArray.getString(3).replace("[","").replace("]","");
        String nombre = jsonArray.getString(1).replace("[","").replace("]","");
        String idpv = jsonArray.getString(0).replace("[","").replace("]","");
        String coord = jsonArray.getString(2).replace("[","").replace("]","");
        String fecha = jsonArray.getString(4).replace("[","").replace("]","");
        String comment = jsonArray.getString(5).replace("[","").replace("]","");
        String idregistro = jsonArray.getString(6).replace("[","").replace("]","");

        String[] cod_part = codigo.split(",");
        String[] arr_nom = nombre.split(",");
        String[] arr_idpv = idpv.split(",");
        String[] arr_fecha = fecha.split(",");
        String[] arr_coord = coord.split(",");
        String[] arr_comment = comment.split(",");
        String[] arr_idreg = idregistro.split(",");

        for (int i=0; i<cod_part.length; i++){
            PuntoVenta puntoVenta = new PuntoVenta();
            puntoVenta.setNombre(arr_nom[i].replace("\"",""));
            puntoVenta.setCodigo(arr_fecha[i].replace("\"","")+" "+cod_part[i].replace("\"",""));
            puntoVenta.setId(arr_idpv[i].replace("\"",""));
            puntoVenta.setCoordendas(arr_coord[i].replace("\"",""));
            puntoVenta.setFolio(arr_comment[i].replace("\"",""));
            puntoVenta.setTipo(arr_idreg[i].replace("\"",""));
            list.add(puntoVenta);
        }

    }

    private void registrarVisita(final String idpv, final String idReg) {
        SharedPreferences settings = this.getSharedPreferences(AppConstants.userPreferences, this.MODE_PRIVATE);
        final String uid = settings.getString("id", "-1");

        if (!uid.equals("-1")) {

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VisitasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //Log.d("Response",response.toString());
                    if (response.contains("DONE")){
//                        Toast.makeText(MiRutaActivity.this, "VISITA REGISTRADA", Toast.LENGTH_SHORT).show();
                        new AlertDialog.Builder(context)
                                .setTitle("Visita")
                                .setMessage("Su visita se ha registrado exitozamente")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        finish();
                                    }
                                })
                                .setIcon(R.drawable.ic_gpslocation)
                                .show();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(MiRutaActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();

                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String formattedDate = simpleDateFormat.format(c.getTime());

                    params.put("method", "registrarVisita");
                    params.put("fecha", formattedDate);
                    params.put("latitud",String.valueOf(mLastLocation.getLatitude()));
                    params.put("longitud",String.valueOf(mLastLocation.getLongitude()));
                    params.put("descripcion","Se registro una visita");
                    params.put("user_id", uid);
                    params.put("pv",idpv);
                    params.put("pendiente","1");
                    params.put("idReg",idReg);
                    return params;
                }
            };
            try{
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        500000,
                        -1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
            } catch (Exception e){
                e.printStackTrace();
            }
            requestQueue.add(stringRequest);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        buildGoogleApiclient();
        boolean isPermiso = true;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            isPermiso = false;
        }
        if (isPermiso)
            mMap.setMyLocationEnabled(true);
    }

    private boolean isMyPositionSuccess = false;
    @Override
    public void onLocationChanged(Location location) {
        if (!isMyPositionSuccess) {
            mLastLocation = location;
            //drawMyPosition();
            isMyPositionSuccess =false;
            usuario = mLastLocation;
        }
    }

    public void mostrarMapa(int position){

        idpv = list.get(position).getCodigo();
        idReg = list.get(position).getTipo();

        mMap.clear();

        linearData.setVisibility(View.GONE);
        mapalayout.setVisibility(View.VISIBLE);

        String coords2 = list.get(position).getCoordendas().replace("|", ">");;
        String[] coords = coords2.split(">");

        double lat = Double.parseDouble(coords[0]);
        double lng = Double.parseDouble(coords[1]);

        nota.setText(list.get(position).getFolio());

        LatLng destino = new LatLng(lat,lng);

        pventa = new Location(String.valueOf(destino));
        pventa.setLatitude(lat);
        pventa.setLongitude(lng);

        MarkerOptions marcadorDestino= new MarkerOptions();
        marcadorDestino.position(destino);
        marcadorDestino.title(list.get(position).getNombre());
        mMap.addMarker(new MarkerOptions()
                .position(destino)
                .draggable(true)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))
                .title(list.get(position).getNombre())
        );
        mMap.moveCamera(CameraUpdateFactory.newLatLng(destino));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }
    @Override
    public void onConnectionSuspended(int i) { }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    protected synchronized void buildGoogleApiclient(){
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

}
