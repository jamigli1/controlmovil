package com.microtec.cmovil.Service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

public class ClickIntentService extends IntentService {

    public static final String ACTION_CLICK = "com.microtec.cmovil.widgets.click";

    public ClickIntentService(){
        super("ClickIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null){
            final String action = intent.getAction();
            if (ACTION_CLICK.equals(action)){
                handleClick();
            }
        }
    }

    private void handleClick(){

    }
}
