package com.microtec.cmovil.Service;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.common.primitives.Chars;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.microtec.cmovil.CMovilWidget;
import com.microtec.cmovil.Helper.LocationResultHelper;
import com.microtec.cmovil.Model.LocalDataBase;
import com.microtec.cmovil.Model.Vendedor;
import com.microtec.cmovil.Utils.AppConstants;
import com.microtec.cmovil.Utils.Estructura;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocationUpdatesBroadcastReceiver extends BroadcastReceiver {

    final private static String PRIMARY_CHANNEL = "com.microtec.controlmovil";

    private static final String TAG = "Segundo Plano";
    private static final String prefencesN = "userdata";
    boolean isGPS = false;

    int banderaRequest = 1;

    private FirebaseDatabase mDatabase;
    private DatabaseReference mReference;

    public float lat, lon, sp, bol, mtr;

    private Context context;

    public static final String ACTION_PROCESS_UPDATES =
            "com.google.android.gms.location.sample.backgroundlocationupdates.action" +
                    ".PROCESS_UPDATES";

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(final Context context, Intent intent) {

        SharedPreferences settings = context.getSharedPreferences(AppConstants.userDataBase, context.MODE_PRIVATE);
        SharedPreferences settings2 = context.getSharedPreferences(AppConstants.activityData, context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = settings.edit();

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    mDatabase = FirebaseDatabase.getInstance();
                    mReference = mDatabase.getReference().child("online_vRuta").child(AppConstants.micdis);
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    final String fecha = format.format(c.getTime());
                    mReference.child("ultconex").setValue(fecha);
                    Thread.sleep(1000*600);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        this.context = context;

        SharedPreferences prueba = context.getSharedPreferences(AppConstants.userPreferences, context.MODE_PRIVATE);

        lat = 0; bol = 0;
        lon = 0; sp = 0; mtr = 35;

        FirebaseApp.getInstance();

        banderaRequest = 1;

        Log.d("micdis",AppConstants.micdis);
        Log.d("micdisShared",settings.getString("dis","no hay"));
        Log.d("micdisNShared",prueba.getString("dis","no hay x2"));

        //getDataDB(context);

        AppConstants.micdis = prueba.getString("dis","nulo");

        mDatabase = FirebaseDatabase.getInstance();
        mReference = mDatabase.getReference().child("online_vRuta").child(AppConstants.micdis);

        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_PROCESS_UPDATES.equals(action)) {
                LocationResult result = LocationResult.extractResult(intent);
                if (result != null) {

                    List<Location> locations = result.getLocations();
                    final LocationResultHelper locationResultHelper = new LocationResultHelper(
                            context, locations);
                    // Save the location data to SharedPreferences.
                    locationResultHelper.saveResults();

                    int banEstado = settings2.getInt("jpausada",-1);

                    int notif = settings2.getInt("notificacion",-1);

                    SharedPreferences sesion = context.getSharedPreferences(AppConstants.userDataBase,context.MODE_PRIVATE);
                    lat = sesion.getFloat("latitud",0);
                    lon = sesion.getFloat("longitud",0);

                    sp = Float.parseFloat(LocationResultHelper.getSavedSpeed(context));
                    if (sp>30){
                        mtr = 60;
                    } else {
                        mtr = 40;
                    }

                    float nLat = Float.parseFloat(LocationResultHelper.getSavedLatitude(context));
                    float nLon = Float.parseFloat(LocationResultHelper.getSavedLongitude(context));
                    float distance = getDistance(lat,lon,nLat,nLon);
                    float saved2 = sesion.getFloat("latitud",-111);
                    float saved = settings.getFloat("recorrido",-111);
                    float recorrido = 0;

                    //Seccion notificacion
                    Calendar hora = Calendar.getInstance();
                    int currentHour = hora.get(Calendar.HOUR_OF_DAY);

                    if (currentHour>=21){
                        locationResultHelper.notifMessage("RECUERDA TERMINAR JORNADA");
                    }
                    //Termina

                    //Widget
                    SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
                    final String fecha = format.format(hora.getTime());
                    editor.putString("ltlng",fecha).apply();
                    AppWidgetManager widgetManager = AppWidgetManager.getInstance(context);
                    int [] widgetIds = widgetManager.getAppWidgetIds(new ComponentName(context, CMovilWidget.class));
                    for (int ids : widgetIds){
                        try {
                            CMovilWidget.updateAppWidget(context, widgetManager, ids);
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                    //Termina widget

                    if (saved!=-111) {
                        recorrido = distance + saved;
                        editor.putFloat("recorrido", recorrido);
                        editor.apply();
                    } else if (saved2!=-111){
                        editor.putFloat("recorrido",distance);
                        editor.apply();
                    }

                    bol = settings.getFloat("firsttime",0);

                    if (bol == 0){
                        editor.putFloat("firsttime",1);
                        editor.apply();
                        guardarCoordenadas(context,LocationResultHelper.getSavedLatitude(context),LocationResultHelper.getSavedLongitude(context),"no");
                    }

                    if (recorrido >= 20 && banEstado==-1){
                        editor.putInt("banEstacionado",-1);
                        editor.putFloat("recorrido",0);
                        editor.apply();
                        guardarCoordenadas(context,LocationResultHelper.getSavedLatitude(context),LocationResultHelper.getSavedLongitude(context), "no");
                    } else {
                        //Log.d("No almacenado", "DISTANCIA MENOR A 30 - distancia "+distance+" jpausada = "+banEstado);
                        try {
                            checkEstacionado(context, LocationResultHelper.getSavedLatitude(context), LocationResultHelper.getSavedLongitude(context));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

                    if (notif==1){
                        try {
                            if(notifService(context)){
                                locationResultHelper.showNotification();
                            }
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    try {
                        finishJornada(context);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    mReference.child("bandera").child("sendData").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {

                            if (dataSnapshot.exists()) {
                                if (dataSnapshot.toString().contains("enviar")) {
                                    try {
                                        mReference.child("bandera").child("sendData").setValue("nulo");
                                        sendPosiocionesRequest(context);
                                        Thread.sleep(1000*4);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                } else if (dataSnapshot.toString().contains("realLoc")) {
                                    sendRealLoc(mReference, context);
                                } else if (dataSnapshot.toString().contains("lista")) {
                                    //Log.d("Mensaje",dataSnapshot.toString());
                                    locationResultHelper.notifMessage("¡Has recibido una lista de visitas pendientes!");
                                    mReference.child("bandera").child("sendData").setValue("nulo");
                                } else if (dataSnapshot.toString().contains("version")) {
                                    locationResultHelper.notifMessage("Nueva versión disponible. Ingresa a la Play Store para actualizar.");
                                    mReference.child("bandera").child("sendData").setValue("nulo");
                                } else if (!dataSnapshot.toString().contains("nulo") && !dataSnapshot.toString().contains("eliminar")) {
                                    String mensaje = dataSnapshot.toString().replace("{", "").replace("}", "").replace("sendData", "");
                                    mensaje = mensaje.replace("key", "").replace("value", "").replace("=", "").replace("DataSnapshot", "").replace(",", "");
                                    mReference.child("bandera").child("sendData").setValue("nulo");
                                    locationResultHelper.notifMessage(mensaje.toUpperCase());
                                } else if (dataSnapshot.toString().contains("eliminar")) {
                                    borraDB();
                                    mReference.child("bandera").child("sendData").setValue("nulo");
                                }
                            } else {
                                mReference.removeEventListener(this);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                    editor.putFloat("latitud",Float.parseFloat(LocationResultHelper.getSavedLatitude(context)));
                    editor.putFloat("longitud",Float.parseFloat(LocationResultHelper.getSavedLongitude(context)));
                    editor.apply();
                }
            }
        }
    }

    public boolean notifService(Context context) throws ParseException {
        SharedPreferences settings = context.getSharedPreferences(AppConstants.activityData,context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        Calendar c = Calendar.getInstance();
        String inicial = settings.getString("notificacionStart","none");
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        String hora = format.format(c.getTime());

        Date dAnt = format.parse(inicial);
        Date nueva = format.parse(hora);

        long diferencia = (nueva.getTime() - dAnt.getTime()) / 1000;
        editor.putLong("notifDif",diferencia);
        editor.apply();

        int first = settings.getInt("compara",1);

        long tiempo = settings.getLong("notifDif",0);

        /*Log.d("tiempo", String.valueOf(tiempo));
        Log.d("first", String.valueOf(first));*/

        if (tiempo >= 900 && first==1){
            //Log.d("ENTRA","notificacion");
            editor.putInt("notifBandera",1);
            editor.putLong("notifDif",0);
            editor.putInt("compara",-1);
            //editor.putInt("notificacion",-1);
            editor.apply();
            return true;
        } else if (tiempo >= 3600){
            //Log.d("FORCE REANUDAR","se reinicia");
            editor.putInt("jpausada",-1);
            editor.putInt("notificacion",-1);
            editor.apply();
        }

        return false;

    }

    public void sendRealLoc(DatabaseReference mReference, Context context){

        Calendar c = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String hora = format.format(c.getTime());

        mReference.child("latitud").setValue(LocationResultHelper.getSavedLatitude(context));
        mReference.child("longitud").setValue(LocationResultHelper.getSavedLongitude(context));
        mReference.child("hora").setValue(hora);

        mReference.child("bandera").child("sendData").setValue("nulo");

    }

    public void checkEstacionado(Context context, String lat, String longi) throws ParseException {

        SharedPreferences settings = context.getSharedPreferences(AppConstants.userDataBase,context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        int bandera = settings.getInt("banEstacionado",-1);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        String hora = simpleDateFormat.format(c.getTime());

        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

        //Entra por primera vez
        if (bandera == -1){
            editor.putInt("banEstacionado",1);
            editor.putString("horaInicial",hora);
            editor.apply();
        } else {
            //Entra cada vez que registra ubicación en el mismo punto (no movimiento)
            bandera += 1;
            editor.putInt("banEstacionado",bandera);
            String inicial = settings.getString("horaInicial",hora);
            Date dateant = format.parse(inicial);
            Date nueva = format.parse(hora);
            long  diferencia = (nueva.getTime() - dateant.getTime())/1000;
            Log.d("DIFERENCIA",String.valueOf(diferencia));
            editor.putLong("tmEstacionado",diferencia);
            editor.apply();
        }

        long tiempo = settings.getLong("tmEstacionado",0);
        if (tiempo >= 5000){
            //Verifica si el tiempo es mayor a 60 minutos y guarda la posición como estacionado
            Log.d("ENTRANDO...","entró "+tiempo);
            //guardarCoordenadas(context, lat, longi, "si");
            editor.putLong("tmEstacionado",0);
            editor.putInt("banEstacionado",-1);
            editor.apply();
        }


    }

    public float getDistance(float lat1, float lon1, float lat2, float lon2){

        float earthRadius = 6371;

        double dLat = Math.toRadians(lat2-lat1);
        double dLon = Math.toRadians(lon2-lon1);

        double sindLat = Math.sin(dLat);
        double sindLon = Math.sin(dLon);

        double a = Math.pow(sindLat, 2) + Math.pow(sindLon, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        double dist = earthRadius * c;

        return (float) (dist/0.0010000);
    }

    //Inicia BD

    public void guardarCoordenadas(Context context, String latitud, String longitud, String estacionado){
        LocalDataBase localDataBase;
        localDataBase = new LocalDataBase(context);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String fecha = simpleDateFormat.format(c.getTime());
        simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        String hora = simpleDateFormat.format(c.getTime());

        SQLiteDatabase sqLiteDatabase = localDataBase.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        if (!latitud.equals("") || !longitud.equals("")){
            contentValues.put(Estructura.COLUMN_NAME_LATITUD,latitud);
            contentValues.put(Estructura.COLUMN_NAME_LONGITUD,longitud);
            contentValues.put(Estructura.COLUMN_NAME_FECHA,fecha);
            contentValues.put(Estructura.COLUMN_NAME_HORA,hora);
            contentValues.put(Estructura.COLUMN_NAME_ESTACIONADO,estacionado);
            sqLiteDatabase.insert(Estructura.TABLE_NAME,null,contentValues);
            Log.i("Almacenado","Coordenadas almacenadas");
        }

        sqLiteDatabase.close();
    }

    public void finishJornada(Context context) throws JSONException, IOException {
        Calendar hora = Calendar.getInstance();
        int currentHour = hora.get(Calendar.HOUR_OF_DAY);

        if (currentHour>=21){
            sendPosiocionesRequest(context);
        }

    }

    public void sendPosiocionesRequest(Context context) throws JSONException, IOException {

        LocalDataBase localDataBase = new LocalDataBase(context);
        SQLiteDatabase sqLiteDatabase = localDataBase.getReadableDatabase();

        Log.d("Aqui", "send posi");

        JSONArray jsonArray = new JSONArray();

        Cursor response = sqLiteDatabase.rawQuery("select * from "+Estructura.TABLE_NAME, null);

        ArrayList<String> datos = new ArrayList<String>();

        Log.d("BDDATS", String.valueOf(response.getCount()));

        if (response.getCount() > 1){
            Log.d("Datos","Si hay");
            while (response.moveToNext()) {
                JSONObject object = new JSONObject();
                object.put("latitud", response.getString(1));
                object.put("longitud", response.getString(2));
                object.put("fecha", response.getString(3));
                object.put("hora", response.getString(4));

                datos.add(response.getString(1) + "," + response.getString(2) + "," + response.getString(3) + "," + response.getString(4));

                jsonArray.put(object);
            }

            //TODO des/comentar esto
            //getFile(datos);
            requestPosi(jsonArray.toString(), context);
        }
        sqLiteDatabase.close();
    }

    public void requestPosi(final String posiciones, final Context context){
        SharedPreferences settings = context.getSharedPreferences(prefencesN,context.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");
        final String id_us = settings.getString("usuario","-1");

        Log.d("Aqui","request posi");

        String micdis = AppConstants.micdis;

        if (micdis == "nulo"){
            micdis = getDataDB(context);
        }

        if(micdis!="nulo"){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/PosicionesA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(context);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("RESPUESTA",response);
                    if (response.toLowerCase().contains("done")){
                        LocalDataBase localDataBase;
                        localDataBase = new LocalDataBase(context);
                        SQLiteDatabase sqLiteDatabase = localDataBase.getWritableDatabase();
                        sqLiteDatabase.execSQL("delete from "+ Estructura.TABLE_NAME);
                        sqLiteDatabase.close();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","crearPunto");
                    params.put("codigo_dis",dis);
                    params.put("usuario",id_us);
                    params.put("status_gps","");
                    params.put("tipo","0");
                    params.put("jsonC",posiciones);
                    return params;
                }
            };
            try{
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        80 * 1000,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
                requestQueue.add(stringRequest);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public String getDataDB(Context context) {

        LocalDataBase localDataBase;
        localDataBase = new LocalDataBase(context);

        SQLiteDatabase sqLiteDatabase = localDataBase.getWritableDatabase();

        Cursor response = sqLiteDatabase.rawQuery("select codigodis from " + Estructura.TABLE_NAME2 + " where satcli = '-128'", null);

        if (response.getCount() != 0) {

            while (response.moveToNext()) {
                Log.d("micdisBD",response.getString(0));
                return response.getString(0);
            }
        } else {
            Log.d("No encontró datos","fdss");
        }

        return "nulo";

    }

    public void getFile(final ArrayList<String> datos) throws IOException {
        final File localFile = File.createTempFile("Posiciones","txt");

        String semana = new SimpleDateFormat("w").format(new java.util.Date());

        StorageReference storageReference = FirebaseStorage.getInstance().getReference();

        storageReference.child(AppConstants.micdis+"/posiciones/Posiciones_S"+semana+".txt").getFile(localFile)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        try {
                            sendFile(localFile,datos);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        try {
                            sendFile(localFile,datos);
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                });

    }

    public void sendFile(File posiciones, ArrayList<String> datos) throws IOException, JSONException {

        String mCurrentPath = posiciones.getAbsolutePath();
        String semana = new SimpleDateFormat("w").format(new java.util.Date());

        FileWriter write = new FileWriter(posiciones, true);

        for (int i=0; i<datos.size(); i++){
            write.append(datos.get(i));
            write.append("\n");
        }

        write.flush();
        write.close();

        Log.d("PATH",mCurrentPath);

        StorageReference mStorage = FirebaseStorage.getInstance().getReference();
        Uri file = Uri.fromFile(new File(mCurrentPath));
        mStorage = mStorage.child(AppConstants.micdis+"/posiciones/Posiciones_S"+semana+".txt");

        mStorage.putFile(file)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Log.d("FBASE","SE LOGRO");
                        Uri download = taskSnapshot.getUploadSessionUri();
                        Log.d("algo", download.toString());
                        borraDB();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("FBASE","NO SE LOGRO ALV");
                    }
                });

    }

    public void borraDB(){
        LocalDataBase localDataBase;
        localDataBase = new LocalDataBase(context);
        SQLiteDatabase sqLiteDatabase = localDataBase.getWritableDatabase();
        sqLiteDatabase.execSQL("delete from "+ Estructura.TABLE_NAME);
        sqLiteDatabase.close();
    }

}
