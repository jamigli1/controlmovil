package com.microtec.cmovil;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.microtec.cmovil.Utils.AppConstants;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class PromotorActivity extends AppCompatActivity {

    private EditText nombre, appaterno, apmaterno, celular, telc, rfc;
    private EditText email, ciudad, municipio, colonia, cp, calle, numint, numext;
    private Spinner estado;
    private CheckBox precioC;
    private Button nw_cliente_send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotor);

        initElementos();

    }

    public boolean nonEmpty(EditText editText){
        if (!TextUtils.isEmpty(editText.getText())){
            return true;
        } else {
            editText.setError("Error: Este campo es requerido.");
            return false;
        }
    }

    public void onClickEnviarRegistro(View view){

        boolean estados = false;

        nonEmpty(nombre); nonEmpty(appaterno); nonEmpty(apmaterno);
        nonEmpty(celular); nonEmpty(email); nonEmpty(ciudad);
        nonEmpty(telc); nonEmpty(rfc); nonEmpty(municipio);
        nonEmpty(colonia); nonEmpty(cp); nonEmpty(calle); nonEmpty(numext);

        if (estado.getSelectedItem().toString().contains("Seleccione")){
            estados = false;
            Toast.makeText(this, "El campo ESTADO es requerido.", Toast.LENGTH_SHORT).show();

        } else {
            estados = true;
        }

        int select = 0;

        if (precioC.isChecked()){
            select = 1;
        }

        if (nonEmpty(colonia) && nonEmpty(municipio) && nonEmpty(rfc) && nonEmpty(telc) && nonEmpty(nombre) && nonEmpty(appaterno) && nonEmpty(apmaterno) && nonEmpty(celular)
                && nonEmpty(email) && nonEmpty(numext) && nonEmpty(calle) && nonEmpty(cp) && nonEmpty(ciudad) && estados){

            nw_cliente_send.setEnabled(false);
            sendNClienteRequest(select);

        } else {
            Toast.makeText(this, "LLENE LOS CAMPOS REQUERIDOS", Toast.LENGTH_SHORT).show();
        }

    }

    public void initElementos(){

        nombre = (EditText) findViewById(R.id.promotor_nombre);
        appaterno = (EditText) findViewById(R.id.promotor_ap1);
        apmaterno = (EditText) findViewById(R.id.promotor_ap2);
        celular = (EditText) findViewById(R.id.promotor_celu);
        email = (EditText) findViewById(R.id.promotor_email);
        ciudad = (EditText) findViewById(R.id.promotor_ciudad);
        estado = (Spinner) findViewById(R.id.promotor_estados);
        telc = (EditText) findViewById(R.id.promotor_telcasa);
        rfc = (EditText) findViewById(R.id.promotor_rfc);
        municipio = (EditText) findViewById(R.id.promotor_municipio);
        colonia = (EditText) findViewById(R.id.promotor_colonia);
        cp = (EditText) findViewById(R.id.promotor_cp);
        calle = (EditText) findViewById(R.id.promotor_calle);
        numint = (EditText) findViewById(R.id.promotor_nint);
        numext = (EditText) findViewById(R.id.promotor_next);
        precioC = (CheckBox) findViewById(R.id.checkbox_precioc);

        nw_cliente_send = (Button) findViewById(R.id.promotor_registro);

        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this,R.array.estados_array, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        estado.setAdapter(arrayAdapter);
    }

    public void sendNClienteRequest(final int seleccionado){
        SharedPreferences settings = this.getSharedPreferences(AppConstants.userPreferences,this.MODE_PRIVATE);
        final String agente = settings.getString("agente","-1");

        if(!agente.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/PVentas.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("RESPUESTA",response);
                    if (response.contains("DONE")){
                        showResponse("TIENDA SMART CREADA "+response.replace("DONE",""));
                    } else {
                        showResponse("HUBO UN ERROR AL REGISTRAR LA TIENDA, REINTENTE.");
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            nw_cliente_send.setEnabled(true);
                            Toast.makeText(PromotorActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","crearSmart");
                    params.put("name_vRuta",agente);
                    params.put("nombreC",nombre.getText().toString()+" "+appaterno.getText().toString()+" "+apmaterno.getText().toString());
                    params.put("telefono",celular.getText().toString());
                    params.put("email",email.getText().toString());
                    params.put("ciudad",ciudad.getText().toString());
                    params.put("estado",estado.getSelectedItem().toString());

                    params.put("telcasa",telc.getText().toString());
                    params.put("rfc",rfc.getText().toString());
                    params.put("municipio",municipio.getText().toString());
                    params.put("colonia",colonia.getText().toString());
                    params.put("cp",cp.getText().toString());
                    params.put("calle",calle.getText().toString());
                    params.put("exterior",numext.getText().toString());
                    params.put("interior",numint.getText().toString());
                    params.put("costo", String.valueOf(seleccionado));
                    return params;
                }
            };
            try{
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        500000,
                        -1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
            } catch (Exception e){
                e.printStackTrace();
            }
            requestQueue.add(stringRequest);
        }
    }

    public void showResponse(String response){
        View mView = getLayoutInflater().inflate(R.layout.alert_response,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.al_response_ok);
        TextView textView = (TextView) mView.findViewById(R.id.al_response);

        textView.setText(response);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                nw_cliente_send.setVisibility(View.GONE);
            }
        });

        dialog.show();
    }

}
