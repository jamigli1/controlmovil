package com.microtec.cmovil;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MenuGarantiasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_garantias);
    }

    public void onClickCards(View view){
        switch (view.getId()){
            case R.id.principal_opc1:
                startActivity(new Intent(this, CapturaGarantiasActivity.class));
                break;
            case R.id.principal_opc2:
                startActivity(new Intent(this, ListarGarantiasActivity.class));
                break;
            case R.id.principal_opc3:
                startActivity(new Intent(this,TransferenciasActivity.class));
                break;
        }
    }
}
