package com.microtec.cmovil;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.microtec.cmovil.Interactor.GarantiaAdapter;
import com.microtec.cmovil.Interactor.TransferenciaAdapter;
import com.microtec.cmovil.Interactor.TransferenciaRAdapter;
import com.microtec.cmovil.Model.Garantia;
import com.microtec.cmovil.Model.Producto;
import com.microtec.cmovil.Model.Transferencia;
import com.microtec.cmovil.Utils.AppConstants;
import com.microtec.cmovil.Utils.ItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TransferenciasActivity extends AppCompatActivity {

    private Spinner tipoTransf;
    private TextView textoTransf;

    private RecyclerView mList;
    private RecyclerView mListCompl;

    private LinearLayoutManager linearLayoutManager, linearLayoutManager2;
    private RecyclerView.Adapter mAdapter;
    private List<Transferencia> list;
    private List<Transferencia> backup;

    private String tipolista = "";
    private int tipo;

    private TransferenciaRAdapter filtro1;
    private TransferenciaAdapter filtro2;
    private TransferenciaRAdapter listComp;

    private View transf1, transf2;

    private ProgressDialog progressDialog;

    private TextView gpoDestino;
    private EditText buscaTrf;

    private List<String> bodegas;

    private List<Transferencia> arrTransf;
    private List<Transferencia> arrNoTransf;

    private ItemClickListener itemClickListener;
    private FloatingActionButton fab_compl;

    private String foliosStr;

    private View mView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transferencias);
        mList = (RecyclerView) findViewById(R.id.rView_ListaTransf);
        initElementos();

    }

    public void initAdapters(final int tipo1) {
        list = new ArrayList<>();

        gpoDestino = findViewById(R.id.grupoDestino1);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);

        if (tipo1 == 1) {
            filtro2 = new TransferenciaAdapter(getApplicationContext(), list);
            mAdapter = new TransferenciaAdapter(getApplicationContext(), list);
            mList.setAdapter(filtro2);
        } else {
            filtro1 = new TransferenciaRAdapter(getApplicationContext(), list);
            mAdapter = new TransferenciaRAdapter(getApplicationContext(), list);
            mList.setAdapter(filtro1);
        }

        mList.removeOnItemTouchListener(itemClickListener);
        mList.addOnItemTouchListener(itemClickListener);

    }

    public void noSeleccionado(){
        for (int i=0; i<list.size(); i++){
            if (!arrTransf.contains(list.get(i)) && !arrNoTransf.contains(list.get(i))){
                arrNoTransf.add(list.get(i));
            }
        }
        Log.d("tamaño del nuevo", String.valueOf(arrNoTransf.size()));
    }

    public void initElementos(){

        bodegas = new ArrayList<>();

        itemClickListener = new ItemClickListener(getApplicationContext(), new ItemClickListener.OnItemClickListener() {
            @Override
            public void OnItemClick(View view, final int position) {
                if (tipo != 0){
                    Intent detalle = new Intent(TransferenciasActivity.this, DetalleTransfActivity.class);
                    detalle.putExtra("list", tipolista);
                    detalle.putExtra("folio", backup.get(position).getFolio());
                    detalle.putExtra("tipo", tipo);
                    startActivity(detalle);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(TransferenciasActivity.this);
                    builder.setTitle("Transferir");
                    builder.setMessage("¿Desea agregar a la lista de garantías por transferir?");
                    builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            llenaLista(backup.get(position));
                            listComp.notifyDataSetChanged();
                        }
                    });
                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();
                }
            }
        });

        buscaTrf = findViewById(R.id.busquedaTrf);

        tipo = 0;

        backup = new ArrayList<>();

        transf1 = findViewById(R.id.transf1);
        transf2 = findViewById(R.id.transf2);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando datos...");

        arrTransf = new ArrayList<>();
        arrNoTransf = new ArrayList<>();

        tipoTransf = findViewById(R.id.spinner_tiportransf);
        textoTransf = findViewById(R.id.tipo_transferenciatv);

        final ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this, R.array.transf_array, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tipoTransf.setAdapter(arrayAdapter);

        tipoTransf.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    if (i != 0) {
                        transf2.setVisibility(View.VISIBLE);
                        transf1.setVisibility(View.GONE);
                        initAdapters(1);
                    } else {
                        transf1.setVisibility(View.VISIBLE);
                        transf2.setVisibility(View.GONE);
                        initAdapters(0);
                    }
                } catch (Exception e){
                    Log.d("ERROR DE VIDA", e.getMessage());
                }

                switch (i){
                    case 0:
                        fab_compl.show();
                        tipo = 0;
                        progressDialog.show();
                        textoTransf.setText("GARANTIAS - REALIZAR TRANSFERENCIA");
                        getListado("transf_disp");
                        break;
                    case 1:
                        arrTransf.clear();
                        fab_compl.hide();
                        tipo = 1;
                        progressDialog.show();
                        textoTransf.setText("GARANTIAS - TRANSFERENCIAS ENVIADAS");
                        gpoDestino.setText("ORIGEN");
                        getListado("transf_env");
                        tipolista = "transf_env_desc";
                        break;
                    case 2:
                        arrTransf.clear();
                        fab_compl.hide();
                        tipo = 2;
                        progressDialog.show();
                        gpoDestino.setText("DESTINO");
                        textoTransf.setText("GARANTIAS - TRANSFERENCIAS ACEPTADAS");
                        getListado("transf_rec");
                        tipolista = "transf_env_desc";
                        break;
                    case 3:
                        arrTransf.clear();
                        fab_compl.hide();
                        tipo = 3;
                        progressDialog.show();
                        gpoDestino.setText("DESTINO");
                        textoTransf.setText("GARANTIAS - TRANSFERENCIAS PENDIENTES DE ACEPTAR");
                        getListado("transf_pend");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        buscaTrf.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    filter(editable.toString());
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        fab_compl = findViewById(R.id.fab_listcomp);
        fab_compl.setEnabled(false);
        fab_compl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                consultaDisname();
                muestraLista(0);
            }
        });

        linearLayoutManager2 = new LinearLayoutManager(this);
        linearLayoutManager2.setOrientation(LinearLayoutManager.VERTICAL);

        mView = getLayoutInflater().inflate(R.layout.content_listtransf,null);
        mListCompl = (RecyclerView) mView.findViewById(R.id.rView_ListaTransfListas);
        mListCompl.setHasFixedSize(true);
        mListCompl.setLayoutManager(linearLayoutManager2);
        listComp = new TransferenciaRAdapter(getApplication(), arrTransf);
//        mListCompl.setAdapter(listComp);

    }

    public void consultaDisname(){

        final String dis = arrTransf.get(0).getOrigen();

        if (!dis.equals("-1")) {
            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/GarantiasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("RespuestaXDD",response);
                    bodegas.add(response);
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(TransferenciasActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","getDisName");
                    params.put("dis",dis);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void muestraLista(final int tipoMenu){

        Log.d("Folios", foliosStr);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (mView.getParent()!=null){
            ((ViewGroup) mView.getParent()).removeView(mView);
        }
        builder.setView(mView);
        builder.setCancelable(false);

        final Dialog dialog;
        dialog = builder.create();

        TextView header = mView.findViewById(R.id.mensaje_series);
        Button btnopc1 = mView.findViewById(R.id.make_transf);
        Button btnopc2 = mView.findViewById(R.id.reinicia_transf);
        ImageView btnCanc = mView.findViewById(R.id.img_cerrarlay);

        if (tipoMenu == 1) {
            noSeleccionado();
            listComp = new TransferenciaRAdapter(this,arrNoTransf);
            listComp.notifyDataSetChanged();
            mListCompl.setAdapter(listComp);
            header.setText("SERIES QUE NO SE VAN A TRANSFERIR");
            btnopc2.setVisibility(View.GONE);
        } else {
            header.setText("SERIES QUE SE VAN A TRANSFERIR");
            listComp = new TransferenciaRAdapter(getApplication(), arrTransf);
            mListCompl.setAdapter(listComp);
            listComp.notifyDataSetChanged();
        }

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tipoMenu == 0){
                    muestraLista(1);
                    dialog.dismiss();
                } else {
                    dialog.dismiss();
                    mObserv();
                }
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(TransferenciasActivity.this, TransferenciasActivity.class));
                dialog.dismiss();
            }
        });

        btnCanc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void llenaLista(final Transferencia transferencia){
        if (!arrTransf.contains(transferencia)){
            if (arrTransf.size() > 0){
                if (arrTransf.get(0).getOrigen().equals(transferencia.getOrigen())){
                    foliosStr = foliosStr+","+transferencia.getFolio();
                    arrTransf.add(transferencia);
                    backup.get(backup.indexOf(transferencia)).setFolio(transferencia.getFolio()+"\nXXXX");
                    filtro1.notifyDataSetChanged();
                } else {
                    Toast.makeText(this, "ESTE ELEMENTO NO PERTENECE AL MISMO ORIGEN", Toast.LENGTH_SHORT).show();
                }
            } else {
                foliosStr = transferencia.getFolio();
                fab_compl.setEnabled(true);
                fab_compl.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.fab_color)));
                arrTransf.add(transferencia);
                backup.get(backup.indexOf(transferencia)).setFolio(transferencia.getFolio()+"\nXXXX");
                filtro1.notifyDataSetChanged();
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(TransferenciasActivity.this);
            builder.setTitle("Transferir");
            builder.setMessage("ESTA GARANTIA YA SE ENCUENTRE DENTRO DE LA LISTA DE GARANTIAS POR TRANSFERIR, ¿ELIMINAR?");
            builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    foliosStr = foliosStr.replace(transferencia.getFolio().replace("\nXXXX",""), "0");
                    Toast.makeText(TransferenciasActivity.this, "GARANTÍA ELIMINADA DE LA LISTA", Toast.LENGTH_SHORT).show();
                    arrTransf.remove(transferencia);
                    backup.get(backup.indexOf(transferencia)).setFolio(transferencia.getFolio().replace("\nXXXX", ""));
                    filtro1.notifyDataSetChanged();
                    dialogInterface.dismiss();

                    if (arrTransf.size() == 0){
                        fab_compl.setEnabled(false);
                        fab_compl.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                    }

                }
            });
            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.setCancelable(false);
            builder.show();

        }
    }

    private void filter(String text){
        List<Transferencia> temp = new ArrayList();
        for (Transferencia transferencia : list){
            if (transferencia.getFolio().toLowerCase().contains(text.toLowerCase())){
                temp.add(transferencia);
            }
        }
        backup = temp;
        if (transf1.getVisibility() == View.VISIBLE){
            filtro1.updateList(temp);
        } else {
            filtro2.updateList(temp);
        }
    }

    public void getListado(final String tipoLista){

        list.clear();
        backup.clear();
        mAdapter.notifyDataSetChanged();

        SharedPreferences settings = this.getSharedPreferences(AppConstants.userPreferences, this.MODE_PRIVATE);

        final String dis = settings.getString("dis", "-1");

        if (!dis.equals("-1")) {
            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/GarantiasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("Respuesta",response);
                    try {
                        String cad = response.substring(1, response.length());
                        JSONObject object = new JSONObject(cad);
                        if (!tipoLista.equals("transf_disp")) {
                            generaListNormal(object);
                        } else {
                            generaListPrincipal(object);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try{
                        filtro1.notifyDataSetChanged();
                        filtro2.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(TransferenciasActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","getListTransf");
                    params.put("dis",dis);
                    params.put("list",tipoLista);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void mObserv(){
        View mView = getLayoutInflater().inflate(R.layout.alert_image,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(mView);
        builder.setCancelable(false);

        final Dialog dialog;
        dialog = builder.create();

        TextView header = mView.findViewById(R.id.header);
        TextView header2 = mView.findViewById(R.id.header2);
        final Spinner spinner = mView.findViewById(R.id.spinnerBodega);
//        header.setTextSize(18);
        Button btnopc1 = mView.findViewById(R.id.btn_img_ok);
        Button btnopc2 = mView.findViewById(R.id.btn_img_cancel);
        final EditText editText = mView.findViewById(R.id.modif_observ);
        spinner.setVisibility(View.VISIBLE);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, bodegas);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);

        Log.d("sizeeee", String.valueOf(bodegas.size()));

        if (bodegas.size()<=1) {
            spinner.setEnabled(false);
            btnopc1.setEnabled(false);
            btnopc1.setTextColor(getResources().getColor(R.color.whiteColor));
            btnopc1.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.grayColor2)));
        }

        editText.setHint("Motivos...");

        header.setText("Datos Adicionales");
        header2.setText("Motivo por el cual deja equipos sin transferir:");

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(editText.getText())){
                    sendTransferRequest(spinner.getSelectedItem().toString());
                } else {
                    editText.setError("CAMPO REQUERIDO");
                }
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void generaListNormal(JSONObject object) throws JSONException {

        Iterator x = object.keys();
        JSONArray jsonArray = new JSONArray();

        while (x.hasNext()) {
            String key = (String) x.next();
            jsonArray.put(object.get(key));
        }

        String folio = jsonArray.getString(0).replace("[", "").replace("]", "");
        String fecha = jsonArray.getString(1).replace("[", "").replace("]", "");
        String destino = jsonArray.getString(2).replace("[", "").replace("]", "");
        String des_name = jsonArray.getString(3).replace("[", "").replace("]", "");
        String n_equipos = jsonArray.getString(4).replace("[", "").replace("]", "");
        String dias = jsonArray.getString(5).replace("[", "").replace("]", "");

        String[] a_folio = folio.split(",");
        String[] a_fecha = fecha.split(",");
        String[] a_destino = destino.split(",");
        String[] a_desname = des_name.split(",");
        String[] a_nequipos = n_equipos.split(",");
        String[] a_dias = dias.split(",");

        for (int i = 0; i < a_folio.length; i++) {
            Transferencia transferencia = new Transferencia();
            transferencia.setFolio(a_folio[i].replace("\"",""));
            transferencia.setFecha(a_fecha[i].replace("\"",""));
            transferencia.setDestino(a_destino[i].replace("\"",""));
            transferencia.setDest_nombre(a_desname[i].replace("\"",""));
            transferencia.setN_equipos(a_nequipos[i].replace("\"",""));
            transferencia.setDias(a_dias[i].replace("\"",""));
            list.add(transferencia);
        }

        backup = list;
        progressDialog.dismiss();

    }

    public void generaListPrincipal(JSONObject object) throws JSONException {

        Iterator x = object.keys();
        JSONArray jsonArray = new JSONArray();

        while (x.hasNext()) {
            String key = (String) x.next();
            jsonArray.put(object.get(key));
        }

        String folio = jsonArray.getString(0).replace("[", "").replace("]", "");
        String fecha = jsonArray.getString(1).replace("[", "").replace("]", "");
        String marca = jsonArray.getString(2).replace("[", "").replace("]", "");
        String modelo = jsonArray.getString(3).replace("[", "").replace("]", "");
        String imei = jsonArray.getString(4).replace("[", "").replace("]", "");
        String origen = jsonArray.getString(5).replace("[", "").replace("]", "");

        String[] a_folio = folio.split(",");
        String[] a_fecha = fecha.split(",");
        String[] a_marca = marca.split(",");
        String[] a_modelo = modelo.split(",");
        String[] a_imei = imei.split(",");
        String[] a_origen = origen.split(",");

        for (int i = 0; i < a_folio.length-1; i++) {
            Transferencia transferencia = new Transferencia();
            transferencia.setFolio(a_folio[i].replace("\"",""));
            transferencia.setFecha(a_fecha[i].replace("\"",""));
            transferencia.setMarca(a_marca[i].replace("\"",""));
            transferencia.setModelo(a_modelo[i].replace("\"",""));
            transferencia.setImei(a_imei[i].replace("\"",""));
            transferencia.setOrigen(a_origen[i].replace("\"",""));
            list.add(transferencia);
        }

        backup = list;
        progressDialog.dismiss();

    }

    public void sendTransferRequest(final String destino){

        SharedPreferences settings = this.getSharedPreferences(AppConstants.userPreferences, this.MODE_PRIVATE);
        final String dis = settings.getString("dis", "-1");
        final String nombre = settings.getString("agente", "-1");
        final String user = settings.getString("usuario", "-1");

        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/GarantiasA.php";
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        if (!dis.equals("-1")) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("Respuesta", response);
                    AlertDialog.Builder builder = new AlertDialog.Builder(TransferenciasActivity.this);
                    builder.setTitle("MENSAJE");
                    builder.setMessage(response);
                    builder.setCancelable(false);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    builder.show();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(TransferenciasActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method", "sendTransfer");
                    params.put("garan_nombre", nombre);
                    params.put("garan_dis", dis);
                    params.put("transferarr", foliosStr);
                    params.put("garan_user", user);
                    params.put("motivo", "algo");
                    params.put("destino", destino);
                    return params;
                }
            };
            requestQueue.add(stringRequest);

        }
    }

}
