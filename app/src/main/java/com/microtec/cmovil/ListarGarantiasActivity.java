package com.microtec.cmovil;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.microtec.cmovil.Interactor.GarantiaAdapter;
import com.microtec.cmovil.Model.Garantia;
import com.microtec.cmovil.Model.Transferencia;
import com.microtec.cmovil.Utils.AppConstants;
import com.microtec.cmovil.Utils.RecyclerTouchListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ListarGarantiasActivity extends AppCompatActivity {

    private RecyclerView mList;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private List<Garantia> list;
    private List<Garantia> backup;

    private GarantiaAdapter adapter;

    private EditText buscaGar;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_garantias);

        initElementos();
        progressDialog.show();
        getListado("sucursal");

    }

    public void initElementos(){

        buscaGar = findViewById(R.id.busquedaGar);

        backup = new ArrayList<>();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando lista de garantías...");

        mList = (RecyclerView) findViewById(R.id.rView_Listacx);
        list = new ArrayList<>();
        mAdapter = new GarantiaAdapter(getApplicationContext(), list);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        adapter = new GarantiaAdapter(getApplicationContext(), list);

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(adapter);

        mList.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent detalle = new Intent(ListarGarantiasActivity.this, DetalleListadoGarActivity.class);
                detalle.putExtra("imei", backup.get(position).getImei());
                detalle.putExtra("folio", backup.get(position).getFolio());
                detalle.putExtra("objeto", backup.get(position));
                startActivity(detalle);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        buscaGar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filter(editable.toString());
            }
        });

    }

    private void filter(String text){
        List<Garantia> temp = new ArrayList();
        for (Garantia garantia : list){
            if (garantia.getFolio().toLowerCase().contains(text.toLowerCase())){
                temp.add(garantia);
            }
        }
        backup = temp;
        adapter.updateList(temp);
    }

    public void onClickPicker(View view){
        switch (view.getId()){
            case R.id.btn_misgar:
                progressDialog.show();
                getListado("misgar");
                break;
            case R.id.btn_sucursal:
                progressDialog.show();
                getListado("sucursal");
                break;
        }
    }

    public void getListado(final String tipo){

        SharedPreferences settings = this.getSharedPreferences(AppConstants.userPreferences, this.MODE_PRIVATE);

        final String dis = settings.getString("dis", "-1");

        if (!dis.equals("-1")) {
            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/GarantiasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("Respuesta",response);
                    try {
                        String cad = response.substring(1, response.length());
                        JSONObject object = new JSONObject(cad);
                        generaList(object);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    adapter.notifyDataSetChanged();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(ListarGarantiasActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","getListado");
                    params.put("usuario",dis);
                    params.put("tipo",tipo);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void generaList(JSONObject object) throws JSONException {
        Iterator x = object.keys();
        JSONArray jsonArray = new JSONArray();

        list.clear();

        mAdapter.notifyDataSetChanged();

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(object.get(key));
        }

        String folio = jsonArray.getString(0).replace("[","").replace("]","");
        String imei = jsonArray.getString(1).replace("[","").replace("]","");
        String descripcion = jsonArray.getString(2).replace("[","").replace("]","");
        String status = jsonArray.getString(3).replace("[","").replace("]","");
        String color = jsonArray.getString(4).replace("[","").replace("]","");
        String fueracaja = jsonArray.getString(5).replace("[","").replace("]","");
        String marca = jsonArray.getString(6).replace("[","").replace("]","");
        String modelo = jsonArray.getString(7).replace("[","").replace("]","");
        String fecha = jsonArray.getString(8).replace("[","").replace("]","");
        String fechacompra = jsonArray.getString(9).replace("[","").replace("]","");
        String fechaventa = jsonArray.getString(10).replace("[","").replace("]","");
        String factelcel = jsonArray.getString(11).replace("[","").replace("]","");
        String facmicro = jsonArray.getString(12).replace("[","").replace("]","");
        String telefono = jsonArray.getString(13).replace("[","").replace("]","");
        String email = jsonArray.getString(14).replace("[","").replace("]","");
        String fallo = jsonArray.getString(15).replace("[","").replace("]","");
        String checklist = jsonArray.getString(16).replace("[","").replace("]","");
        String propiedad = jsonArray.getString(17).replace("[","").replace("]","");
        String historial = jsonArray.getString(18).replace("[","").replace("]","");
        String comentarios = jsonArray.getString(19).replace("[","").replace("]","");

        String[] a_folio = folio.split(",");
        String[] a_imei = imei.split(",");
        String[] a_descripcion = descripcion.split(",");
        String[] a_status = status.split(",");
        String[] a_color = color.split(",");
        String[] a_fcaja = fueracaja.split(",");
        String[] a_marca = marca.split(",");
        String[] a_modelo = modelo.split(",");
        String[] a_fecha = fecha.split(",");
        String[] a_fventa = fechacompra.split(",");
        String[] a_compra = fechaventa.split(",");
        String[] a_factelcel = factelcel.split(",");
        String[] a_facmicro = facmicro.split(",");
        String[] a_telefono = telefono.split(",");
        String[] a_email = email.split(",");
        String[] a_fallo = fallo.split(",");
        String[] a_checklist = checklist.split(",");
        String[] a_propiedad = propiedad.split(",");
        String[] a_historial = historial.split(",");
        String[] a_comentarios = comentarios.split(",");

        for (int i=0; i<a_imei.length; i++){
            Garantia garantia = new Garantia();
            garantia.setFolio(a_folio[i].replace("\"",""));
            garantia.setImei(a_imei[i].replace("\"",""));
            garantia.setDescripcion(a_descripcion[i].replace("\"",""));
            garantia.setStatus(a_status[i].replace("\"",""));
            garantia.setColor(a_color[i].replace("\"",""));
            garantia.setFcaja(a_fcaja[i].replace("\"",""));
            garantia.setMarca(a_marca[i].replace("\"",""));
            garantia.setModelo(a_modelo[i].replace("\"",""));
            garantia.setFecha(a_fecha[i].replace("\"",""));
            garantia.setFventa(a_fventa[i].replace("\"",""));
            garantia.setFcompra(a_compra[i].replace("\"",""));
            garantia.setFactelcel(a_factelcel[i].replace("\"",""));
            garantia.setFacmicro(a_facmicro[i].replace("\"",""));
            garantia.setTelefono(a_telefono[i].replace("\"",""));
            garantia.setEmail(a_email[i].replace("\"",""));
            garantia.setFallo(a_fallo[i].replace("\"",""));
            garantia.setChecklist(a_checklist[i].replace("\"",""));
            garantia.setPropiedad(a_propiedad[i].replace("\"",""));
            garantia.setHistorial(a_historial[i].replace("\"",""));
            garantia.setComentarios(a_comentarios[i].replace("\"",""));
            list.add(garantia);
        }
        backup = list;
        progressDialog.dismiss();

    }
}
