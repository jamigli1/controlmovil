package com.microtec.cmovil;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.microtec.cmovil.Interactor.NuevaVisitaAdapter;
import com.microtec.cmovil.Model.PuntoVenta;
import com.microtec.cmovil.Utils.AlertResponse;
import com.microtec.cmovil.Utils.RecyclerTouchListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class NuevaVisitaActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private GoogleMap mMap;

    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private static final String prefencesN = "userdata";

    private RecyclerView mList;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private List<PuntoVenta> list;
    private double lat, lon;
    private AlertDialog.Builder alertDialog;

    AlertResponse alertResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_visita);

        buildGoogleApiclient();

        mList = (RecyclerView) findViewById(R.id.rView_NewVisita);
        list = new ArrayList<>();
        mAdapter = new NuevaVisitaAdapter(getApplicationContext(), list);

        alertResponse = new AlertResponse(this);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(mAdapter);

        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("CONTROL MOVIL");
        alertDialog.setMessage("¿Está seguro que desea registrar una visita?");

        final Intent i = new Intent(this, VisitasActivity.class);


        mList.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                final String title1 = ((TextView) mList.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.nvisita_idpv)).getText().toString();
                selectorMenu("SI", title1, i);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        listarPtoVentas();
    }

    private void registrarVisita(final String idpv, final double latitud, final double longitud) {
        SharedPreferences settings = this.getSharedPreferences(prefencesN, this.MODE_PRIVATE);
        final String uid = settings.getString("id", "-1");

        if (!uid.equals("-1")) {

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/VisitasA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //Log.d("Response",response.toString());
                    if (response.contains("DONE")){
//                        Toast.makeText(NuevaVisitaActivity.this, "VISITA REGISTRADA", Toast.LENGTH_SHORT).show();
                        alertResponse.showResponse("VISITA REGISTRADA");
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                            Toast.makeText(NuevaVisitaActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                            alertResponse.showResponse("TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.");
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();

                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String formattedDate = simpleDateFormat.format(c.getTime());

                    params.put("method", "registrarVisita");
                    params.put("fecha", formattedDate);
                    params.put("latitud",String.valueOf(latitud));
                    params.put("longitud",String.valueOf(longitud));
                    params.put("descripcion","Se registro una visita");
                    params.put("user_id", uid);
                    params.put("pv",idpv);
                    params.put("pendiente","404");
                    return params;
                }
            };
            try{
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        500000,
                        -1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
            } catch (Exception e){
                e.printStackTrace();
            }
            requestQueue.add(stringRequest);
        }
    }

    private void listarPtoVentas() {

        SharedPreferences settings = this.getSharedPreferences(prefencesN, this.MODE_PRIVATE);
        final String uid = settings.getString("id", "-1");
        if (!uid.equals("-1")) {
            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/PVentas.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        String cad = response.substring(1, response.length());
                        JSONObject object = new JSONObject(cad);
                        if (object.getString("usuario_id").contains(uid)) {
                            generaList(object);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mAdapter.notifyDataSetChanged();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                            Toast.makeText(NuevaVisitaActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                            alertResponse.showResponse("TIEMPO DE ESPERA AGOTADO. REVISTA TU CONEXIÓN A INTERNET.");
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","listar");
                    params.put("uid",uid);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void generaList(JSONObject object) throws JSONException {
        Iterator x = object.keys();
        JSONArray jsonArray = new JSONArray();

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(object.get(key));
        }

        String codigo = jsonArray.getString(3).replace("[","").replace("]","");
        String nombre = jsonArray.getString(1).replace("[","").replace("]","");
        String idpv = jsonArray.getString(0).replace("[","").replace("]","");
        String[] cod_part = codigo.split(",");
        String[] arr_nom = nombre.split(",");
        String[] arr_idpv = idpv.split(",");
        List<Integer> indices = new ArrayList<Integer>();

        for (int i=0; i<cod_part.length; i++){
            if (!cod_part[i].toString().contains("null")) {
                indices.add(i);
            }
        }

        for (int i=0; i<indices.size(); i++){
            PuntoVenta puntoVenta = new PuntoVenta();
            puntoVenta.setNombre(arr_nom[indices.get(i)].replace("\"",""));
            puntoVenta.setCodigo(cod_part[indices.get(i)].replace("\"",""));
            puntoVenta.setId(arr_idpv[indices.get(i)].replace("\"",""));
            list.add(puntoVenta);
        }
    }

    public void selectorMenu(String opc1, final String title1, final Intent i){

        View mView = getLayoutInflater().inflate(R.layout.alert_dialog,null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.btnDialog_opc1);
        Button btnopc2 = (Button) mView.findViewById(R.id.btnDialog_opc2);
        Button btncancel = (Button) mView.findViewById(R.id.btnDialog_cancel);
        TextView header = (TextView) mView.findViewById(R.id.header);

        btnopc1.setText(opc1);
        btnopc2.setText(R.string.cancelar);
        header.setText(R.string.registrar_visita);

        final Handler handler = new Handler();
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.nw_visit_progrssbar);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //Log.d("COORDENADAS",String.valueOf(lat)+" , "+String.valueOf(lon));
                if (lat == 0) {
                    progressBar.setVisibility(View.VISIBLE);
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            //Log.d("COORDENADAS lentas",String.valueOf(lat)+" , "+String.valueOf(lon));
                            registrarVisita(title1, lat, lon);
                            finish();
                            startActivity(i);
                        }
                    }, 6000);
                } else {
                    //Log.d("COORDENADAS rapidas",String.valueOf(lat)+" , "+String.valueOf(lon));
                    registrarVisita(title1, lat, lon);
                    finish();
                    startActivity(i);
                }
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btncancel.setVisibility(View.GONE);

        dialog.show();
    }

    public void setCoordenadas(){
        lat = mLastLocation.getLatitude();
        lon = mLastLocation.getLongitude();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        //Log.d("CONECTADO","CONECTADO EL MAPA");
        mMap = googleMap;
        buildGoogleApiclient();
        boolean isPermiso = true;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            isPermiso = false;
        }
        if (isPermiso)
            mMap.setMyLocationEnabled(true);
    }

    private boolean isMyPositionSuccess = false;
    @Override
    public void onLocationChanged(Location location) {
        if (!isMyPositionSuccess) {
            mLastLocation = location;
            setCoordenadas();
            isMyPositionSuccess =true;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    protected synchronized void buildGoogleApiclient(){
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

}
