package com.microtec.cmovil;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.microtec.cmovil.Interactor.ComisionesAdapter;
import com.microtec.cmovil.Model.Comision;
import com.microtec.cmovil.Utils.AlertResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DetalleComisionesActivity extends AppCompatActivity {

    private static final String prefencesN = "userdata";

    private RecyclerView mList;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private RecyclerView.Adapter mAdapter;
    private List<Comision> listcomision;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_comisiones);

        mList = (RecyclerView) findViewById(R.id.rView_Comisiones);
        listcomision = new ArrayList<>();
        mAdapter = new ComisionesAdapter(getApplicationContext(),listcomision);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(),linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(mAdapter);

        listarComisiones();

    }

    public void listarComisiones(){
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String agente = settings.getString("agente","-1");

        Bundle extras = getIntent().getExtras();
        final String tipo = extras.getString("tipo");
        final String fecha = extras.getString("fecha");

        final AlertResponse alertResponse = new AlertResponse(DetalleComisionesActivity.this);

        if(!agente.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/ComisionesA.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("response",response);
                    try {
                        generaListComis(response,tipo);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mAdapter.notifyDataSetChanged();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                            Toast.makeText(DetalleComisionesActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                            alertResponse.showResponse("TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.");
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","getReporte");
                    params.put("agente",agente);
                    params.put("fecha",fecha);
                    params.put("tipo",tipo);
                    return params;
                }
            };
            requestQueue.add(stringRequest);
        }
    }

    public void generaListComis(String comisiones, String tipoComi) throws JSONException {
        JSONArray jsonArray = new JSONArray(comisiones);

        for (int i=0; i<jsonArray.length(); i++){
            JSONObject objeto = jsonArray.getJSONObject(i);
            Comision oComision = new Comision();
            oComision.setId(objeto.getString("id"));
            oComision.setFecha(objeto.getString("fecha"));
            oComision.setConcepto(objeto.getString("concepto"));
            oComision.setFauto(objeto.getString("fauto"));
            oComision.setFpago(objeto.getString("fpago"));
            oComision.setTipo(objeto.getString("tipo"));
            oComision.setComision("$"+objeto.getString("comision"));
            if (objeto.getString("status").equals("A")) {
                oComision.setStatus("Aprobado");
            } else {
                oComision.setStatus("Pendiente");
            }
            if (objeto.has("serie")) {
                oComision.setSerie(objeto.getString("serie"));
            } else {
                oComision.setSerie("0");
            }

            oComision.setDistribuidor(objeto.getString("distribuidor"));
            listcomision.add(oComision);
        }
    }


}
