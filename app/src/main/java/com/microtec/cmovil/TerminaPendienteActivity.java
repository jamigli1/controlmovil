package com.microtec.cmovil;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.microtec.cmovil.Interactor.ProductosAdapter;
import com.microtec.cmovil.Model.LocalDataBase;
import com.microtec.cmovil.Model.Producto;
import com.microtec.cmovil.Utils.Estructura;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TerminaPendienteActivity extends AppCompatActivity {

    private RecyclerView mList;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private ProductosAdapter filtro;
    private List<Producto> pProductos;

    private LocalDataBase localDataBase;
    private SQLiteDatabase sqLiteDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_termina_pendiente);

        localDataBase = new LocalDataBase(this);
        sqLiteDatabase = localDataBase.getReadableDatabase();

        mList = (RecyclerView) findViewById(R.id.rView_Pendientes);
        pProductos = new ArrayList<>();
        filtro = new ProductosAdapter(getApplicationContext(),pProductos);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(),linearLayoutManager.getOrientation());

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(filtro);

        try {
            getDataDB();
            filtro.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getDataDB() throws JSONException {

        TextView nombre = (TextView) findViewById(R.id.pendiente_nombrecliente);
        TextView tfecha = (TextView) findViewById(R.id.pendiente_fecha);

        Bundle extras = getIntent().getExtras();
        String enombre = extras.getString("nombre");
        String satcli = extras.getString("satcli");
        String fecha = extras.getString("fecha");

        nombre.setText(enombre);
        tfecha.setText(fecha);

        String sqlConcat = " where satcli = '"+satcli+"' and fecha = '"+fecha+"' ";

        Cursor response = sqLiteDatabase.rawQuery("select * from " + Estructura.TABLE_NAME2 + sqlConcat, null);

        if (response.getCount() != 0){
            while (response.moveToNext()){
                listaProductos(response.getString(6));
            }
        }
    }

    public void listaProductos(String json) throws JSONException {
        JSONArray jsonArray = new JSONArray(json);

        for (int i=0; i<jsonArray.length(); i++){
            Producto producto = new Producto();
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            producto.setMarca(jsonObject.getString("marca"));
            producto.setModelo(jsonObject.getString("modelo"));
            producto.setSerie(jsonObject.getString("serie"));
            producto.setPrecio(jsonObject.getString("precio"));
            producto.setPrecioR(jsonObject.getString("precioR"));
            pProductos.add(producto);
        }

    }

    public void onClickDelPendiente(View view){

        makeAlert("CONTROL MÓVIL", "¿Desea eliminar esta venta pendiente?");

    }

    public void sqlDelete(){
        Bundle extras = getIntent().getExtras();
        String satcli = extras.getString("satcli");
        String fecha = extras.getString("fecha");

        String sqlConcat = " where satcli = '"+satcli+"' and fecha = '"+fecha+"' ";

        LocalDataBase localDataBase;
        localDataBase = new LocalDataBase(this);
        SQLiteDatabase sqLiteDatabase = localDataBase.getWritableDatabase();
        sqLiteDatabase.execSQL("delete from "+ Estructura.TABLE_NAME2 + " " + sqlConcat);

        Toast.makeText(this, "VENTA PENDIENTE ELIMINADA", Toast.LENGTH_SHORT).show();

        sqLiteDatabase.close();

        startActivity(new Intent(this, VentasPendientesActivity.class));
        finish();
    }

    public void makeAlert(String titulo, String cuerpo){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(cuerpo);
        builder.setCancelable(true);
        builder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sqlDelete();
                    }
                });
        builder.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        AlertDialog alerta = builder.create();
        alerta.show();
    }
}
