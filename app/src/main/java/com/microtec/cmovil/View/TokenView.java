package com.microtec.cmovil.View;

public interface TokenView {
    void setTokenInView(String token);
    void setSecondsInView(String seconds);
}

