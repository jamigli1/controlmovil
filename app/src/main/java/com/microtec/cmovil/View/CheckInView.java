package com.microtec.cmovil.View;

import com.microtec.cmovil.Model.BuscarFolioCheckIn;

import java.util.List;

public interface CheckInView {

    void showInfo(List<BuscarFolioCheckIn> resultado);

    void showMessage(String mensaje);

    void showButtonCheckIn(boolean isCheckIn);

    void showProgressBar();

    void hideProgressBar();

}
