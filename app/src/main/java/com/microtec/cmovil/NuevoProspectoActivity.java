package com.microtec.cmovil;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.microtec.cmovil.Model.Encuesta;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class NuevoProspectoActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String prefencesN = "userdata";
    static final int REQUEST_IMAGE_CAPTURE = 1;

    String mCurrentPaht;

    private GoogleMap mMap;

    private Location mLastLocation;
    private Marker myPositionMarker;
    private LatLng pickupLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private RelativeLayout mapaLayout;
    private RelativeLayout formLayout;
    private RelativeLayout cargaPant;

    public LinearLayout form2, form3;
    private TextView tipotienda;
    private RadioGroup microtec, competencia;
    public double lat, lon;
    public String tipot;
    Button mostrarL, cliente;

    //Botón de guardado
    public Button guardar;

    //Datos formulario

    //EditText
    public EditText nombrex, nombrepventa, comentarios, telefono, email;
    public EditText municipio, ciudad, colonia, codigop, calle;

    //Array
    public ArrayList<String> lista;
    public String idpv1, idt1;

    //Spinners
    public  Spinner estado;

    //TERMINA FORMULARIOS

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_prospecto);

        initElementos();

        mostrarL = (Button) findViewById(R.id.btn_mostrar);
        mostrarL.setOnClickListener(this);

        mapaLayout = (RelativeLayout) findViewById(R.id.nuevo_prospecto_contenido_mapa);
        formLayout = (RelativeLayout) findViewById(R.id.nuevo_prospecto_contenido_form);
        cargaPant = (RelativeLayout) findViewById(R.id.layoutcarga);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nuevo_propecto_mapa);
        mapFragment.getMapAsync(this);

    }

    public void hidePicker(String tipot){
        switch (tipot){
            case "micro":
                tipotienda.setText("TIENDA MICROTEC");
                break;
            case "comp":
                competencia.setVisibility(View.VISIBLE);
                microtec.setVisibility(View.GONE);
                tipotienda.setText("COMPETENCIA");
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        buildGoogleApiclient();
        boolean isPermiso = true;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            isPermiso = false;
        }
        if (isPermiso)
            mMap.setMyLocationEnabled(true);
    }

    private boolean isMyPositionSuccess = false;
    @Override
    public void onLocationChanged(Location location) {
        if (!isMyPositionSuccess) {
            mLastLocation = location;
            drawMyPosition();
            isMyPositionSuccess =true;
        }
    }

    public void drawMyPosition(){
        pickupLocation = new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(pickupLocation));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
        myPositionMarker = mMap.addMarker(new MarkerOptions()
                .position(pickupLocation)
                .title("ESTÁS AQUÍ")
                .draggable(true)
                .alpha(0.8f)
                .icon(BitmapDescriptorFactory.defaultMarker(
                        BitmapDescriptorFactory.HUE_CYAN)));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            lat = mLastLocation.getLatitude();
            lon = mLastLocation.getLongitude();
            tipot = extras.getString("tipotienda");
            //Log.d("TIPO TIENDA", tipot);
            hidePicker(tipot);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    protected synchronized void buildGoogleApiclient(){
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public void onClickLocReady(View view){
        mapaLayout.setVisibility(View.GONE);
        formLayout.setVisibility(View.VISIBLE);
    }

    public void initElementos(){

        //Botón
        guardar = (Button) findViewById(R.id.guardarProspecto);
        cliente = (Button) findViewById(R.id.convertircliente);

        //Layouts
        form2 = (LinearLayout) findViewById(R.id.lnl_datos_cliente);
        form3 = (LinearLayout) findViewById(R.id.lnl_datos_establecimiento);

        //Textview tipo tienda
        tipotienda = (TextView) findViewById(R.id.tipo_prospecto);

        //Radiogroup tipo tienda
        microtec = (RadioGroup) findViewById(R.id.radio_microtec);
        competencia = (RadioGroup) findViewById(R.id.radio_competencia);

        //Spinner estados
        estado = (Spinner) findViewById(R.id.new_prspcto_estado);
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this,R.array.estados_array, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        estado.setAdapter(arrayAdapter);

        //Edittext
        nombrex = (EditText) findViewById(R.id.et_nprospecto_nombre);
        nombrepventa = (EditText) findViewById(R.id.et_nprospecto_nombrepventa);
        comentarios = (EditText) findViewById(R.id.et_nprospecto_comentarios);
        telefono = (EditText) findViewById(R.id.opc_et_nprospecto_telefono);
        email = (EditText) findViewById(R.id.opc_et_nprospecto_email);
        municipio = (EditText) findViewById(R.id.opc_et_nprospecto_municipio);
        ciudad = (EditText) findViewById(R.id.opc_et_nprospecto_ciudad);
        colonia = (EditText) findViewById(R.id.opc_et_nprospecto_colonia);
        codigop = (EditText) findViewById(R.id.opc_et_nprospecto_codigopostal);
        calle = (EditText) findViewById(R.id.opc_et_nprospecto_calle);

    }

    public void onClickMostrar(){
        mostrarL.setText("OCULTAR DATOS OPCIONALES");
        form2.setVisibility(View.VISIBLE);
        form3.setVisibility(View.VISIBLE);
        mostrarL.setId(R.id.btn_ocultar);
    }

    public void onClickOcultar(){
        mostrarL.setText("MOSTRAR DATOS OPCIONALES");
        form2.setVisibility(View.GONE);
        form3.setVisibility(View.GONE);
        mostrarL.setId(R.id.btn_mostrar);
    }

    public void checkResponse(JSONObject jsonObject, int opc, final ArrayList<String> campos) throws JSONException {

        Iterator x = jsonObject.keys();
        JSONArray jsonArray = new JSONArray();

        String[] estado;

        while (x.hasNext()){
            String key = (String) x.next();
            jsonArray.put(jsonObject.get(key));
        }

        estado = (jsonArray.getString(0).replace("[","").replace("]","")).split(",");
        final String[] idpv = (jsonArray.getString(1).replace("[","").replace("]","")).split(",");
        final String[] idt = (jsonArray.getString(2).replace("[","").replace("]","")).split(",");


        if (estado[0].equals("0")){
            Toast.makeText(this, "Hubo un error al guardar. La ubicación del punto de venta ya existe.", Toast.LENGTH_SHORT).show();
            guardar.setVisibility(View.VISIBLE);
        } else {
            if (opc == 0) {
                Toast.makeText(this, "PV Creado...", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, ProspectosActivity.class));
                finish();
                guardar.setVisibility(View.GONE);
            } else if(opc == 1) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("TIPO DE CLIENTE:");
                builder.setTitle("CONVERSIÓN");
                builder.setPositiveButton("Punto de Venta", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        selectorMenu("OK", "Escriba su RFC", idpv[0], idt[0], campos, 1);
                        guardar.setVisibility(View.GONE);
                    }
                });

                builder.setNeutralButton("Micropayment", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        selectorMenu("OK", "Escriba su RFC", idpv[0], idt[0], campos, 0);
                        guardar.setVisibility(View.GONE);
                    }
                });

                builder.show();
            }
        }
    }

    public void sendEncuesta(final String respuestas, final ArrayList<String> campos, final int opc){
        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String id_us = settings.getString("id","-1");

        if(!id_us.equals("-1")){

            String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/PVentas.php";
            RequestQueue requestQueue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("RESPUESTA",response);
                    try{
                        String cad = response.substring(1,response.length());
                        JSONObject object = new JSONObject(cad);

                        if(object.length() != 0){
                            checkResponse(object, opc, campos);
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            guardar.setEnabled(true);
                            Toast.makeText(NuevoProspectoActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                            guardar.setVisibility(View.VISIBLE);
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("method","crearPV");
                    params.put("respuestas",respuestas);
                    params.put("nombrePV",campos.get(0));
                    params.put("id_us",id_us);
                    params.put("estado",campos.get(3));
                    params.put("tipo_id",campos.get(4));
                    params.put("nombre_titular",campos.get(1));
                    params.put("telefono",campos.get(6));
                    params.put("correo",campos.get(7));
                    params.put("municipio",campos.get(8));
                    params.put("ciudad",campos.get(9));
                    params.put("colonia",campos.get(10));
                    params.put("calle",campos.get(12));
                    params.put("codigo_postal",campos.get(11));
                    params.put("latitud", String.valueOf(mLastLocation.getLatitude()));
                    params.put("longitud", String.valueOf(mLastLocation.getLongitude()));
                    params.put("subtipo",campos.get(5));
                    return params;
                }
            };
            try{
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        500000,
                        -1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
            } catch (Exception e){
                e.printStackTrace();
            }
            requestQueue.add(stringRequest);
        }
    }

    public void sendConversionRequest(final String rfc, final ArrayList<String> campos, final String idpv, final String idt, final int subtipo){
        String url = "https://www.micro-tec.com.mx/pagina/app/clases/Control_Movil/ClientesA.php";
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        SharedPreferences settings = this.getSharedPreferences(prefencesN,this.MODE_PRIVATE);
        final String dis = settings.getString("dis","-1");
        final String agente = settings.getString("agente","-1");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Respuestaconversion",response);
                if (response.contains("DONE")){
                    cargaPant.setVisibility(View.GONE);
                    Toast.makeText(NuevoProspectoActivity.this, response.replace("null","").replace("DONE",""), Toast.LENGTH_SHORT).show();
                    Intent wizard = new Intent(NuevoProspectoActivity.this,FotosWizardActivity.class);
                    Log.d("IDPV",idpv);
                    wizard.putExtra("latitud",mLastLocation.getLatitude());
                    wizard.putExtra("longitud",mLastLocation.getLongitude());
                    wizard.putExtra("idpv",idpv);
                    startActivity(wizard);
                    finish();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        cliente.setEnabled(true);
                        Toast.makeText(NuevoProspectoActivity.this, "TIEMPO DE ESPERA AGOTADO. REVISA TU CONEXIÓN A INTERNET.", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("method","crearCliente");
                params.put("idPV",idpv);
                params.put("idT",idt);
                params.put("dis",dis);
                params.put("nombreT",campos.get(1));
                params.put("cel",campos.get(6));
                params.put("email",email.getText().toString());
                params.put("edo",campos.get(3));
                params.put("muni",campos.get(8));
                params.put("ciudad",campos.get(9));
                params.put("colonia",campos.get(10));
                params.put("calle",campos.get(12));
                params.put("next","8");
                params.put("nint","");
                params.put("cp",campos.get(11));
                params.put("rfc",rfc);
                params.put("agente",agente);
                //TODO agregar lo de subtipo
                params.put("subtinuevo", String.valueOf(subtipo));
                return params;
            }
        };
        try{
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    500000,
                    -1,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
        } catch (Exception e){
            e.printStackTrace();
        }
        requestQueue.add(stringRequest);
    }

    public void selectorMenu(String opc1, final String title1, final String idpv, final String idt, final ArrayList<String> campos, final int subtipo){

        View mView = getLayoutInflater().inflate(R.layout.alert_dialog,null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        Button btnopc1 = (Button) mView.findViewById(R.id.btnDialog_opc1);
        Button btnopc2 = (Button) mView.findViewById(R.id.btnDialog_opc2);
        Button btncancel = (Button) mView.findViewById(R.id.btnDialog_cancel);
        final EditText rfc = (EditText) mView.findViewById(R.id.et_rfc);
        TextView header = (TextView) mView.findViewById(R.id.header);

        rfc.setVisibility(View.VISIBLE);

        btnopc1.setText(opc1);
        btnopc2.setText(R.string.cancelar);
        header.setText(title1);

        btnopc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                sendConversionRequest(rfc.getText().toString(), campos, idpv, idt, subtipo);
            }
        });

        btnopc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btncancel.setVisibility(View.GONE);

        dialog.show();
    }

    //TODO Habilita el botón para pruebas
    public void onClickConvertirCliente(View view) throws JSONException {

        ArrayList<String> campos = new ArrayList<>();

        String nombrePv = checkRequiredEditText(nombrepventa);
        String nombreT = checkRequiredEditText(nombrex);
        String comments = checkRequiredEditText(comentarios);
        //Otros
        String telefonoT = checkRequiredEditText(telefono);
        String emailT = checkRequiredEditText(email);
        String municipioT = checkRequiredEditText(municipio);
        String ciudadT = checkRequiredEditText(ciudad);
        String coloniaT = checkRequiredEditText(colonia);
        String codigo = checkRequiredEditText(codigop);
        String calleT = checkRequiredEditText(calle);

        String micro = getSelectedRGroup(microtec);
        String compe = getSelectedRGroup(competencia);

        setRequiredRButton(micro,compe);

        if(!nombrePv.equals("") && !nombreT.equals("") && !comments.equals("") && (!micro.equals("-1") || !compe.equals("-1"))
            && !telefonoT.equals("") && !emailT.equals("") && !municipioT.equals("") && !ciudadT.equals("") && !coloniaT.equals("") && !codigo.equals("") && !calleT.equals("")) {
            campos.add(nombrePv); //0
            campos.add(nombreT); //1
            campos.add(comments); //2
            campos.add(estado.getSelectedItem().toString()); //3
            if (tipot.equals("micro")){
                campos.add("2"); //4
            } else {
                campos.add("1"); //4
            }
            campos.add(getRButtonO(microtec,competencia)); //5
            campos.add(telefono.getText().toString()); //6
            campos.add(email.getText().toString()); //7
            campos.add(municipio.getText().toString()); //8
            campos.add(ciudad.getText().toString()); //9
            campos.add(colonia.getText().toString()); //10
            campos.add(codigop.getText().toString()); //11
            campos.add(calle.getText().toString()); //12

            cargaPant.setVisibility(View.VISIBLE);
            guardar.setEnabled(false);
            cliente.setEnabled(false);
            sendEncuesta(checkPreguntas(),campos,1);

        }

        /*Intent fotos = new Intent(this, FotosWizardActivity.class);
        fotos.putExtra("latitud",mLastLocation.getLatitude());
        fotos.putExtra("longitud",mLastLocation.getLongitude());
        startActivity(fotos);*/
    }

    public void onClickNuevoPV(View view) throws JSONException {
        ArrayList<String> campos = new ArrayList<>();

        String nombrePv = checkRequiredEditText(nombrepventa);
        String nombreT = checkRequiredEditText(nombrex);
        String comments = checkRequiredEditText(comentarios);

        String micro = getSelectedRGroup(microtec);
        String compe = getSelectedRGroup(competencia);

        setRequiredRButton(micro,compe);

        if(!nombrePv.equals("") && !nombreT.equals("") && !comments.equals("") && (!micro.equals("-1") || !compe.equals("-1"))) {
            campos.add(nombrePv); //0
            campos.add(nombreT); //1
            campos.add(comments); //2
            campos.add(estado.getSelectedItem().toString()); //3
            if (tipot.equals("micro")){
                campos.add("2"); //4
            } else {
                campos.add("1"); //4
            }
            campos.add(getRButtonO(microtec,competencia)); //5
            campos.add(telefono.getText().toString()); //6
            campos.add(email.getText().toString()); //7
            campos.add(municipio.getText().toString()); //8
            campos.add(ciudad.getText().toString()); //9
            campos.add(colonia.getText().toString()); //10
            campos.add(codigop.getText().toString()); //11
            campos.add(calle.getText().toString()); //12

            guardar.setEnabled(false);
            sendEncuesta(checkPreguntas(), campos, 0); //13
        }

    }

    public String getRButtonO(RadioGroup micro, RadioGroup compe){
        RadioButton radioButton;
        if (!getSelectedRGroup(micro).equals("-1")){
            return getSelectedRGroup(micro);
        } else {
            return getSelectedRGroup(compe);
        }
    }

    public void setRequiredRButton(String smicro, String scompe){
        if (tipot.equals("micro") && (smicro.equals("-1"))){
            tipotienda.setTextColor(getResources().getColor(R.color.required));
            tipotienda.setText(tipotienda.getText().toString().concat(" <- CAMPO REQUERIDO"));
        } else if (tipot.equals("comp") && scompe.equals("-1")){
            tipotienda.setTextColor(getResources().getColor(R.color.required));
            tipotienda.setText(tipotienda.getText().toString().concat(" <- CAMPO REQUERIDO"));
        }
    }

    public String getSelectedRGroup(RadioGroup radioGroup){
        int index;
        String seleccionado;
        RadioButton selected;

        if (radioGroup.getCheckedRadioButtonId() != -1){
            index = radioGroup.getCheckedRadioButtonId();
            selected = (RadioButton) radioGroup.findViewById(index);
            seleccionado = selected.getText().toString();
            return seleccionado;
        }

        return "-1";
    }

    public String checkRequiredEditText(EditText requerido){
        if (TextUtils.isEmpty(requerido.getText())){
            requerido.setError("Este campo es requerido.");
        } else {
            return requerido.getText().toString();
        }
        return "";
    }

    public String checkPreguntas() throws JSONException {

        JSONArray jsonEncuesta = new JSONArray();
        ArrayList<Encuesta> encuestas = new ArrayList<>();

        if (!getSelectedRGroup(microtec).contains("-1")){
            encuestas.add(new Encuesta("1",getIdRespuesta(getSelectedRGroup(microtec))));
        }
        if (!getSelectedRGroup(competencia).contains("-1")){
            encuestas.add(new Encuesta("2",getIdRespuesta(getSelectedRGroup(competencia))));
        }

        for (int i=0; i<encuestas.size(); i++){
            JSONObject jsonOEncuesta = new JSONObject();
            jsonOEncuesta.put("pregunta",encuestas.get(i).getPregunta());
            jsonOEncuesta.put("respuesta",encuestas.get(i).getRespuesta());
            jsonEncuesta.put(jsonOEncuesta);
        }

        return jsonEncuesta.toString();

    }

    public String getIdRespuesta(String respuesta){

        String id = "-2";

        switch (respuesta){
            case "Tienda propia (Solo MicroTec)":
                return "12";
            case "Franquicia":
                return "13";
            case "Micro-Franquicia":
                return "14";
            case "Micropayments":
                return "15";
            case "Sub-Distribuidor":
                return "16";
            case "Prospecto":
                return "17";
            case "PV Telcel":
                return "51";
            case "AT&T":
                return "52";
            case "Unefon":
                return "53";
            case "Multimarca":
                return "54";
            case "Movistar":
                return "55";
            case "GMM":
                return "56";
                default:
                    return "NO SE ENCUENTRA";

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_mostrar:
                onClickMostrar();
                break;
            case R.id.btn_ocultar:
                onClickOcultar();
                break;
        }
    }

}
